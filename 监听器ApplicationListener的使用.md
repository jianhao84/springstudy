### 监听器ApplicationListener的使用场景

#### 一、当容器初始化完成之后，自定义初始化操作，比如一些数据的加载、初始化缓存、特定任务的注册等.
* 使用步骤:
    1. 自定义监听类(比如叫DemoListener),实现ApplicationListener<ApplicationContextEvent>,注意事件类型必须是ApplicationContextEvent(ApplicationContextEvent的子类都不行).   
    2. 实现public void onApplicationEvent(DemoEvent2 event)方法,用来做一些自定义初始化操作,代码如下:
    ~~~
    @Component
    public class DemoListener implements ApplicationListener<ApplicationContextEvent> {
    
        /**
         * Handle an application event.
         *
         * @param event the event to respond to
         */
        @Override
        public void onApplicationEvent(ApplicationContextEvent event) {
        /**
         *Spring的内置事件有几个(专门文章介绍),
         *其中ContextRefreshedEvent事件会在容器初始化完成后调用,以此来实现在spring初始化完成后进行自定义初始化操作.
         */
            if(event instanceof ContextRefreshedEvent) {
                // 实现自定义初始化操作代码
            }
        }
    }
    ~~~
    
#### 二、springboot正常事件监听使用
* 事件过程:
    1. 需要先注册(装载)事件
    2. 发布事件
    3. 监听到事件并处理事件
* 自定义事件类(比如类名为MyEvent),方便下面方式使用,代码如下:
~~~
@Data
public class MyEvent extends ApplicationEvent {
    // 构造
    public DemoEvent(Object source) {
        // source可以传递事件参数对象
        super(source);
        this.msg = msg;
    }
}
~~~
##### 事件监听的定义及注册(加载)的4种方式:
1. 手动向ApplicationContext中添加监听器.
    * 使用步骤:
        1. 创建MyListener1类,实现ApplicationListener,如下:
        ~~~
        public class MyListener1 implements ApplicationListener<MyEvent>
        {
            Logger logger = Logger.getLogger(MyListener1.class);
            
            // 监听方法
            public void onApplicationEvent(MyEvent event)
            {
                logger.info(String.format("%s监听到事件源：%s.", MyListener1.class.getName(), event.getSource()));
                // TODO 事件处理代码
            }
        }
        ~~~
        2. 然后在springboot应用启动类中获取ConfigurableApplicationContext上下文，装载监听,代码如下:
        ~~~
        @SpringBootApplication
        public class LisenterApplication
        {
            public static void main(String[] args)
            {
                ConfigurableApplicationContext context = SpringApplication.run(LisenterApplication.class, args);
                // 装载监听
                context.addApplicationListener(new MyListener1());
            }
        }
        ~~~
        或者:
        ~~~
        @SpringBootApplication
        public class LisenterApplication
        {
            public static void main(String[] args)
            {
                SpringApplication springApplication = new SpringApplication(StartupServer.class);		
                springApplication.addListeners(new MyListener1());		
                springApplication.run(args); 
            }
        }
        ~~~
2. 将监听器装载入spring容器.
    * 使用步骤:
        1. 创建MyListener2类,实现ApplicationListener接口，并使用@Component注解将该类装载入spring容器中,代码如下:
        ~~~
        // 用@Component注解将该类装载入spring容器中
        @Component
        public class MyListener2 implements ApplicationListener<MyEvent>
        {
            Logger logger = Logger.getLogger(MyListener2.class);
            
            // 事件监听方法
            public void onApplicationEvent(MyEvent event)
            {
                logger.info(String.format("%s监听到事件源：%s.", MyListener2.class.getName(), event.getSource()));
                // TODO 事件处理代码
            }
        }
        ~~~
3. 在application.properties中配置监听器
    * 使用步骤:
       1. 创建MyListener3类,实现ApplicationListener接口,代码如下:
       ~~~
       public class MyListener3 implements ApplicationListener<MyEvent>
       {
            Logger logger = Logger.getLogger(MyListener3.class);
            // 事件监听方法
            public void onApplicationEvent(MyEvent event)
            {
                logger.info(String.format("%s监听到事件源：%s.", MyListener3.class.getName(), event.getSource()));
                // TODO 事件处理代码
            }
       }
       ~~~
       2. 在application.properties中配置监听,内容如下:
       ~~~
       context.listener.classes=com.listener.MyListener3
       ~~~
4. 通过@EventListener注解实现事件监听
    * 使用步骤:
        1. 创建MyListener4类，该类无需实现ApplicationListener接口，使用@EventListener装饰具体方法,代码如下:
        ~~~
        @Component
        public class MyListener4
        {
            Logger logger = Logger.getLogger(MyListener4.class);
            // 事件监听方法
            @EventListener
            public void listener(MyEvent event)
            {
                logger.info(String.format("%s监听到事件源：%s.", MyListener4.class.getName(), event.getSource()));
                // TODO 事件处理代码
            }
        }
        ~~~
##### 事件的发布:
* 使用步骤:
    1. 获取到ApplicationContext的bean(方法在专门文档介绍).
    2. 发布事件,为了方便测试,这里在启动的时候发布事件,代码如下:
    ~~~
    ApplicationContext context = SpringApplication.run(StartupServer.class, args);
    //  发布事件事件参数为"abcd".
    context.publishEvent(new DemoEvent("abcd"));
    ~~~
    3. 发布事件后,对应的监听器会监听并执行.
