package dd;

import org.fusesource.jansi.Ansi;
import org.springframework.core.io.Resource;


/**
 * @author huqichao
 * @date 2018-06-07 15:20
 */
public class Test {

    public static void main(String[] args) {
        System.out.println(" ___                           \n" +
                "/\\_ \\                    __    \n" +
                "\\//\\ \\    __  __     __ /\\_\\   \n" +
                "  \\ \\ \\  /\\ \\/\\ \\  /'__`\\/\\ \\  \n" +
                "   \\_\\ \\_\\ \\ \\_\\ \\/\\ \\L\\ \\ \\ \\ \n" +
                "   /\\____\\\\ \\____/\\ \\___, \\ \\_\\\n" +
                "   \\/____/ \\/___/  \\/___/\\ \\/_/\n" +
                "                        \\ \\_\\  \n" +
                "                         \\/_/  ");
        System.out.println(Ansi.ansi().eraseScreen().render("@|red Hello|@ @|green World|@"));
    }
}
