package com.jianhao84;

import com.jianhao84.test.testevent.DemoEvent;
import com.jianhao84.test.testapplicationContext.ApplicationContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * @author huqichao
 * @date 2018-05-28 10:10
 */
@SpringBootApplication
public class StartupServer {

    public static void main(String[] args) {
        System.out.println("00000000");

        ApplicationContext context = SpringApplication.run(StartupServer.class, args);
        context.publishEvent(new DemoEvent("abcd"));
        context.getBean(ApplicationContextUtil.class).show();
    }
}
