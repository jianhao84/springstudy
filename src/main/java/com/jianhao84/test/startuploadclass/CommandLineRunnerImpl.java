package com.jianhao84.test.startuploadclass;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created by wj69716 on 2018/12/20.
 */
@Component
@Order(1)
public class CommandLineRunnerImpl implements CommandLineRunner {
    @Override
    public void run(String... strings) throws Exception {
        System.out.println("通过实现commandLineRunner接口实现启动加载...");
    }
}
