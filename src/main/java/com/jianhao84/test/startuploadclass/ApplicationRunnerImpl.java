package com.jianhao84.test.startuploadclass;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Created by wj69716 on 2018/12/20.
 */
@Component
@Order(2)
public class ApplicationRunnerImpl implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        System.out.println("通过实现ApplicationRunner接口实现启动加载...");
    }
}
