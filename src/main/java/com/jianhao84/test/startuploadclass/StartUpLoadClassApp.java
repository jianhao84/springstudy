package com.jianhao84.test.startuploadclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by wj69716 on 2018/12/20.
 */
@SpringBootApplication
public class StartUpLoadClassApp  {
    public static void main(String[] args) {
        SpringApplication.run(StartUpLoadClassApp.class,args);
    }
}
