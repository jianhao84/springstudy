package com.jianhao84.test.testautowire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-16
 * @time: 10:50
 */
@Component
public class TestBeerBean {
    private String name;

    @Autowired
    public void initBeerName(BeerBean beer) {
        this.name = beer.getName();

    }

    public void test() {
        System.out.println("通过@Autowired加在方法上注入:");
        System.out.println(name);
    }
}
