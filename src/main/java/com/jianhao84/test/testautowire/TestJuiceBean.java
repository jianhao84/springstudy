package com.jianhao84.test.testautowire;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-16
 * @time: 11:04
 */
@Data
public class TestJuiceBean {
    private String nick;

    public void test() {
        System.out.println("通过@Bean的参数注入:");
        System.out.println(nick);
    }
}
