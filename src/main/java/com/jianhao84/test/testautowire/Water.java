package com.jianhao84.test.testautowire;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-16
 * @time: 11:10
 */
@Component
@Data
public class Water implements InitializingBean {
    private String weight;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.weight = "100kg";
    }
}
