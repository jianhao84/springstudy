package com.jianhao84.test.testautowire;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-16
 * @time: 10:34
 */
@Component
@Data
public class BeerBean implements InitializingBean {

    private String name;


    @Override
    public void afterPropertiesSet() throws Exception {
        this.name = "beer";
    }
}
