package com.jianhao84.test.testautowire;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-16
 * @time: 11:09
 */
@Data
@Component
public class TestWaterBean {
    private String weight;

    public  TestWaterBean(Water water) {
        this.weight = water.getWeight();
    }

    public void test() {
        System.out.println("通过构造方法注入:");
        System.out.println(weight);
    }
}
