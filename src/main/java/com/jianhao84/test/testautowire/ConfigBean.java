package com.jianhao84.test.testautowire;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-16
 * @time: 11:00
 */
@Configuration
public class ConfigBean {

    @Bean
    public TestJuiceBean getJuice(JuiceBean juice) {
        TestJuiceBean bean = new TestJuiceBean();
        bean.setNick(juice.getNick());
        return bean;
    }
}
