package com.jianhao84.test.testautowire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-16
 * @time: 10:25
 */
@SpringBootApplication
public class AutoWiredStart {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(AutoWiredStart.class,args);
        context.getBean(TestBeerBean.class).test();
        context.getBean(TestJuiceBean.class).test();
        context.getBean(TestWaterBean.class).test();
    }
}
