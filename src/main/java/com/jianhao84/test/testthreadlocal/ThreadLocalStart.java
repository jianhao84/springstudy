package com.jianhao84.test.testthreadlocal;

import com.jianhao84.test.testthreadlocal.pojo.TestThreadLoacl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-05
 * @time: 14:12
 */
@SpringBootApplication
public class ThreadLocalStart {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(ThreadLocalStart.class,args);
        context.getBean(TestThreadLoacl.class).show();
    }
}
