package com.jianhao84.test.testthreadlocal.pojo;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-05
 * @time: 15:46
 */
@Component
public class TestThreadLoacl {
    private ThreadLocal<Integer> a = new ThreadLocal();
    private ThreadLocal<Integer> b = new ThreadLocal();

    public void show() {
        a.set(1);
        b.set(2);
        System.out.println(a.get());
        System.out.println(b.get());
    }
}
