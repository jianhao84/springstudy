package com.jianhao84.test.testresource;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.*;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-06
 * @time: 11:01
 */
public class ResourceTest {

    @Test
    public void test() throws IOException {
        Resource resource = new ClassPathResource("data.db");
        String fileName = resource.getDescription();
        System.out.println(fileName);
        if (resource.isReadable()) {
            //每次都会打开一个新的流
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line=br.readLine()) != null) {
                System.out.println(line);
            }
            if (is != null) {
                is.close();
            }
            if (br != null) {
                br.close();
            }
        }
    }

    @Test
    public void testFileSystem() throws IOException {
        FileSystemResource resource = new FileSystemResource("d:/abc.properties");
        if (resource.isReadable()) {
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line=br.readLine()) != null) {
                System.out.println(line);
            }
            if (is != null) {
                is.close();
            }
            if (br != null) {
                br.close();
            }
        }
        if (resource.isWritable()) {
            //每次都会获取到一个新的输出流
            OutputStream os = resource.getOutputStream();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            bw.write("FileSystemResource实现类测试。");
            bw.flush();
            if (os != null) {
                os.close();
            }
            if (bw != null) {
                bw.close();
            }
        }
    }

    @Test
    public void testURL() throws Exception {
        UrlResource resource = new UrlResource("http://www.baidu.com");
        if (resource.isReadable()) {
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line=br.readLine()) != null) {
                System.out.println(line);
            }
            if (is != null) {
                is.close();
            }
            if (br != null) {
                br.close();
            }
        }
    }

    @Test
    public void testByteArray() throws IOException {
        ByteArrayResource resource = new ByteArrayResource("Hello".getBytes());
        if (resource.isReadable()) {
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line=br.readLine()) != null) {
                System.out.println(line);
            }
            if (is != null) {
                is.close();
            }
            if (br != null) {
                br.close();
            }
        }
    }

    @Test
    public void testInputStream() throws Exception {
        InputStream is = new FileInputStream("D:/abc.properties");
        InputStreamResource resource = new InputStreamResource(is);
        //对于InputStreamResource而言，其getInputStream()方法只能调用一次，继续调用将抛出异常。
        InputStream target = resource.getInputStream();   //返回的就是构件时的那个InputStream
        if (resource.isReadable()) {
            BufferedReader br = new BufferedReader(new InputStreamReader(target));
            String line;
            while ((line=br.readLine()) != null) {
                System.out.println(line);
            }
            if (is != null) {
                is.close();
            }
            if (br != null) {
                br.close();
            }
        }
    }

    @Test
    public void testDefaultResourceLoader() {
        //ResourceLoader还可以通过实现ResourceLoaderAware接口或者使用@Autowired注解注入的方式获取
        ResourceLoader loader = new DefaultResourceLoader();
        Resource resource = loader.getResource("http://www.baidu.com");
        System.out.println(resource instanceof UrlResource); //true
        //注意这里前缀不能使用“classpath*:”，这样不能真正访问到对应的资源，exists()返回false
        resource = loader.getResource("classpath:test.txt");
        System.out.println(resource instanceof ClassPathResource); //true
        resource = loader.getResource("test.txt");
        System.out.println(resource instanceof ClassPathResource); //true
    }


    @Test
    public void testContextResource() throws IOException{
        //获取spring上下文
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
        //通过spring context获取Resource
        Resource resource = context.getResource("application.properties");
        if (resource.isReadable()) {
            //URLConnection对应的getInputStream()。
            if (resource.isReadable()) {
                InputStream is = resource.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line=br.readLine()) != null) {
                    System.out.println(line);
                }
                if (is != null) {
                    is.close();
                }
                if (br != null) {
                    br.close();
                }
            }
        }
    }

}
