package com.jianhao84.test.testcglibcallback;

import com.jianhao84.test.testcglibcallback.callback.*;
import org.junit.Test;
import org.springframework.cglib.proxy.*;
import org.springframework.cglib.proxy.Proxy;

import java.lang.reflect.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-14
 * @time: 18:04
 */
public class CallBackTest {

    /**
     * 单个回调
     */
    @Test
    public void testSingleCallBack() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(CallBackServiceImpl.class);
        enhancer.setCallback(new MethodInterceptorCallBack.MethodInterceptorCallBackA());
        CallBackServiceImpl a = (CallBackServiceImpl)enhancer.create();
        System.out.println(a.singleCall());
    }

    /**
     *多个回调,用CallbackFilter选择一个CallBack进行方法代理.
     */
    @Test
    public void testMultiCall() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(CallBackServiceImpl.class);
        enhancer.setCallbacks(new Callback[]{new MethodInterceptorCallBack.MethodInterceptorCallBackA(),new MethodInterceptorCallBack.MethodInterceptorCallBackB()});
        enhancer.setCallbackFilter(new CallbackFilter() {
            @Override
            public int accept(Method method) {
                switch (method.getName()) {
                    case "singleCall":return 0;
                    case "multiCall":return 1;
                    default:return 0;
                }
            }
        });
        CallBackServiceImpl b = (CallBackServiceImpl)enhancer.create();
        System.out.println(b.multiCall());
    }

    /**
     * 只执行原方法,不执行代理方法.
     */
    @Test
    public void testNoOp() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(CallBackServiceImpl.class);
        enhancer.setCallback(NoOp.INSTANCE);
        CallBackServiceImpl c = (CallBackServiceImpl)enhancer.create();
        System.out.println(c.noOpCall());
    }

    /**
     * 只执行代理的loadObject方法,不会执行原方法
     * @throws InterruptedException
     */
    @Test
    public void testFixedValueCall() throws InterruptedException {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(CallBackServiceImpl.class);
        enhancer.setCallback(new FixCallBack());
        CallBackServiceImpl d = (CallBackServiceImpl)enhancer.create();
        System.out.println(d.fixedValueCall());
        Thread.sleep(1000L);
        System.out.println(d.fixedValueCall());
    }

    /**
     * 相当于单例,2次的代理对象一样
     * @throws InterruptedException
     */
    @Test
    public void testLazyCall() throws InterruptedException {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(TestLog.class);
        enhancer.setCallback(new LazyLoaderCallBack());
        TestLog e = (TestLog)enhancer.create();
        System.out.println(e);
        Thread.sleep(1000L);
        System.out.println(e);
    }

    /**
     * 相当于多例,2次的代理对象不一样
     * @throws InterruptedException
     */
    @Test
    public void testDispatcherCallBack() throws InterruptedException {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(TestLog.class);
        enhancer.setCallback(new DispatcherCallBack());
        TestLog f = (TestLog)enhancer.create();
        System.out.println(f);
        Thread.sleep(1000L);
        System.out.println(f);
    }

    @Test
    public void testSpringInvocationHandlerCallBack() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(CallBackServiceImpl.class);
        enhancer.setCallback(new SpringInvocationHandlerCallBack(new CallBackServiceImpl()));
        CallBackServiceImpl g = (CallBackServiceImpl)enhancer.create();
        System.out.println(g.springInvocationHandlerCallBack());
        System.out.println("-----------------cglib包下的代理可以用2种方式生成调用--------------------");
        CallBackServiceImpl target = new CallBackServiceImpl();
        CallBackService ss = (CallBackService)Proxy.newProxyInstance(target.getClass().getClassLoader(),target.getClass().getInterfaces(),new SpringInvocationHandlerCallBack(target));
        System.out.println(ss.springInvocationHandlerCallBack());
    }

    @Test
    public void testJdkInvocationHandlerCallBack() {
        CallBackServiceImpl target = new CallBackServiceImpl();
        JdkInvocationHandlerCallBack jdk = new JdkInvocationHandlerCallBack(target);
        CallBackService proxy = (CallBackService)java.lang.reflect.Proxy.newProxyInstance(target.getClass().getClassLoader(),target.getClass().getInterfaces(),jdk);
        System.out.println(proxy.jdkInvocationHandlerCallBack());
    }

}
