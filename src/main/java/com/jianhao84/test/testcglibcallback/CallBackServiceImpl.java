package com.jianhao84.test.testcglibcallback;

import com.jianhao84.test.testcglibcallback.callback.LazyLoaderCallBack;
import lombok.Data;
import org.springframework.cglib.proxy.Enhancer;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-14
 * @time: 15:45
 */
@Data
public class CallBackServiceImpl implements CallBackService {

    /**测试单回调*/
    @Override
    public String singleCall() {
        return "单回调测试";
    }
    /**测试多回调*/
    @Override
    public String  multiCall() {
        return "多回调测试";
    }

    /**不处理回调*/
    @Override
    public String noOpCall() {
        return "不处理回调测试";
    }

    /**不处理回调*/
    @Override
    public String fixedValueCall() {
        return "固定值回调测试";
    }
    /**懒加载回调测试*/
    @Override
    public String lazyCall() {
        return "懒加载回调测试";
    }

    @Override
    public String springInvocationHandlerCallBack() {
        return "spring的InvocationHandler接口测试";
    }

    @Override
    public String jdkInvocationHandlerCallBack() {
        return "jdk的InvocationHandler接口测试";
    }
}
