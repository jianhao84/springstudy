package com.jianhao84.test.testcglibcallback.callback;

import com.jianhao84.test.testcglibcallback.TestLog;
import org.springframework.cglib.proxy.Dispatcher;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-15
 * @time: 10:52
 */
public class DispatcherCallBack implements Dispatcher {
    /**
     * 每次都会调用,代理对象不保存
     * @return
     * @throws Exception
     */
    @Override
    public Object loadObject() throws Exception {
        System.out.println("DispatcherCallBack.loadObject:" + new Date());
        TestLog log = new TestLog();
        log.setDate(new Date());
        return log;
    }
}
