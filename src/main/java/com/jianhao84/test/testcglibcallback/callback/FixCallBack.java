package com.jianhao84.test.testcglibcallback.callback;

import org.springframework.cglib.proxy.FixedValue;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:固定值callBack
 *
 * @author jianhao84
 * @date 2018-11-14
 * @time: 16:02
 */
public class FixCallBack implements FixedValue {
    @Override
    public Object loadObject() throws Exception {
        System.out.println("执行了FixCallBack");
        return new Date().toString();
    }
}
