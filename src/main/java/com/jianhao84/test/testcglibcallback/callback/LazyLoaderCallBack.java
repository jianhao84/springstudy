package com.jianhao84.test.testcglibcallback.callback;

import com.jianhao84.test.testcglibcallback.TestLog;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cglib.proxy.LazyLoader;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:懒加载callback
 *
 * @author jianhao84
 * @date 2018-11-14
 * @time: 16:03
 */
public class LazyLoaderCallBack implements LazyLoader {
    /**只会执行一次,返回的log对象会保存,下次直接用,相当于单例*/
    @Override
    public Object loadObject() throws Exception {
        System.out.println("LazyLoaderCallBack.loadObject:" + new Date());
        TestLog log = new TestLog();
        log.setDate(new Date());
        return log;
    }


}
