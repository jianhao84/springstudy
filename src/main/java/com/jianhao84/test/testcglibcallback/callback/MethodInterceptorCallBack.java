package com.jianhao84.test.testcglibcallback.callback;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * Description:为了方便展示cglib的calback的多回调,实现2个MethodInterceptor.
 *
 * @author jianhao84
 * @date 2018-11-14
 * @time: 15:58
 */
public class MethodInterceptorCallBack {
    public static class MethodInterceptorCallBackA implements MethodInterceptor {
        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            System.out.println("执行了MethodInterceptorA");
            return methodProxy.invokeSuper(o, objects);
        }
    }

    public static class MethodInterceptorCallBackB implements MethodInterceptor {
        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            System.out.println("执行了MethodInterceptorB");
            return methodProxy.invokeSuper(o, objects);
        }
    }


}
