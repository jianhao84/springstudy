package com.jianhao84.test.testcglibcallback.callback;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-15
 * @time: 16:55
 */
public class JdkInvocationHandlerCallBack implements InvocationHandler {
    private Object target;
    public JdkInvocationHandlerCallBack(Object target) {
        this.target = target;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("InvocationHandlerCallBack.invoke:代理");
        /**注意,这里一样要是执行target对象的方法,而不是proxy对象的方法,不然一直循环调用了*/
        return method.invoke(target,args);
    }
}
