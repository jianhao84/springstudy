package com.jianhao84.test.testcglibcallback;

import lombok.Data;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-15
 * @time: 10:49
 */
@Data
public class TestLog {
    private Date date;
}
