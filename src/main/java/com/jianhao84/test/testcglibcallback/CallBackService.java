package com.jianhao84.test.testcglibcallback;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-15
 * @time: 17:20
 */
public interface CallBackService {

    /**测试单回调*/
    public String singleCall();
    /**测试多回调*/
    public String  multiCall();

    /**不处理回调*/
    public String noOpCall();

    /**不处理回调*/
    public String fixedValueCall();
    /**懒加载回调测试*/
    public String lazyCall();

    public String springInvocationHandlerCallBack();

    public String jdkInvocationHandlerCallBack();

}
