package com.jianhao84.test.testnamed;

import lombok.Data;

import javax.inject.Named;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-30
 * @time: 17:46
 */
@Named("maomao")
@Data
public class NamedBean {
    private String a;

    public NamedBean() {
        System.out.println("初始化了");
        a = "毛毛";
    }
}
