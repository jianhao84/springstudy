package com.jianhao84.test.testnamed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-30
 * @time: 17:45
 */
@SpringBootApplication
public class NamedStart {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(NamedStart.class,args);
        System.out.println(((NamedBean)context.getBean("maomao")).getA());
    }

}
