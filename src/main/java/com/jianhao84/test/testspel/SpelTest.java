package com.jianhao84.test.testspel;

import com.jianhao84.test.testspel.pojo.Config;
import com.jianhao84.test.testspel.pojo.Person;
import com.sun.corba.se.spi.activation._InitialNameServiceImplBase;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-19
 * @time: 17:58
 */
public class SpelTest {
    @Test
    public void testVariableExpression() {
        ExpressionParser parser = new SpelExpressionParser();
        EvaluationContext context = new StandardEvaluationContext();
//        // context中set一个变量
//        context.setVariable("sayContent", "haha");
//        // 从context中get变量内容
//        String result1 = parser.parseExpression("#sayContent").getValue(context, String.class);
//        Assert.assertEquals("haha", result1);

        context = new StandardEvaluationContext(12);
        String result2 = parser.parseExpression("#root").getValue(context, String.class);
        System.out.println("result2:" + result2);
        context.setVariable("sex","男");
        String result3 = parser.parseExpression("#sex + #this").getValue(context, String.class);
        System.out.println("result3:" + result3);

        String result4 = parser.parseExpression("#this").getValue(context, String.class);
        System.out.println("result4:" + result4);
    }

    @Test
    public void testFunctionExpression() throws SecurityException, NoSuchMethodException {
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        Method parseInt = Integer.class.getDeclaredMethod("parseInt", String.class);
        context.registerFunction("parseInt", parseInt);
        context.setVariable("parseInt2", parseInt);
        int parse1 = parser.parseExpression("#parseInt('3')").getValue(context,int.class);
        int parse2 = parser.parseExpression("#parseInt2('3')").getValue(context,int.class);
        System.out.println(parse1);
        System.out.println(parse2);
    }

    @Test
    public void testAssignExpression() {
        ExpressionParser parser = new SpelExpressionParser();
        //1.给root对象赋值
        StandardEvaluationContext context = new StandardEvaluationContext("aaaa");
        String result1 = parser.parseExpression("#root='bbb'").getValue(context, String.class);
        System.out.println("#root='bbb':" + result1);
        String result6 = parser.parseExpression("#root").getValue(context, String.class);
        System.out.println("#root:" + result6);
        String result2 = parser.parseExpression("#this='ccc'").getValue(context, String.class);
        System.out.println("#this='ccc':" + result2);
        String result7 = parser.parseExpression("#this").getValue(context, String.class);
        System.out.println("#this:" + result7);
        //2.给自定义变量赋值
        context.setVariable("name", "dddd");
        context.setVariable("age","12");
        String result3 = parser.parseExpression("#name=#age").getValue(context, String.class);
        System.out.println("#name=#age:" + result3);
        String result5 = parser.parseExpression("#name").getValue(context, String.class);
        System.out.println("#name:" + result5);
        context.setRootObject("kkk");
        String result4 = parser.parseExpression("#name=#root").getValue(context, String.class);
        System.out.println("#name=#root:" + result4);
    }

    @Test
    public void test12() {
        ExpressionParser parser = new SpelExpressionParser();
        Person person = new Person("毛毛");
        StandardEvaluationContext context = new StandardEvaluationContext(person);
        //1.访问root对象属性:可以直接属性名,也可以通过#root.属性名访问.
        String result1 = parser.parseExpression("name").getValue(context, String.class);
        System.out.println(result1);
        String result2 = parser.parseExpression("#root.name").getValue(context, String.class);
        System.out.println(result2);
        //2.安全访问
        context.setRootObject(null);
        Integer result3 = parser.parseExpression("#root?.age").getValue(context, Integer.class);
        System.out.println(result3);
        try {
            //3.不安全访问
            Integer result4 = parser.parseExpression("#root.age").getValue(context, Integer.class);
            System.out.println(result4);
        }catch (Exception e) {
            System.out.println("报错了:" + e.getMessage());
        }
        //4.访问对象属性
        Person tt = new Person("周周");
        context.setVariable("tt",tt);
        String result5 = parser.parseExpression("#tt.name").getValue(context, String.class);
        System.out.println(result5);
        //5.设置对象属性
        parser.parseExpression("#tt.age").setValue(context,12);
        Integer result6 = parser.parseExpression("#tt.age").getValue(context, Integer.class);
        System.out.println(result6);
    }

    @Test
    public void test32() {
        ExpressionParser parser = new SpelExpressionParser();
        Person person = new Person("吉吉");
        StandardEvaluationContext context = new StandardEvaluationContext(person);
        // 根对象可以直接只写方法名.
        String result1 = parser.parseExpression("getName()").getValue(context, String.class);
        System.out.println(result1);
        Person tt = new Person("毛毛");
        context.setVariable("tt",tt);
        // 普通对象方法访问
        String result2 = parser.parseExpression("#tt.getName()").getValue(context, String.class);
        System.out.println(result2);
    }

    @Test
    public void testBeanExpression() {
        // spring容器
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);
        ExpressionParser parser = new SpelExpressionParser();
        // spel虚拟容器
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setBeanResolver(new BeanFactoryResolver(ctx));
        Person result1 = parser.parseExpression("@person").getValue(context, Person.class);
        System.out.println("result1:" + result1);
    }

    @Test
    public void test324() {
        ExpressionParser parser = new SpelExpressionParser();
        //将返回不可修改的空List
        List<Integer> result1 = parser.parseExpression("{}").getValue(List.class);
        System.out.println("result1:" + result1);

        //对于字面量列表也将返回不可修改的List
        List<Integer> result2 = parser.parseExpression("{3,6}").getValue(List.class);
        System.out.println("result2:" + result2);
        try {
            result2.add(22);
            System.out.println("try_result2:" + result2);
        }catch (Exception e) {
            System.out.println("List不可修改");
        }
        //对于列表中只要有一个不是字面量表达式，将只返回原始List，
        //不会进行不可修改处理
        String expression3 = "{{1+2,2+4},{3,4+4}}";
        List<List<Integer>> result3 = parser.parseExpression(expression3).getValue(List.class);
        System.out.println("result3:" + result3);
        // 定义中只要有一个不是字面常量,则可以修改(如1+2不是字面常量,而是一个表达式,所以result6可以修改).
        List<Integer> result6 = parser.parseExpression("{1+2,6}").getValue(List.class);
        System.out.println("result6:" + result6);
        result6.add(43);
        System.out.println("modify_result6:" + result6);
    }

    @Test
    public void test432() {
        ExpressionParser parser = new SpelExpressionParser();
        //声明一个大小为2的一维数组并初始化
        Integer[] result4 = parser.parseExpression("new Integer[2]{1,2}").getValue(Integer[].class);
        System.out.println("result4:" + Arrays.toString(result4));
        result4[1] = 32323;
        System.out.println("modify_result4:" + Arrays.toString(result4));
        //定义一维数组但不初始化
        Integer[] result5 = parser.parseExpression("new Integer[1]").getValue(Integer[].class);
        System.out.println("result5:" + Arrays.toString(result5));
        //定义多维数组但不初始化
        int[][][] result3 = parser.parseExpression("new int[1][2][3]").getValue(int[][][].class);
        System.out.println("result3:" + result3);
        //错误的定义多维数组，多维数组不能初始化
        String expression4 = "new int[1][2][3]{{1}{2}{3}}";
        try {
            int[][][] result6 = parser.parseExpression(expression4).getValue(int[][][].class);
        } catch (Exception e) {
            System.out.println("数组定义错误");
        }
    }

    @Test
    public void test23() {
        ExpressionParser parser = new SpelExpressionParser();
        //SpEL内联List访问index为0的元素
        int result1 = parser.parseExpression("{1,2,3}[0]").getValue(int.class);
        System.out.println("result1:" + result1);

        //SpEL目前支持所有集合类型的访问
        Collection<Integer> collection = new HashSet<Integer>();
        collection.add(1);
        collection.add(2);
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("collection", collection);
        // 访问变量collection的index为1的元素
        int result2 = parser.parseExpression("#collection[1]").getValue(context, int.class);
        System.out.println("result2:" + result2);

        //SpEL对Map字典元素访问的支持
        Map<String, Integer> map = new HashMap();
        map.put("a", 1);
        EvaluationContext context3 = new StandardEvaluationContext();
        context3.setVariable("map", map);
        // 访问Map类型变量map的key为"a"的元素.
        int result3 = parser.parseExpression("#map['a']").getValue(context3, int.class);
        System.out.println("result3:" + result3);
    }

    @Test
    public void test435() {
        ExpressionParser parser = new SpelExpressionParser();
        //1.修改数组元素值
        int[] array = new int[] {1, 2};
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("array", array);
        // 通过表达式修改
        parser.parseExpression("#array[1] = 3").getValue(context, int.class);
        System.out.println("modify_array1:" + Arrays.toString(array));
        // 通过setValue修改
        parser.parseExpression("#array[1]").setValue(context,4);
        System.out.println("modify_array2:" + Arrays.toString(array));
        //2.修改集合值
        Collection<Integer> collection = new ArrayList<Integer>();
        collection.add(1);
        collection.add(2);
        context.setVariable("collection", collection);
        // 通过表达式修改
        parser.parseExpression("#collection[1] = 3").getValue(context, int.class);
        System.out.println("modify_collection1:" + collection.toString());
        // 通过setValue修改
        parser.parseExpression("#collection[1]").setValue(context, 4);
        System.out.println("modify_collection2:" + collection.toString());
        //3.修改map
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("a", 1);
        context.setVariable("map", map);
        parser.parseExpression("#map['a'] = 2").getValue(context, int.class);
        System.out.println("modify_map1:" + map);
        parser.parseExpression("#map['a']").setValue(context,3);
        System.out.println("modify_map2:" + map);
    }

    @Test
    public void test3324() {
        ExpressionParser parser = new SpelExpressionParser();
        //1.首先准备测试数据
        Collection<Integer> collection = new ArrayList();
        collection.add(4);
        collection.add(5);
        Map<String, Integer> map = new HashMap();
        map.put("a", 1);
        map.put("b", 2);
        //2.测试集合或数组
        EvaluationContext context1 = new StandardEvaluationContext();
        context1.setVariable("collection", collection);
        // 对集合中所有元素进行+1操作,其中#this代表集合中所有迭代元素.
        Collection<Integer> result1 = parser.parseExpression("#collection.![#this+1]").getValue(context1, Collection.class);
        System.out.println(result1);
        //3.测试字典:SpEL投影运算还支持Map投影,但Map投影最终只能得到List结果
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("map", map);
        //对于投影表达式中的“#this”将是Map.Entry,所以可以使用“value”来获取值,使用“key”来获取键
        List<Integer> result2 = parser.parseExpression("#map.![ value+1]").getValue(context, List.class);
        System.out.println(result2);
        //对于投影表达式中的“#this”将是Map.Entry,所以可以使用“value”来获取值,使用“key”来获取键.
        List<Integer> result3 = parser.parseExpression("#map.![ key+key]").getValue(context, List.class);
        System.out.println(result3);
        // #this表示键值对,可以用#this.key和#this.value代表key和value,#this可以省略.
        List<Integer> result4 = parser.parseExpression("#map.![#this.key+1]").getValue(context, List.class);
        System.out.println(result4);
    }

    @Test
    public void test3213() {
        ExpressionParser parser = new SpelExpressionParser();
        //1.首先准备测试数据
        List<Integer> collection = new ArrayList();
        collection.add(4);
        collection.add(5);
        Map<String, Integer> map = new HashMap();
        map.put("a", 1);
        map.put("b", 2);
        //2.集合或数组测试
        EvaluationContext context1 = new StandardEvaluationContext();
        context1.setVariable("collection", collection);
        // 只选择大于4的值到新的list中,#this放在[]中代表每个元素.
        List<Integer> result1 = parser.parseExpression("#collection.?[#this>4]").getValue(context1, List.class);
        System.out.println(result1);
        //3.字典测试
        EvaluationContext context2 = new StandardEvaluationContext();
        context2.setVariable("map", map);
        // #this放在map的[]中代表每个map元素,即键值对.这里是筛选key不为'a'的元素到新的map中.
        Map<String, Integer> result2 = parser.parseExpression("#map.?[#this.key != 'a']").getValue(context2, Map.class);
        System.out.println(result2);
        // 选择和投影一起用:选择key不为'a'的元素,并将所有满足条件的元素value+1并投影为list.
        List<Integer> result3 = parser.parseExpression("#map.?[key != 'a'].![value+1]").getValue(context2, List.class);
        System.out.println(result3);
    }

    @Test
    public void testParserContext() {
        // 创建解析器
        ExpressionParser parser = new SpelExpressionParser();
        // 1.自定义模版
        ParserContext parserContext = new ParserContext() {
            @Override
            public boolean isTemplate() {
                // 定义字符串表达式是模版
                return true;
            }
            @Override
            public String getExpressionPrefix() {
                // 定义表达式前缀
                return "{";
            }
            @Override
            public String getExpressionSuffix() {
                // 定义表达式后缀
                return "}";
            }
        };
        String template = "{'Hello '}{'World!'}";
        Expression expression = parser.parseExpression(template, parserContext);
        System.out.println(expression.getValue());

        // 2.使用默认的模版TemplateParserContext.
        String greetingExp = "Hello, #{ #user }";
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("user", "苍老师");
        String result = parser.parseExpression(greetingExp,new TemplateParserContext()).getValue(context, String.class);
        System.out.println(result);
    }

    public static void main(String[] args) {
        String greetingExp = "Hello, #{ #user }";
        ExpressionParser parser = new SpelExpressionParser();
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("user", "苍老师");
        // 用默认的模版TemplateParserContext
        Expression expression = parser.parseExpression(greetingExp,new TemplateParserContext());
        System.out.println(expression.getValue(context, String.class));
    }
}
