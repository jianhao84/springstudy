package com.jianhao84.test.testspel.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-27
 * @time: 15:35
 */
@Data
public class Person {
    private String name;
    private Integer age;

    public Person() {
        this.name = "默认名字";
    }
    public Person(String name) {
        this.name = name;
    }
}
