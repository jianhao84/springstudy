package com.jianhao84.test.testspel.pojo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-27
 * @time: 17:04
 */
@Configuration
public class Config {

    @Bean(value = "person")
    public Person getPerson() {
        return new Person("spring实例化默认名");
    }
}
