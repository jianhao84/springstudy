package com.jianhao84.test.testSingle.impl;

import com.jianhao84.test.testSingle.DemoService;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:spring的bean的单例测试
 * 结论:由于spring的bean默认是单例,可能会产生成员变量混乱,解决办法2个:
 * 1.使用@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)将bean设置为非单例.
 * 2.用ThreadLocal.
 *
 * @author jianhao84
 * @date 2018-10-15
 * @time: 17:05
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE) // spring默认是单例,这里设置为非单例,就不会count混乱.
public class DemoServiceImpl implements DemoService {

    private  ThreadLocal<Integer> localCount = new ThreadLocal();

    private int count;


    @Override
    @Async
    public void print() throws InterruptedException {
        count = 0;
        localCount.set(0);
        Thread.sleep(2000);
        count++;
        System.out.println(Thread.currentThread().getName() + "  count    " + count);
        localCount.set(localCount.get()+1);
        System.out.println(Thread.currentThread().getName() + "  localCount    " + localCount.get());
    }
}
