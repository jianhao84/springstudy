package com.jianhao84.test.testSingle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-15
 * @time: 17:11
 */
@SpringBootApplication
@EnableAsync
public class TestStart {

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext context = SpringApplication.run(TestStart.class,args);
        context.getBean(DemoService.class).print();
        context.getBean(DemoService.class).print();
        context.getBean(DemoService.class).print();
        context.getBean(DemoService.class).print();
        context.getBean(DemoService.class).print();


        context.getBean(SingleTest.class).test();
        context.getBean(SingleTest.class).test();
        context.getBean(SingleTest.class).test();

    }
}
