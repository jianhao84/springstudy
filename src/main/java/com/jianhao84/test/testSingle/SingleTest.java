package com.jianhao84.test.testSingle;

import org.springframework.stereotype.Component;

import javax.inject.Singleton;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-31
 * @time: 10:40
 */
@Singleton
@Component
public class SingleTest {

    private int count = 0;

    public void test() {
        count++;
        System.out.println(count);
    }
}
