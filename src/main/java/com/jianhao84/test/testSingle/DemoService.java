package com.jianhao84.test.testSingle;

/**
 * Created with IntelliJ IDEA.
 * Description: bean的单例测试
 *
 * @author jianhao84
 * @date 2018-10-15
 * @time: 17:05
 */
public interface DemoService  {

    public void print() throws InterruptedException;
}
