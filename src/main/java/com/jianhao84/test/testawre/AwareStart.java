package com.jianhao84.test.testawre;

import com.jianhao84.test.testawre.beanware.TestBeanNameAware;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-05
 * @time: 9:55
 */
@SpringBootApplication
public class AwareStart {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(AwareStart.class,args);
        context.getBean(TestBeanNameAware.class).show();
    }
}
