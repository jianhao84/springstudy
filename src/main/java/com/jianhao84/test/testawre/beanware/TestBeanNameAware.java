package com.jianhao84.test.testawre.beanware;


import org.springframework.beans.factory.BeanNameAware;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:通过实现BeanNameAware接口的方法得到beanName.
 *
 * @author jianhao84
 * @date 2018-11-05
 * @time: 10:13
 */
@Component
public class TestBeanNameAware implements BeanNameAware {
    private String beanName;

    @Override
    public void setBeanName(String name) {
        this.beanName = name;
    }

    public void show() {
        System.out.println("beanName:" + beanName);
    }
}
