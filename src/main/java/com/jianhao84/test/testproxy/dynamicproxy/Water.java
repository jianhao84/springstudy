package com.jianhao84.test.testproxy.dynamicproxy;

/**
 * Created with IntelliJ IDEA.
 * Description:水的接口
 *
 * @author jianhao84
 * @date 2018-11-08
 * @time: 17:46
 */
public interface Water {
    void drink();
}
