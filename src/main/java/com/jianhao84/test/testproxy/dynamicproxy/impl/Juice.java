package com.jianhao84.test.testproxy.dynamicproxy.impl;


import com.jianhao84.test.testproxy.dynamicproxy.Water;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:果汁目标类
 *
 * @author jianhao84
 * @date 2018-11-08
 * @time: 17:49
 */
@Component
public class Juice implements Water {
    @Override
    public void drink() {
        System.out.println("喝果汁...");
    }
}
