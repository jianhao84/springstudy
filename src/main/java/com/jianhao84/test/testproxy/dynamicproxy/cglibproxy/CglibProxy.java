package com.jianhao84.test.testproxy.dynamicproxy.cglibproxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * Description:cglib动态代理
 * 1.直接用spring-core中的cglib包和asm包,也可以单独引入asm包和cglib包.
 * 2.实现MethodInterceptor接口的intercept方法.
 * 3.通过Enhancer对象提供创建代理方法.
 * 4.提供增强方法.
 *
 * @author jianhao84
 * @date 2018-11-09
 * @time: 10:33
 */
public class CglibProxy implements MethodInterceptor {
    public  Object  createProxy(Class clazz){
        /**在enhancer中有一个setCallBack(this)方法,这样就实现了代理类和委托类的关联*/
        Enhancer enhancer=new Enhancer();
        //设置公共的接口或者公共的类
        enhancer.setSuperclass(clazz);
        //建立关联关系,setCallback需要实现了CallBack接口的对象(在该对象中有对代理的增强)
        enhancer.setCallback(this);
        return enhancer.create();
    }
    /**
     * @param obj   被代理的目标对象
     * @param method    被代理的目标对象的方法
     * @param args  方法参数
     * @param methodProxy   方法代理
     * @throws Throwable
     */
    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        try{
            /**在方法执行前手动增强被代理对象的方法,spring的aop通过@Before(表达式)来指定在什么连接点(时机)进行什么通知(指定切入点做什么事)*/
            shake();
            /**执行被代理对象的方法*/
            Object result= methodProxy.invokeSuper(obj,args);
            /**返回执行结果*/
            return result;
            /**这个不知道怎么实现:在被代理对象的方法被执行完成后增强方法,相当于spring的@AfterRunning(表达式)*/
        }catch (Throwable e) {
            e.printStackTrace();
            /**如果这里也有增强方法,则相当于spring的@AfterThrowing()*/
            return null;
        }finally {
            /**在方法执行后手动增强被代理对象的方法,无论是否异常都执行,相当于spring的@After(表达式)*/
            cover();
        }
    }
    /**前置增强方法:喝前摇一摇*/
    private void shake() {
        System.out.println("前置增强->喝前摇一摇...");
    }
    /**后置增强方法:喝完盖上盖子*/
    private void cover() {
        System.out.println("后置增强->喝完盖上....");
    }
    /**异常增强方法:中毒了*/
    private void poisoning() {
        System.out.println("妈的中毒了...");
    }
}
