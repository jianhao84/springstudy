package com.jianhao84.test.testproxy.dynamicproxy;

import com.jianhao84.test.testproxy.dynamicproxy.cglibproxy.CglibProxy;
import com.jianhao84.test.testproxy.dynamicproxy.impl.Beer;
import com.jianhao84.test.testproxy.dynamicproxy.impl.Juice;
import com.jianhao84.test.testproxy.dynamicproxy.jdkproxy.JdkProxy;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-09
 * @time: 10:11
 */
public class Dynamic {

    /**
     * jdk动态代理test
     */
    @Test
    public void testJdkProxy() {
        JdkProxy jdkProxy = new JdkProxy();
        Water beer = (Water)jdkProxy.createProxy(new Beer());
        beer.drink();
        System.out.println("-----------------------------------");
        Water juice = (Water)jdkProxy.createProxy(new Juice());
        juice.drink();
    }

    @Test
    public void testCglibProxy() {
        CglibProxy cglibProxy = new CglibProxy();
        Water beer = (Water)cglibProxy.createProxy(Beer.class);
        beer.drink();
        System.out.println("===================================");
        Water juice = (Water)cglibProxy.createProxy(Juice.class);
        juice.drink();
    }
}
