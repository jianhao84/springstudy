package com.jianhao84.test.testproxy.dynamicproxy.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created with IntelliJ IDEA.
 * Description:JDK动态代理类:
 * 0.新建目标对象变量,新建创建代理方法,并将需要代理的目标对象复制给新建的目标对象变量.
 * 1.实现InvocationHandler接口的invoke方法.
 * 2.在invoke方法中result= method.invoke(target,args)并返回result.
 * 3.在invoke中可以增强被代理对象的方法.
 *
 * @author jianhao84
 * @date 2018-11-08
 * @time: 17:52
 */
public class JdkProxy implements InvocationHandler {
    /**需要代理的目标对象:不确定代理哪个类,所以用Object*/
    private Object target;
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try{
            /**在方法执行前手动增强被代理对象的方法,spring的aop通过@Before(表达式)来指定在什么连接点(时机)进行什么通知(指定切入点做什么事)*/
            shake();
            /**执行被代理对象的方法*/
            Object result= method.invoke(target,args);
            /**返回执行结果*/
            return result;
            /**这个不知道怎么实现:在被代理对象的方法被执行完成后增强方法,相当于spring的@AfterRunning(表达式)*/
        }catch (Throwable e) {
            e.printStackTrace();
            /**如果这里也有增强方法,则相当于spring的@AfterThrowing()*/
            return null;
        }finally {
            /**在方法执行后手动增强被代理对象的方法,无论是否异常都执行,相当于spring的@After(表达式)*/
            cover();
        }
    }
    /**将目标对象传入进行代理*/
    public Object createProxy(Object target){
        this.target = target;
        // 返回代理对象
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(), this);
    }
    /**前置增强方法:喝前摇一摇*/
    private void shake() {
        System.out.println("前置增强->喝前摇一摇...");
    }
    /**后置增强方法:喝完盖上盖子*/
    private void cover() {
        System.out.println("后置增强->喝完盖上....");
    }
    /**异常增强方法:中毒了*/
    private void poisoning() {
        System.out.println("妈的中毒了...");
    }
}
