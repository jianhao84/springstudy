package com.jianhao84.test.testproxy.staticproxy;

import com.jianhao84.test.testproxy.staticproxy.impl.Dog;
import com.jianhao84.test.testproxy.staticproxy.proxy.DogProxy;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-08
 * @time: 16:31
 */
public class StaticProxyTest {

    @Test
    public void test() {
        DogProxy dogProxy = new DogProxy();
        Dog dog = new Dog();
        dog.setName("毛毛");
        dogProxy.setDog(dog);
        dogProxy.eatFood();
    }
}
