package com.jianhao84.test.testproxy.staticproxy.proxy;

import com.jianhao84.test.testproxy.staticproxy.Animal;
import com.jianhao84.test.testproxy.staticproxy.impl.Dog;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:狗类的代理类,对狗类的eatFood方法进行增强,增加了主人召唤和主人离开功能,但不对狗类代码进行修改
 *
 * @author jianhao84
 * @date 2018-11-08
 * @time: 16:23
 */
@Data
public class DogProxy implements Animal {
    private Dog dog;
    @Override
    public void eatFood() {
        masterCallUp();
        dog.eatFood();
        masterLeave();
    }
    private void masterCallUp() {
        System.out.println("狗" + dog.getName() + "的主人在召唤...");
    }
    private void masterLeave() {
        System.out.println("狗" + dog.getName() + "的主人离开了...");
    }
}
