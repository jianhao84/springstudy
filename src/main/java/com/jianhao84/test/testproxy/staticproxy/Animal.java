package com.jianhao84.test.testproxy.staticproxy;

/**
 * Created with IntelliJ IDEA.
 * Description:动物接口
 *
 * @author jianhao84
 * @date 2018-11-08
 * @time: 16:20
 */
public interface Animal {
    public void eatFood();
}
