package com.jianhao84.test.testproxy.staticproxy.impl;

import com.jianhao84.test.testproxy.staticproxy.Animal;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:狗类,实现动物接口
 *
 * @author jianhao84
 * @date 2018-11-08
 * @time: 16:21
 */
@Data
public class Dog implements Animal {
    private String name;
    @Override
    public void eatFood() {
        System.out.println("名字叫" + name + "的狗在吃食物...");
    }
}
