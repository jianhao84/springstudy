package com.jianhao84.test.testvalue.value;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-12
 * @time: 11:03
 */
@Data
@Component
public class PojoBean {
    private String nickName;
    public PojoBean() {
        this.nickName="周剑";
    }
    public String sayHello() {
        return nickName.concat(",hello!");
    }
    public String fuck(String name) {
        return nickName.concat(" fuck").concat(" ").concat(name);
    }
}
