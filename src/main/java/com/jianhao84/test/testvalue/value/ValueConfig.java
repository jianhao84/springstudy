package com.jianhao84.test.testvalue.value;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-12
 * @time: 10:50
 */
@Data
@Configuration
@PropertySource("test.properties")
public class ValueConfig {
    // 1.@Value标签读取配置文件的配置信息
    @Value("${lalaname}")
    private String lalaname;
    // 2.读取spring容器中其他bean的属性
    @Value("#{pojoBean.nickName}")
    private String nickName;
    // 3.调用其他bean的无参方法
    @Value("#{pojoBean.sayHello()}")
    private String sayContent;
    // 3.调用其他bean的带参方法
    @Value("#{pojoBean.fuck('妹子')}")
    private String fuckContent;
}
