package com.jianhao84.test.testvalue;

import com.jianhao84.test.testvalue.value.ValueConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-12
 * @time: 10:47
 */
@SpringBootApplication
public class TestStart {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(TestStart.class,args);
        System.out.println(context.getBean(ValueConfig.class));
    }
}
