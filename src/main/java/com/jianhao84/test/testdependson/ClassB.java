package com.jianhao84.test.testdependson;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-17
 * @time: 9:52
 */
@Component
@Lazy
public class ClassB {

    public ClassB() {
        System.out.println("加载classB...");
    }
}
