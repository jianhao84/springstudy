package com.jianhao84.test.testdependson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-17
 * @time: 10:17
 */
@SpringBootApplication
public class Start {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Start.class,args);
    }
}
