package com.jianhao84.test.testdependson;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-17
 * @time: 9:51
 */
@Component
@DependsOn(value = {"classB"})
public class ClassA {

    public ClassA() {
        System.out.println("加载classA...");
    }
}
