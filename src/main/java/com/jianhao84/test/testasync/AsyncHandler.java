package com.jianhao84.test.testasync;

import com.jianhao84.common.BusinessException;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-19
 * @time: 11:32
 */
public class AsyncHandler implements AsyncUncaughtExceptionHandler {
    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... params) {
        if (ex instanceof BusinessException) {
            // 业务异常处理逻辑
            System.out.println("业务异常处理逻辑" + ex.getMessage());
        }else {
            // 未知异常处理逻辑
            System.out.println("未知异常处理逻辑" + ex.getMessage());
        }
        ex.printStackTrace();
    }
}
