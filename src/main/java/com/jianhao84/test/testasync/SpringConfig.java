package com.jianhao84.test.testasync;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-18
 * @time: 16:38
 */
@Configuration
public class SpringConfig  {
    @Bean
    public Executor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        /**线程池维护线程的最小数量*/
        taskExecutor.setCorePoolSize(2);
        /**线程池维护线程的最大数量*/
        taskExecutor.setMaxPoolSize(5);
        /**线程池维护线程所允许的空闲时间*/
        taskExecutor.setKeepAliveSeconds(1);
        /**线程池所使用的缓冲队列大小*/
        taskExecutor.setQueueCapacity(17);
        /**用来拒绝一个任务的执行的处理handler,可以自己实现RejectedExecutionHandler接口*/
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        /**线程前缀*/
        taskExecutor.setThreadNamePrefix("spring_bean_method-async-thread-pool-");
        return taskExecutor;
    }
}
