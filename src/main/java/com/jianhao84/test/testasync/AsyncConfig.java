package com.jianhao84.test.testasync;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-18
 * @time: 16:01
 */
@Component
public class AsyncConfig implements AsyncConfigurer {

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        /**线程池维护线程的最小数量*/
        executor.setCorePoolSize(2);
        /**线程池维护线程的最大数量*/
        executor.setMaxPoolSize(5);
        /**线程池维护线程所允许的空闲时间*/
        executor.setKeepAliveSeconds(1000);
        /**线程池所使用的缓冲队列大小*/
        executor.setQueueCapacity(17);
        /**用来拒绝一个任务的执行的处理handler,可以自己实现RejectedExecutionHandler接口*/
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        /**线程前缀*/
        executor.setThreadNamePrefix("implements-method-Thread-");
        /**实现AsyncConfigurer接口或继承AsyncConfigurerSupport方式的getAsyncExecutor必须要initialize,不然会报找不到执行器*/
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new AsyncHandler();
    }
}
