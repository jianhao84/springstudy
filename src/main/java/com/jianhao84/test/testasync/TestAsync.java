package com.jianhao84.test.testasync;

import com.alibaba.fastjson.JSONObject;
import com.jianhao84.common.BusinessException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-17
 * @time: 16:22
 */
@Component
public class TestAsync {

    @Async
    public void test() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "-----------");
    }

    @Async
    public void test3() {
        try {
            throw new BusinessException(20500,"aaaaaaaaaaaaaaaa");
        }catch (Exception e) {

        }

//        System.out.println(2/0);
    }


    @Async
    public Future<String> test2() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("thread", Thread.currentThread().getName());
        jsonObject.put("time", System.currentTimeMillis());
        return new AsyncResult<String>(jsonObject.toJSONString());
    }


}
