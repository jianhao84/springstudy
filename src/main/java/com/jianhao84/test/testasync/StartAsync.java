package com.jianhao84.test.testasync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-17
 * @time: 16:20
 */
@SpringBootApplication
@EnableAsync
public class StartAsync {


    public static void main(String[] args) throws Exception {
        ApplicationContext context = SpringApplication.run(StartAsync.class,args);

//        for(int i = 0;i < 22; i++) {
//            context.getBean(TestAsync.class).test();
//            System.out.println("-----------" + i);
//        }

        context.getBean(TestAsync.class).test3();

//        System.out.println("开始执行异步任务");
//        Future<String> future = context.getBean(TestAsync.class).test2();
//        while (true) {
//            if(future.isCancelled()){
//                System.out.println("异步任务已被取消");
//                break;
//            }
//            if (future.isDone() ) {
//                System.out.println("异步任务已执行完成");
//                System.out.println("返回结果是: " + future.get());
//                break;
//            }
//            System.out.println("异步任务执行中...");
//            Thread.sleep(1000);
//        }

//          Thread.sleep(10000);
        SpringApplication.exit(context);
    }
}
