package com.jianhao84.test.testapplicationContext;

import lombok.Data;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:获取applicationContext的方式,测试入口在启动类
 *
 * @author jianhao84
 * @date 2018-10-10
 * @time: 17:00
 */
@Component
@Data
public class ApplicationContextUtil implements ApplicationContextAware {

    @Autowired
    private ApplicationContext applicationContext_autowired;
    private ApplicationContext applicationContext_implements;
    private ApplicationContext applicationContext_struct;

    /**
     * 展示测试结果方法
     */
    public void show() {
        show_autowired();
        show_struct();
        show_implements();
    }

    /**
     * 直接注入方式
     */
    public void show_autowired() {
        System.out.println("----------直接注入方式方式获取applicationContext------------------");
        System.out.println("byName方式获取bean:========================:" + applicationContext_autowired.getBean("testBean"));
        System.out.println("byType方式获取bean:========================:" + applicationContext_autowired.getBean(TestBean.class));
    }

    /**
     * 构造器传入方式
     */
    public void  show_struct() {
        System.out.println("----------构造器传入方式获取applicationContext------------------");
        System.out.println("byName方式获取bean:========================:" + applicationContext_struct.getBean("testBean"));
        System.out.println("byType方式获取bean:========================:" + applicationContext_struct.getBean(TestBean.class));
    }

    /**
     * 实现ApplicationContextAware接口方式
     */
    public void  show_implements() {
        System.out.println("----------实现ApplicationContextAware接口方式获取applicationContext------------------");
        System.out.println("byName方式获取bean:========================:" + applicationContext_implements.getBean("testBean"));
        System.out.println("byType方式获取bean:========================:" + applicationContext_implements.getBean(TestBean.class));
    }

    /**
     *使用构造器,在构造器里传入applicationContext
     * @param applicationContext
     */
    public ApplicationContextUtil(ApplicationContext applicationContext) {
        this.applicationContext_struct = applicationContext;
    }

    /**
     *实现ApplicationContextAware接口setApplicationContext方法
     * @param applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext_implements = applicationContext;
    }
}
