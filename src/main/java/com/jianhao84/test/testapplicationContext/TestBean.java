package com.jianhao84.test.testapplicationContext;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 17:29
 */
@Component
@Data
public class TestBean {
    private String msg="啦啦啦啦";
}
