package com.jianhao84.test.testAmbiguity;

/**
 * Created with IntelliJ IDEA.
 * Description:接口
 *
 * @author jianhao84
 * @date 2018-10-15
 * @time: 14:40
 */
public interface BaseInterface {

    public void showName(String name);

}
