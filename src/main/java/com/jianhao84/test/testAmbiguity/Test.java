package com.jianhao84.test.testAmbiguity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-15
 * @time: 14:56
 */
@Component
public class Test {

    @Autowired
    @Qualifier("englishShow")
    BaseInterface baseInterface;

    public void test() {
        baseInterface.showName("周剑");
    }
}
