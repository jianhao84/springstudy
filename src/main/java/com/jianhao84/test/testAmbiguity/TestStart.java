package com.jianhao84.test.testAmbiguity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-15
 * @time: 14:45
 */
@SpringBootApplication
public class TestStart {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(TestStart.class,args);
        // 测试
        context.getBean(Test.class).test();
        context.getBean(Test.class).test();
    }
}
