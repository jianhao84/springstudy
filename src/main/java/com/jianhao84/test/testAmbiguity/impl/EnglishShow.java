package com.jianhao84.test.testAmbiguity.impl;

import com.jianhao84.test.testAmbiguity.BaseInterface;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-15
 * @time: 14:41
 */
@Component
public class EnglishShow implements BaseInterface {

    @Override
    public void showName(String name) {
        System.out.println("welcome to EnclishShow, ".concat(name));
    }
}
