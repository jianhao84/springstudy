package com.jianhao84.test.testAmbiguity.impl;

import com.jianhao84.test.testAmbiguity.BaseInterface;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-15
 * @time: 14:44
 */
@Component
@Primary
public class ChinaShow implements BaseInterface {
    @Override
    public void showName(String name) {
        System.out.println("欢迎来到中文秀, ".concat(name));
    }
}
