package com.jianhao84.test.testchain;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-13
 * @time: 10:45
 */
public class ChainTest {
    class HandlerDrink extends BaseChainHandler{
        @Override
        protected void handlerProcess() {
            System.out.println("喝茶");
        }
    }

    class HandlerTalk extends BaseChainHandler{
        @Override
        protected void handlerProcess() {
            System.out.println("扯淡");
        }
    }

    class HandlerSleep extends BaseChainHandler{
        @Override
        protected void handlerProcess() {
            System.out.println("睡觉");
        }
    }

    @Test
    public void testChain() {
        List<BaseChainHandler> handlers = Arrays.asList(
                new HandlerDrink(),new HandlerSleep(),new HandlerTalk()
        );
        Chain chain = new Chain(handlers);
        chain.process();
    }

}
