package com.jianhao84.test.testchain;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-13
 * @time: 10:39
 */
public abstract class BaseChainHandler {

    protected abstract void handlerProcess();

    public void  execute(Chain chain) {
        handlerProcess();
        chain.process();
    }
}
