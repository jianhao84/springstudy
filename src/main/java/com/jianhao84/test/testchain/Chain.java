package com.jianhao84.test.testchain;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:封装了多个handler的链对象.
 *
 * @author jianhao84
 * @date 2018-11-13
 * @time: 10:39
 */
public class Chain {

    private List<BaseChainHandler> handlers;

    private int index = 0;

    public Chain(List<BaseChainHandler> handlers) {
        this.handlers = handlers;
    }

    public void process() {
        if(index >= handlers.size()) {
            return;
        }
        handlers.get(index++).execute(this);
    }

}
