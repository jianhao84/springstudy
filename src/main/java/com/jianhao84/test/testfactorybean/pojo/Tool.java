package com.jianhao84.test.testfactorybean.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-02
 * @time: 9:46
 */
@Data
@AllArgsConstructor
public class Tool {
    private int id;
}
