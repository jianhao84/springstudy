package com.jianhao84.test.testfactorybean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-02
 * @time: 9:33
 */
@SpringBootApplication
public class FactoryBeanStart {
    public static void main(String[] args) {
        SpringApplication.run(FactoryBeanStart.class,args);
    }
}
