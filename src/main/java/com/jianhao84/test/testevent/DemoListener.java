package com.jianhao84.test.testevent;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-09
 * @time: 14:18
 */
@Component
public class DemoListener implements ApplicationListener<DemoEvent> {
    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(DemoEvent event) {
        System.out.println("demoListener接受到了demoPublisher发布的消息："+event.getSource());
    }
}
