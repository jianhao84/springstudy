package com.jianhao84.test.testevent;

import lombok.Data;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ApplicationContextEvent;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-09
 * @time: 14:17
 */
@Data
public class DemoEvent2 extends ApplicationContextEvent {
    private static final long serialVersionUID = -374205795827574569L;
    private String msg;


    public DemoEvent2(ApplicationContext source, String msg) {
        super(source);
        this.msg = msg;
    }

}
