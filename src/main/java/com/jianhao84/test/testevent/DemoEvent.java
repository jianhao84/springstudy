package com.jianhao84.test.testevent;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-09
 * @time: 14:17
 */
@Data
public class DemoEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public DemoEvent(Object source) {
        super(source);
    }
}
