package com.jianhao84.test.testthreadpoolexecutor;

import java.util.concurrent.*;

public class ThreadPoolTest {

    public static final int CPU_CORE_CONT = Runtime.getRuntime().availableProcessors();
    public static final int MAX_POOL_SIZE = 10;
    public static final int TIME_OUT = 30;

    public static void main(String[] args) {
        //CPU密集型推荐设置线
        int corePoolSize = CPU_CORE_CONT + 1;
        //IO密集型推荐设置
        //int ioCorePoolSize = 2 * CPU_CORE_CONT;
        //数组实现的阻塞队列，性能较差，使用较少
        BlockingQueue<Runnable> arrayBlockQueue = new ArrayBlockingQueue(20);
        //链表实现的阻塞队列
        //BlockingQueue linkedBockingQueue = new LinkedBlockingQueue(20);
        //同步阻塞队列
        //BlockingQueue synchronousQueue = new SynchronousQueue();
        //优先级阻塞队列，使用较少
        //BlockingQueue priorityBlockingQueue = new PriorityBlockingQueue();

        ExecutorService executor = new ThreadPoolExecutor(corePoolSize, MAX_POOL_SIZE, TIME_OUT, TimeUnit.SECONDS, arrayBlockQueue);
        executor.execute(new WorkerThread("thread1"));
//        executor.shutdown();

    }

    static class WorkerThread implements Runnable {
        private String name;

        WorkerThread(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            System.out.println(name + "is running");
        }
    }

}


