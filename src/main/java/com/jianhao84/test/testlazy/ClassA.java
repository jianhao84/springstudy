package com.jianhao84.test.testlazy;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-17
 * @time: 10:52
 */
//@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Lazy
public class ClassA {

    public ClassA() {
        System.out.println("ClassA........" + this.toString());
    }
}
