package com.jianhao84.test.testlazy;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-17
 * @time: 10:53
 */
@Component
public class ClassB {

    @Autowired
    BeanFactory beanFactory;

    public void test() {
        ClassA classA = beanFactory.getBean(ClassA.class);
        System.out.println(classA);
    }
}
