package com.jianhao84.test.testlazy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-17
 * @time: 10:57
 */
@SpringBootApplication
public class LazyStart {


    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(LazyStart.class,args);
        System.out.println("----------启动完成");
        context.getBean(ClassB.class).test();
        SpringApplication.exit(context);
    }
}
