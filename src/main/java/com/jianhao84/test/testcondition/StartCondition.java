package com.jianhao84.test.testcondition;

import com.jianhao84.test.testcondition.condition.EnableTestCondition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-24
 * @time: 9:58
 */
@SpringBootApplication
@EnableTestCondition
public class StartCondition {
    public static void main(String[] args) {
        SpringApplication.run(StartCondition.class,args);

    }
}
