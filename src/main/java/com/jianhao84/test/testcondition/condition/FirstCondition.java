package com.jianhao84.test.testcondition.condition;


import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.lang.annotation.Annotation;

/**
 * Created with IntelliJ IDEA.
 * Description:实现条件接口的方法
 *
 * @author jianhao84
 * @date 2018-10-24
 * @time: 10:00
 */
public class FirstCondition implements Condition {
    /**
     * 判断启动类上是否有配置类上指定的注解,如果有则初始化配置,相当于自定义的springboot提供的@ConditionalOnXXX方式.
     * @param context
     * @param metadata
     * @return
     */
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        // 获取配置类上指定的开关注解
        Class<Annotation> annotation =  (Class<Annotation>)metadata.getAnnotationAttributes(ConditionOnTest.class.getName()).get("annotation");
        // 获取启动类
        Class mainClass = null;
        StackTraceElement[] stackTrace = new RuntimeException().getStackTrace();
        for (StackTraceElement stackTraceElement : stackTrace) {
            if ("main".equals(stackTraceElement.getMethodName())) {
                try {
                    mainClass = Class.forName(stackTraceElement.getClassName());
                    // 千万别忘了break,不然会找到idea本身的AppMain.
                    break;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        // 启动类上有指定的开关注解,则初始化配置类
        Annotation openAnnotation = mainClass.getAnnotation(annotation);
        if(openAnnotation != null) {
            return true;
        }
        return false;
    }
}
