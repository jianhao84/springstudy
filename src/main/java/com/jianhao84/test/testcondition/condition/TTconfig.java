package com.jianhao84.test.testcondition.condition;

import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-29
 * @time: 18:00
 */
@ConditionalOnResource(resources = "data.db")
@Component
public class TTconfig {

    public TTconfig() {
        System.out.println("------------------哈哈");
    }
}
