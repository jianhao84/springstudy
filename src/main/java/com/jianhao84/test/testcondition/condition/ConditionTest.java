package com.jianhao84.test.testcondition.condition;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-24
 * @time: 10:02
 */
@Component
@ConditionOnTest(annotation = EnableTestCondition.class)
public class ConditionTest {
    public ConditionTest() {
        System.out.println("满足条件初始化ConditionTest了");
    }
}
