package com.jianhao84.test.testcondition.condition;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-24
 * @time: 17:58
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Conditional(FirstCondition.class)
public @interface ConditionOnTest {
    Class<? extends Annotation> annotation();
}
