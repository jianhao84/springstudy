package com.jianhao84.test.testaop.springaop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-09
 * @time: 11:48
 */
@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AopStart {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(AopStart.class,args);
        context.getBean(AopWater.class).drink();
        System.out.println("=================================");
        context.getBean(Juice.class).drink();
    }
}
