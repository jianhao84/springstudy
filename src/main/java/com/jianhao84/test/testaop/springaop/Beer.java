package com.jianhao84.test.testaop.springaop;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:实现接口的目标类
 *
 * @author jianhao84
 * @date 2018-11-09
 * @time: 14:27
 */
@Component
public class Beer implements AopWater{

    @Override
    public String drink() {
        System.out.println("beer的目标方法");
        return "喝啤酒";
    }
}
