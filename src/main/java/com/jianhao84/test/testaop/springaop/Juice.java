package com.jianhao84.test.testaop.springaop;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:没有实现接口的目标类
 *
 * @author jianhao84
 * @date 2018-11-09
 * @time: 14:28
 */
@Component
public class Juice {
    public String drink() {
        return "喝果汁";
    }
}
