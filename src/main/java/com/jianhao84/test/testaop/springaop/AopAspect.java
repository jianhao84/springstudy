package com.jianhao84.test.testaop.springaop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;


import static com.sun.xml.internal.ws.dump.LoggingDumpTube.Position.Before;

/**
 * Created with IntelliJ IDEA.
 * Description:定义切面
 *
 * @author jianhao84
 * @date 2018-11-09
 * @time: 14:31
 */
@Aspect
@Component
public class AopAspect {

    /**定义一个切入点:指定切入规则
     * 方法名就是定义的切入点名字,可以在@Before等通知注解中直接使用,如:@Before("testpoint()").
     */
    @Pointcut("execution(* *.drink(..))")
    public void testpoint() {}

    /**
     *定义前置增强,对drink方法增强
     */
    @Before("testpoint()")
    public void beforeEnhance(JoinPoint point) {
        System.out.println("@Before:前置增强....");
    }

    /**
     *定义后置增强,对drink方法增强
     */
    @After(value = "execution(* *.drink(..))")
    public void afterEnhance(JoinPoint point) {
        System.out.println("@After:后置增强....");
    }

    /**
     *定义环绕增强,对drink方法增强
     */
    @Around(value = "execution(* *.drink(..))")
    public Object aroundEnhance(ProceedingJoinPoint point) throws Throwable {
        System.out.println("@Around:环绕增强....前");
        Object result = point.proceed();
        System.out.println("目标代理对象方法执行结果:" + result);
        System.out.println("@Around:环绕增强....后");
        return result;
    }

    /**定义返回增强,对drink方法增强*/
    @AfterReturning(returning = "ee",pointcut = "execution(* *.drink(..))")
    public void afterReturing(JoinPoint point,String ee) {
        System.out.println("@AfterReturning:返回增强....执行结果:" + ee);
    }

    /**定义异常增强,对drink方法增强*/
    @AfterThrowing(throwing = "throwing",pointcut = "execution(* *.drink(..))")
    public void afterThrowing(JoinPoint point,Throwable throwing) {
        System.out.println("@AfterThrowing:异常增强...");
    }

}
