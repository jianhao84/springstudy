package com.jianhao84.test.testaop.springaop;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-09
 * @time: 11:56
 */
public interface AopWater {

    String drink();
}
