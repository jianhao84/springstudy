spring aop的使用,重点分析:
1.通知的先后顺序.
2.jdk代理和cglib代理的使用时机及强制使用cglib代理.

运行得出结果:
1.实现了接口的目标代理对象的代理对象是由JDK动态代理生成的,比如Beer.
2.没有实现接口的目标代理对象的代理对象是由Cglib动态代理生成的,比如Juice.
3.可以通过@EnableAspectJAutoProxy(proxyTargetClass = true)来强制使用cglib代理.
4.目标对象方法执行正常情况下通知的执行顺序:@Around->@Before->目标对象方法->@Around->@After->@AfterReturning.
5.目标对象方法执行异常情况下通知的执行顺序:@Around->@Before->目标对象方法->@After->@AfterThrowing.
6.被通知注解标记的方法的第一个参数必须是ProceedingJoinPoint或JoinPoint对象.

ProceedingJoinPoint继承了JoinPoint,在JoinPoint的基础上多出了proceed方法,proceed方法就是用于启动目标方法执行的.
ProceedingJoinPoint对象的常用方法:
getTarget():获取目标对象.
getSignature().getName():获取目标对象上正在执行的方法名.
getTarget().getClass().getInterfaces():获取目标对象上的接口.
getTarget().getClass().getAnnotations():获取目标对象上的注解.