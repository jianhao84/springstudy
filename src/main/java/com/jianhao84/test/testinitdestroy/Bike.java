package com.jianhao84.test.testinitdestroy;

import lombok.Getter;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 15:22
 */
@Getter
public class Bike {

    private String name;
    // 设置属性
    public void setName(String name) {
        this.name = name;
        System.out.println("setName");
    }

    public Bike() {
        System.out.println("构造方法");
    }

    public void init() {
        System.out.println("init");
    }

    public void destroy() {
        System.out.println("destroy");
    }
}
