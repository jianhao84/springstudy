package com.jianhao84.test.testinitdestroy;

import lombok.Getter;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 15:22
 */
@Getter
public class Bike4  implements InitializingBean,DisposableBean {

    private String name;
    // 设置属性
    public void setName(String name) {
        this.name = name;
        System.out.println("setName");
    }

    public Bike4() {
        System.out.println("构造方法");
    }

//-----------方式一:通过@Bean注解的initMethod和destroyMethod参数指定--------
    public void method1_init() {
        System.out.println("方式一:bean指定的initMethod方法执行...");
    }

    public void method1_destroy() {
        System.out.println("方式一:bean指定的destroyMethod方法执行...");
    }
//-------方式二:使用@PostConstruct和@PreDestroy注解------------------------
    @PostConstruct
    public void method2_init() {
        System.out.println("方式二:PostConstruct注解方式的init");
    }

    @PreDestroy
    public void method2_destroy() {
        System.out.println("方式二:PreDestroy注解方式的destroy");
    }
//----------方式三:继承InitializingBean和DisposableBean方法---------------------
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("方式三:实现InitializingBean接口的方式init");
    }
    @Override
    public void destroy() {
        System.out.println("方式三:实现DisposableBean接口的方式destroy");
    }


}
