package com.jianhao84.test.testinitdestroy.test5;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 15:24
 */
@Configuration
public class TestConfig5 {

    public TestConfig5() {
        System.out.println("TestConfig5容器启动初始化...");
    }

    @Bean(initMethod = "method1_init",destroyMethod = "method1_destroy")
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Bike5 getBike() {
        Bike5 bike = new Bike5();
        bike.setName("周剑");
        return bike;
    }
}
