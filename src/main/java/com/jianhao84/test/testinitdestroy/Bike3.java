package com.jianhao84.test.testinitdestroy;

import lombok.Getter;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 15:22
 */
@Getter
public class Bike3 implements InitializingBean,DisposableBean {

    private String name;

    public void setName(String name) {
        this.name = name;
        System.out.println("setName");
    }

    public Bike3() {
        System.out.println("构造方法");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("init");
    }

    @Override
    public void destroy() {
        System.out.println("destroy");
    }
}
