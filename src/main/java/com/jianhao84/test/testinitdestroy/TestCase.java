package com.jianhao84.test.testinitdestroy;

import com.jianhao84.test.testinitdestroy.test5.Bike5;
import com.jianhao84.test.testinitdestroy.test5.TestConfig5;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 15:26
 */
public class TestCase {

    @Test
    public void test1() {
        System.out.println("------方式一:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig.class);
        System.out.println(context.getBean(Bike.class));
        context.close();

    }

    @Test
    public void test2() {
        System.out.println("------方式二:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig2.class);
        System.out.println(context.getBean(Bike2.class));
        context.close();
    }

    @Test
    public void  test3() {
        System.out.println("------方式三:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig3.class);
        System.out.println(context.getBean(Bike3.class));
        context.close();
    }

    @Test
    public void  test4() {
        System.out.println("------混合顺序测试:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig4.class);
        System.out.println(context.getBean(Bike4.class));
        context.close();
    }

    @Test
    public void test5() {
        System.out.println("------多例测试:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig5.class);
        System.out.println(context.getBean(Bike5.class));
        context.close();
    }
}
