package com.jianhao84.test.testinitdestroy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 15:24
 */
@Configuration
public class TestConfig {

    @Bean(initMethod = "init",destroyMethod = "destroy")
    public Bike getBike() {
        Bike bike = new Bike();
        bike.setName("周剑");
        return bike;
    }
}
