package com.jianhao84.test.testinitdestroy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 15:24
 */
@Configuration
public class TestConfig4 {

    public TestConfig4() {
        System.out.println("TestConfig4容器启动初始化...");
    }

    @Bean(initMethod = "method1_init",destroyMethod = "method1_destroy")
    public Bike4 getBike() {
        Bike4 bike = new Bike4();
        bike.setName("周剑");
        return bike;
    }
}
