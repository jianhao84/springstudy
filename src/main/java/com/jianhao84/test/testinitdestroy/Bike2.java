package com.jianhao84.test.testinitdestroy;

import lombok.Getter;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-10-11
 * @time: 15:22
 */
@Getter
public class Bike2 {

    private String name;

    public void setName(String name) {
        this.name = name;
        System.out.println("setName");
    }

    public Bike2() {
        System.out.println("构造方法");
    }

    @PostConstruct
    public void init() {
        System.out.println("init");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("destroy");
    }
}
