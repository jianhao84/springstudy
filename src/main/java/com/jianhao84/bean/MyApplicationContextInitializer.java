package com.jianhao84.bean;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * spring初始化前
 * 实现方式:
 * 1.实现ApplicationContextInitializer接口.
 * 2.在application.properties中设置context.initializer.classes的值为该类的完整路径.
 * @author huqichao
 * @date 2018-05-28 11:27
 */
public class MyApplicationContextInitializer implements ApplicationContextInitializer {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        System.out.println(this.getClass().getName() + "-------initialize");
//        AbstractApplicationContext ac = (AbstractApplicationContext) applicationContext;
//        ac.start();
//
//        ac.stop();
    }
}
