package com.jianhao84.bean;

import com.jianhao84.UserServiceImpl;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.*;
import org.springframework.context.support.AbstractApplicationContext;

/**
 * spring加载完成:
 * 使用场景:在一些业务场景中，当容器初始化完成之后，需要处理一些操作，比如一些数据的加载、初始化缓存、特定任务的注册等等。
 * 这个时候我们就可以使用Spring提供的ApplicationListener来进行操作。
 * @author huqichao
 * @date 2018-05-28 11:50
 */
@Configuration
public class MyApplicationListener implements ApplicationListener<ApplicationContextEvent> {
    @Override
    public void onApplicationEvent(ApplicationContextEvent event) {
        if (event instanceof ContextRefreshedEvent){
            System.out.println(this.getClass().getName() + "-------onApplicationEvent-------ContextRefreshedEvent");

            AbstractApplicationContext ac = (AbstractApplicationContext) event.getApplicationContext();
            ac.start();

            ac.stop();
        }
        if (event instanceof ContextStartedEvent){
            System.out.println(this.getClass().getName() + "-------onApplicationEvent-------ContextStartedEvent");
        }
        if (event instanceof ContextStoppedEvent){
            System.out.println(this.getClass().getName() + "-------onApplicationEvent-------ContextStoppedEvent");
            AbstractApplicationContext ac = (AbstractApplicationContext) event.getApplicationContext();
            UserServiceImpl userService = ac.getBean(UserServiceImpl.class);
            userService.sayHello();
        }
        if (event instanceof ContextClosedEvent){
            System.out.println(this.getClass().getName() + "-------onApplicationEvent-------ContextClosedEvent");
        }
    }
}



