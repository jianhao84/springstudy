package com.jianhao84.common;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author jianhao84
 * @date 2018-11-12
 * @time: 11:34
 */
public class BusinessException extends RuntimeException {
    private int code;
    private String msg;


    public BusinessException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
