### Spring配置跨域
#### 1.细粒度的跨域
* 解决方法:使用@CrossOrigin注解在controller类或者controller类的方法上.
    * 注解参数介绍:
        * origins:允许可访问的域列表(ip或域名).
        * maxAge:预请求的结果的有效期，默认30分钟.
        * allowCredentials:是否允许cookie随请求发送，使用时必须指定具体的域.
        * methods:请求支持的方法，例如"{RequestMethod.GET, RequestMethod.POST}"},默认支持RequestMapping中设置的方法.
        * exposedHeaders:响应头中允许访问的header，默认为空.
        * allowedHeaders:允许请求头中的header，默认都支持.
    * 控制controller中所有接口跨域:
    ~~~
    @CrossOrigin(origins = "http://domain2.com", maxAge = 3600)
    @RestController
    @RequestMapping("/account")
    public class AccountController {
        @RequestMapping("/{id}")
        public Account retrieve(@PathVariable Long id) {
            // ...
        }
        @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
        public void remove(@PathVariable Long id) {
            // ...
        }
    }
    ~~~
    * 控制controller中指定接口跨域
    ~~~
    @RestController
    @RequestMapping("/account")
    public class AccountController {
        @CrossOrigin("http://domain2.com")
        @RequestMapping("/{id}")
        public Account retrieve(@PathVariable Long id) {
            // ...
        }
    }
    ~~~
    * 两种方式合并:spring也会合并属性
    ~~~
    @CrossOrigin(maxAge = 3600)
    @RestController
    @RequestMapping("/account")
    public class AccountController {
    
        @CrossOrigin("http://domain2.com")
        @RequestMapping("/{id}")
        public Account retrieve(@PathVariable Long id) {
            // ...
        }
    
        @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
        public void remove(@PathVariable Long id) {
            // ...
        }
    }
    ~~~
#### 2.CORS全局配置
* 基于springMvc
~~~
@Configuration
public class WebConfig {
    /**
     * 跨域支持
     * @param
     */
    @Bean
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // 设置你要允许的网站域名，如果全允许则设为 *
        config.addAllowedOrigin("*");
        // 如果要限制 HEADER 或 METHOD 请自行更改
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        // 这个顺序很重要哦，为避免麻烦请设置在最前
        bean.setOrder(0);
        return bean;
    }
}
~~~
* 基于springboot
~~~
@Configuration
public class MyConfiguration {

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }
}
~~~
* 不管基于springMvc还是基于springboot,都可以修改一下所有属性来达到控制跨域
    * 其中*表示匹配到下一层,**表示后面不管有多少层,都能匹配。
    ~~~
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api/**")
            .allowedOrigins("http://domain2.com")
            .allowedMethods("PUT", "DELETE")
                .allowedHeaders("header1", "header2", "header3")
            .exposedHeaders("header1", "header2")
            .allowCredentials(false).maxAge(3600);
    }
    ~~~
