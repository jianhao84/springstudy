### 初始化器ApplicationContextInitializer的3种使用方法:
* 介绍:
~~~
1.ApplicationContextInitializer是在ConfigurableApplicationContext刷新之前初始化Spring ConfigurableApplicationContext的回调接口,会在ConfigurableApplicationContext的refresh()方法调用之前被调用,做一些容器的初始化工作。
2.总结第一点,就是在spring初始化前执行.
3.实现类上支持@Order注解,Order的value值越小表示越早执行.
~~~
#### 1.spring.factories方式
* 使用步骤:
    1. 自定义个类实现了ApplicationContextInitializer,比如类名为com.jianhao84.MyApplicationContextInitializer. 
    2. 在resource下面新建/META-INF/spring.factories文件.
    3. 在spring.factories文件中加入以下内容:
    ~~~
    org.springframework.context.ApplicationContextInitializer=\
    com.jianhao84.MyApplicationContextInitializer
    ~~~
* 加载时机:这个加载过程是在SpringApplication中的getSpringFactoriesInstances()方法中直接加载并实例后执行对应的initialize方法

#### 2.application.properties添加配置方式
* 使用步骤:
   1. 自定义个类实现了ApplicationContextInitializer,比如类名为com.jianhao84.MyApplicationContextInitializer. 
   2. 在配置文件application.properties中添加配置内容如下:
   ~~~
   context.initializer.classes=com.jianhao84.MyApplicationContextInitializer
   ~~~
* 加载时机:这种方式是通过DelegatingApplicationContextInitializer这个初始化类中的initialize方法获取到application.properties中context.initializer.classes对应的类并执行对应的initialize方法.

#### 3.直接通过add方式
* 使用步骤:
    1. 自定义个类实现了ApplicationContextInitializer,比如类名为com.jianhao84.MyApplicationContextInitializer.
    2. 在项目的启动类的main方法中new一个SpringApplication对象,在new的SpringApplication对象添加initializers,代码如下:
    ~~~
    public static void main(String[] args) {		
        // StartupServer是启动类类名
        SpringApplication springApplication = new SpringApplication(StartupServer.class);		
        springApplication.addInitializers(new MyApplicationContextInitializer());		
        springApplication.run(args); 		
    }
    
    ~~~
