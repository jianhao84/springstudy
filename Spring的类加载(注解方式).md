### Spring的类加载(注解方式)
#### 方式介绍
* 单例+预加载:spring默认的方式
    * 代码
    ~~~
    @Component
    public class Test{
    
    }
    ~~~
    * 理解:
        * 项目一启动就产生一个且仅一个实例,即单例.
        * 并且通过@Autowired、@Resource等注入注解只能获得这个单例。
        * new Test()则不受单例限制,但不受spring管理.
* 单例+懒加载
    * 代码
    ~~~
    @Lazy
    @Component
    public class Test{
    
    }
    ~~~
    * 理解:
        * 项目启动时不加载.
        * 仅当其他类需要注入的时候,才会生成Test类的一个且仅一个实例.
        * 禁止使用以下注解注入需要懒加载的bean(只要被spring扫描到就会生成test实例了).
            * @AutoWired Test test;
            * @Resource Test test;
            * @Inject Test test;
            * @DependsOn("test");
        * 正确实例化懒加载类的方式:
        ~~~
        public class TestDemo {

            TestSingle testSingle;
        
            //通过 BeanFactory 接口创建实例
            @Autowired
            BeanFactory beanFactory;
        
            public void doSth(){
                //test 是 Test 类名首字母小写！一定要首字母小写！
                //只有运行到这里 Test 类才被实例化，延迟加载成功
                TestSingle ts=(TestSingle) beanFactory.getBean("test");
            }
        }
        ~~~
        * 通过@Autowired、@Resource等注入注解只能获得这个单例.
        * new Test()则不受单例限制,但不受spring管理.
* 多例+懒加载
     * 代码
     ~~~
     @Scope("prototype")
     @Component
     public class Test{
     
     }
     ~~~
     * 理解:
        * @Scope("prototype"):每个注入的地方(@Autowired、@Resource、@Inject)都会生成一个实例,可以有多个实例
        * 多例本身就是懒加载,所以无论加不加@Lazy,被@Scope("prototype")修饰的类都会懒加载.