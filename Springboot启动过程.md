### 1.启动类中静态调用SpringApplication.run方法，将启动类的Class和main参数传进去。
### 2.初始化SpringApplication(初始化成员变量):
~~~
1.sources:就是把启动类的Class放进去.

2.webEnvironment:是一个boolean值，表示是否是web项目,这里为true.(在classpath中查看是否存在WEB_ENVIRONMENT_CLASSES这个数组中所包含的类，如果存在那么当前程序即是一个Web应用程序，反之则不然)

3.initializers:是一个ApplicationContextInitializer类型对象的集合,ApplicationContextInitializer就是可以用来初始化ApplicationContext的接口,初始化ApplicationContext在refresh之前调用.
    A.通过getSpringFactoriesInstances方法从META-INF/spring.factories文件中用ApplicationContextInitializer的className为key找到多个ApplicationContextInitializer的完整类名。
    B.将找到的多个ApplicationContextInitializer类型名通过createSpringFactoriesInstances方法创建成对象列表返回。
    C.将对象列表赋值给SpringApplication的成员变量initializers。
    D.最终initalizers就被初始化为以下类型对象的list:
        a.ConfigurationWarningsApplicationContextInitializer:为ApplicationContext添加一个能检查配置,并在有常见配置错误时打印警告信息的BeanPostProcessor,
            实现方式是通过向context上下文对象中添加一个BeanFactoryPostProcessor，然后在refresh ApplicationContext的时候BeanFactoryPostProcessor会被调用到,如:
            @Override
            public void initialize(ConfigurableApplicationContext context) {
                context.addBeanFactoryPostProcessor(
                        new ConfigurationWarningsPostProcessor(getChecks()));
            }
            
        b.ContextIdApplicationContextInitializer:给ApplicationContext设置一个ID，这个ID的生成规则是尝试读取以下几个属性(spring.application.name、vcap.application.name、spring.config.name),
            如果不存在则使用application作为值。除此之外，还会在上面得到的结果后附加一个port或者index，同样也是读取属性(vcap.application.instance_index、spring.application.index、server.port、PORT),
            所以一个可能的Context ID就是application:10001。
            
        c.DelegatingApplicationContextInitializer:从ApplicationContext中的环境配置中读取initializers并应用,
            这个初始化器实际上将初始化的工作委托给context.initializer.classes环境变量指定的初始化器(通过类名:比如该项目在application.properties中指定的context.initializer.classes)。
        
        d.ServerPortInfoApplicationContextInitializer:为ApplicationContext设置一个环境变量,以方便测试代码里使用服务器正在监听的端口.
        
4.listeners:是一个ApplicationListener类型对象的集合,ApplicationListener是观察者模式的监听器接口,用来监听各种ApplicationEvent.
    A.获取该成员变量内容使用的是跟成员变量initializers一样的方法，只不过传入的类型从ApplicationContextInitializer.class变成了ApplicationListener.class.  
    B.listener最终会被初始化为以下对象的list:
        a.ParentContextCloserApplicationListener:当父Context关闭的时,关闭当前ApplicationContext.
        b.FileEncodingApplicationListener:当系统属性file.encoding与配置spring强制编码属性不一致时,打印出错信息,并终止程序的启动.
        c.AnsiOutputApplicationListener:根据属性spring.output.ansi.enable配置ANSI输出.
        d.ConfigFileApplicationListener:搜索并加载配置文件,根据配置文件设置Environment和SpringApplication.
        e.DelegatingApplicationListener:加载并转发事件至context.listener.classes中配置的ApplicationListener.
        f.LiquibaseServiceLocatorApplicationListener:如果classpath中存在类liquibase.servicelocator.ServiceLocator,那么将其替换成一个使用springBoot的版本.
        g.ClasspathLoggingApplicationListener:当程序启动成功时,将classpath路径打印到debug日志,当程序启动失败时,将classpath路径打印到info日志.
        h.LoggingApplicationListener:根据配置,在合适的时候初始化和配置日志系统.

5.mainApplicationClass:主类,即启动类.设置办法是,new一个RuntimeException,然后getStackTrace()从该运行时异常的调用栈中找到main方法的类.
~~~
### 3.执行SpringApplication的run方法:
1. 设置headless模式:实际上是就是设置系统属性java.awt.headless,默认true,Headless模式是在缺少显示屏、键盘或者鼠标是的系统配置(因为我们开发的是服务端程序,很可能没有).

2. 加载运行时监听器(SpringApplicationRunListener:监听SpringApplication的run方法的执行),并开始计时.

3. 通过SpringFactoriesLoader检索META-INF/spring.factories*,找到声明的所有SpringApplicationRunListener的实现类并将其实例化,实例化的时候会初始化事件广播器(initialMulticaster),
之后逐个调用其started()方法，广播SpringBoot要开始执行了。这里只有EventPublishingRunListener这么一个SpringApplicationRunListener.

4. 构建main方法传入的参数为ApplicationArguments对象.

5. 创建并配置当前SpringBoot应用将要使用的Environment(包括配置要使用的PropertySource以及Profile),并遍历调用所有的SpringApplicationRunListener的environmentPrepared()方法，广播Environment准备完毕:
    * 创建environment:如果存在就直接返回,如果是web环境就创建StandardServletEnvironment(web服务环境),否则创建StandardEnvironment(标准环境).
    * 配置一些环境信息:比如profile和命令行参数.
    * 并遍历调用所有的SpringApplicationRunListener的environmentPrepared()方法，广播Environment准备完毕。
    * 环境信息的校对:如果不是web环境，但environment是web的environment，则把这个web的environment转换成标准的environment

6. 决定是否打印Banner:可以在项目的classpath下新建一个banner.txt文件.

7. 根据webEnvironment的值来决定创建何种类型的ApplicationContext对象
    * 如果是web环境，则创建org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext
    * 否则创建org.springframework.context.annotation.AnnotationConfigApplicationContext

8. 注册异常分析器.

9. 准备ApplicationContext:
    * 为ApplicationContext加载environment.
    * 逐个执行ApplicationContextInitializer的initialize()方法来进一步封装ApplicationContext.
    * 并调用所有的SpringApplicationRunListener的contextPrepared()方法,【EventPublishingRunListener只提供了一个空的contextPrepared()方法】，
    * 之后初始化IoC容器，并调用SpringApplicationRunListener的contextLoaded()方法,广播ApplicationContext的IoC加载完成,这里就包括通过**@EnableAutoConfiguration**导入的各种自动配置类。
  
10. 初始化所有自动配置类:
    * 调用AbstractApplicationContext.refresh 完成初始化:
    ~~~
    public void refresh() throws BeansException, IllegalStateException {
        // 
        /**
         * 第一步.synchronized同步块:使用了对象锁startUpShutdownMonitor,applicationContext.close()方法也使用了对象锁startUpShutdownMonitor,
         *   保证了在调用refresh()方法的时候无法调用close()方法,反之亦然,避免了冲突.
         */
        synchronized (this.startupShutdownMonitor) {
            /**
             *第二步.prepareRefresh()做了3件事:
             *  1.设置其启动日期startupDate和活动标志active,这个在容器启动后也会有使用.
             *  2.initPropertySources():用来初始化一些属性设置,目前没有代码,是用来给子类去定义实现的,这是第一个扩展点,如果需要自己实现自己的ApplicationContext,并且在验证之前为系统属性设置一些值可以在子类中实现此方法.
             *  3.getEnvironment().validateRequiredProperties():
             *      3.1.获取当前的Environment,如果没有则创建一个StandardEnvironment,并且进行属性校验,校验属性的合法性.
             *      3.2.如果前面初始化容器的时候设置了requiredProperty,则会在这里校验,校验不通过会报错.(可以用System.setProperty("key","value")给requiredProperty属性set值)
             *  4.this.earlyApplicationEvents = new LinkedHashSet<ApplicationEvent>():初始化一个集合,用于保存容器中早期的事件.
             */
            prepareRefresh();

            /**
             *第三步.获取Bean工厂.通过obtainFreshBeanFactory()这个方法后,ApplicationContext就已经拥有了BeanFactory的所有功能了.
             *  1.refreshBeanFactory():刷新或者创建beanFactory.
             *      1.1.通过对beanFactoryMonitor对象锁的方式进行BeanFactory是否存在的判断,如果存在则将单例bean容器清空并且将之前的BeanFactory设置为null.
             *      1.2.通过DefaultListableBeanFactory beanFactory = createBeanFactory()方法创建新的BeanFactory,并且设置BeanFactory唯一的Id.
             *      1.3.customizeBeanFactory(beanFactory):主要设置BeanFactory的相关属性:
             *          1.3.1.this.allowBeanDefinitionOverriding:是否允许覆盖同名称的不同定义的对象.
             *          1.3.2.this.allowCircularReferences:是否允许Bean之前存在循环依赖.
             *      1.4.loadBeanDefinitions(beanFactory):加载beanDefinition,这个步骤就是通过初始化AnnotatedBeanDefinitionReader来读取配置的.
             *          1.4.1.获取AnnotatedBeanDefinitionReader和ClassPathBeanDefinitionScanner及BeanNameGenerator.
             *          1.4.2.若BeanNameGenerator不为空,则将其设置为AnnotatedBeanDefinitionReader和ClassPathBeanDefinitionReader使用的BeanNameGenerator(若为空,则取各自默认BeanNameGenerator),并将该BeanNameGenerator以internalConfigurationBeanNameGenerator的名字注册到容器中,这是spring容器启动时最先初始化的内部Bean之一.
             *          1.4.3.获取ScopeMetadataResolver,若ScopeMetadataResolver不为空,则将其设置为AnnotatedBeanDefinitionReader和ClassPathBeanDefinitionReader使用的ScopeMetadataResolver,ScopeMetadataResolver用于解析@Scope注解.              
             *          1.4.4.注册自定义添加的annotatedClass到AnnotatedBeanDefinitionReader.
             *          1.4.5.ClassPathBeanDefinitionScanner扫描指定包路径下所有class包.
             *          1.4.6.取configLocations配置,先当作AnnotatedClass进行注册,若找不到类,则当作包路径用以扫描BeanDefinition;reader.register(clazz)主要实现在方法registerBean(Class<?> annotatedClass, String name, Class<? extends Annotation>... qualifiers):
             *              1.4.6.1.首先根据@Conditional注解判断是否需要注册.
             *              1.4.6.2.获取类上的@Scope注解并解析其元数据.
             *              1.4.6.3.返回一个definitionHolder,并通过这个方法applyScopedProxyMode生成scope的代理.
             *              1.4.6.4.最后将definitionHolder注册到容器中(definitionHolder的beanName作为beanName,definitionHolder的beanDefinition作为bean).
             *      1.5.加对象锁beanFactoryMonitor,然后将context的beanFactory更新为新初始化好的.
             *  2.获取context的beanFactory并返回.
             */
            ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

            /**
             *第四步:设置这个BeanFactory的标准上下文特性,比如比如上下文的类加载器和后置处理器,传进来的参数是待配置的beanFactory.
             *  1.设置BeanFactory的BeanClassLoader,如果存在,则直接使用之前的那个,否则,初始化一个新的ClassLoader.
             *  2.设置BeanExpressionResolver表达式解析器,主要用来解析EL表达式(在Bean进行初始化后会有属性填充,这时候会对属性值进行解析).
             *  3.设置属性注册解析器PropertyEditor,这个主要是对某些注入的Bean的一些属性的支持.
             *  4.设置ApplicationContextAwareProcessor的支持.这里仅仅是注册,真是的实现还是在ApplicationContextAwareProcessor的前置处理方法postProcessBeforeInitialization:
             *      4.1.首先就是校验权限.
             *      4.2.判断bean的类型是否是指定的Aware类型(EnvironmentAware,EmbeddedValueResolverAware,ResourceLoaderAware,ApplicationEventPublisherAware,MessageSourceAware,ApplicationContextAware)
             *      4.3.如果是指定的Aware类型,则调用invokeAwareInterfaces,这样实现了这些Aware接口的Bean在被初始化之后就能取到响应的资源.
             *  5.设置忽略自动装配的属性(就是4.2里判断的几个Aware类型,因为在4的时候已经将这些内容注册了,所以Spring做Bean的注入的时候要忽略).
             *  6.注册依赖:当检查到依赖时,将依赖的实例进行注册.
             *  7.注册后置处理器ApplicationListenerDetector:主要是检测是否有事件监听,以及判断是否有对编译时AspectJ的支持,这些都是通过后置处理器来完成的
                8.给BeanFactory注册一些能通用的组件:loadTimeWeaver、environment、systemProperties、systemEnvironment.
             */
            prepareBeanFactory(beanFactory);

            try {
                // Allows post-processing of the bean factory in context subclasses.
                postProcessBeanFactory(beanFactory);

                // Invoke factory processors registered as beans in the context.
                invokeBeanFactoryPostProcessors(beanFactory);

                // Register bean processors that intercept bean creation.
                registerBeanPostProcessors(beanFactory);

                // Initialize message source for this context.
                initMessageSource();

                // Initialize event multicaster for this context.
                initApplicationEventMulticaster();

                // Initialize other special beans in specific context subclasses.
                onRefresh();

                // Check for listener beans and register them.
                registerListeners();

                // Instantiate all remaining (non-lazy-init) singletons.
                finishBeanFactoryInitialization(beanFactory);

                // Last step: publish corresponding event.
                finishRefresh();
            }

            catch (BeansException ex) {
                if (logger.isWarnEnabled()) {
                    logger.warn("Exception encountered during context initialization - " +
                            "cancelling refresh attempt: " + ex);
                }

                // Destroy already created singletons to avoid dangling resources.
                destroyBeans();

                // Reset 'active' flag.
                cancelRefresh(ex);

                // Propagate exception to caller.
                throw ex;
            }

            finally {
                // Reset common introspection caches in Spring's core, since we
                // might not ever need metadata for singleton beans anymore...
                resetCommonCaches();
            }
        }
    }
    ~~~
    * 如果registerShutdownHook为true的话,注册一个关闭容器时的钩子函数,默认是true.
        
11. 容器创建完成之后执行额外一些操作:
    a.遍历所有注册的ApplicationRunner和CommandLineRunner，并执行其run()方法,该过程可以理解为是SpringBoot完成ApplicationContext初始化前的最后一步工作.
    b.我们可以实现自己的ApplicationRunner或者CommandLineRunner，来对SpringBoot的启动过程进行扩展。
    
12. 调用所有的SpringApplicationRunListener的finished()方法，广播SpringBoot已经完成了ApplicationContext初始化的全部过程。

13. 关闭任务执行时间监听器.

14. 如果日志开启,则打印执行时间.

