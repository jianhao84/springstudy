### Spring的代理
#### 代理的概念
* 代理设计模式:是java中常用的设计模式！
* 特点：
    * 委托类和代理类有相同的接口或者共同的父类.
    * 代理类为委托类负责处理消息，并将消息转发给委托类.
    * 委托类和代理类对象通常存在关联关系,一个代理类对象与一个委托类对象关联.
    * 代理类本身并不是真正的实现者！而是通过调用委托类的方法来实现功能.
* 代理对象:当一个对象(客户端)不能或者不想直接引用另一个对象(目标对象),这时可以应用代理模式在这两者之间构建一个桥梁.
* 按照代理类创建的时机,代理分为:
    * 静态代理:由程序员创建代理类或特定工具自动生成源代码再对其编译,在程序运行前代理类的class文件就已经存在了.
    * 动态代理:在程序运行期间,动态的创建出来代理对象.
#### 静态代理
* 代码举例:通过实现接口方式实现静态代理(也可以通过继承父类方式)
    * 新建一个Animal(动物)接口:有一个eatFood方法.
    ~~~
    public interface Animal {
        public void eatFood();
    }
    ~~~
    * 新建一个Dog(狗)类,实现Animal(动物)接口.
    ~~~
    @Data
    public class Dog implements Animal {
        private String name;
        @Override
        public void eatFood() {
            System.out.println("名字叫" + name + "的狗在吃食物...");
        }
    }
    ~~~
    * 新建一个狗的静态代理类,来增强狗类中的eatFood方法(添加其他功能,但不修改Dog类)
    ~~~
    @Data
    public class DogProxy implements Animal {
        private Dog dog;
        @Override
        public void eatFood() {
            masterCallUp();
            dog.eatFood();
            masterLeave();
        }
        private void masterCallUp() {
            System.out.println("狗" + dog.getName() + "的主人在召唤...");
        }
        private void masterLeave() {
            System.out.println("狗" + dog.getName() + "的主人离开了...");
        }
    }
    ~~~
    * 测试类:通过静态代理类来调用Dog的eatFood方法,并对方法进行了增强.
    ~~~
    public class StaticProxyTest {
        @Test
        public void test() {
            DogProxy dogProxy = new DogProxy();
            Dog dog = new Dog();
            dog.setName("毛毛");
            dogProxy.setDog(dog);
            dogProxy.eatFood();
        }
    }
    ~~~
    * 测试结果
    ~~~
    狗毛毛的主人在召唤...
    名字叫毛毛的狗在吃食物...
    狗毛毛的主人离开了...
    ~~~
* 静态代理的问题:
    * 代理对象只服务于一种类型的对象,如果要服务多类型的对象,势必要为每一种对象都进行代理,静态代理在程序规模稍大时就无法胜任了.
    * 如果还有一个Cat(猫)类,也需要增强eatFood方法,又需要把Dog的代理重新写一份.
    * 如果动物很多,那就需要写很多份,产生大量的冗余代码.
#### 动态代理
* spring中动态代理有2种:
    * jdk动态代理:利用拦截器(拦截器必须实现InvocationHanlder)加上反射机制生成一个实现代理接口的匿名类,在调用具体方法前调用InvokeHandler来处理.
    * cglib动态代理:利用asm开源包对代理对象类的class文件加载进来,通过修改其字节码生成子类来处理.
* jdk动态代理和cglib动态代理区别:
    * JDK动态代理只能对实现了接口的类生成代理,而不能针对类.
    * CGLIB是针对类实现代理,主要是对指定的类生成一个子类,并覆盖其中方法实现增强,但是因为采用的是继承,所以该类或方法最好不要声明成final.
* Spring AOP选择JDK代理和CGLiB代理的规则:
    * 当Bean实现接口时,Spring就会用JDK的动态代理.
    * 当Bean没有实现接口时,Spring使用CGlib是实现.
    * 当Bean实现接口时,可以强制使用CGLib代理:
        * XML方式:在spring配置中加入<aop:aspectj-autoproxy proxy-target-class="true"/>
        * 注解方式:在启动类上加注解@EnableAspectJAutoProxy(proxyTargetClass = true)
* JDK动态代理
    * 实现步骤:
        * 新建目标对象变量,提供创建代理方法,并将需要代理的目标对象复制给新建的目标对象变量.
        * 实现InvocationHandler接口的invoke方法.
        * 在invoke方法中result= method.invoke(target,args)并返回result.
        * 在invoke中可以增强被代理对象的方法.
    * 举例:
        * 定义接口:Water
        ~~~
        public interface Water {
            void drink();
        }
        ~~~
        * 定义目标类Beer和Juice类,并实现了接口Water的drink方法.
        ~~~
        public class Beer implements Water {
            @Override
            public void drink() {
                System.out.println("喝啤酒....");
            }
        }
        ~~~
        ~~~
        public class Juice implements Water {
            @Override
            public void drink() {
                System.out.println("喝果汁...");
            }
        }
        ~~~
        * 定义JDK动态代理类
        ~~~
        public class JdkProxy implements InvocationHandler {
            /**需要代理的目标对象:不确定代理哪个类,所以用Object*/
            private Object target;
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                try{
                    /**在方法执行前手动增强被代理对象的方法,spring的aop通过@Before(表达式)来指定在什么连接点(时机)进行什么通知(指定切入点做什么事)*/
                    shake();
                    /**执行被代理对象的方法*/
                    Object result= method.invoke(target,args);
                    // 返回执行结果
                    return result;
                    /**这个不知道怎么实现:在被代理对象的方法被执行完成后增强方法,相当于spring的@AfterRunning(表达式)*/
        
                }catch (Throwable e) {
                    e.printStackTrace();
                    /**如果这里也有增强方法,则相当于spring的@AfterThrowing()*/
                    return null;
                }finally {
                    /**在方法执行后手动增强被代理对象的方法,无论是否异常都执行,相当于spring的@After(表达式)*/
                    cover();
                }
            }
            /**将目标对象传入进行代理*/
            public Object createProxy(Object target){
                this.target=target;
                // 返回代理对象
                return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                        target.getClass().getInterfaces(), this);
            }
            /**前置增强方法:喝前摇一摇*/
            private void shake() {
                System.out.println("前置增强->喝前摇一摇...");
            }
            /**后置增强方法:喝完盖上盖子*/
            private void cover() {
                System.out.println("后置增强->喝完盖上....");
            }
            /**异常增强方法:中毒了*/
            private void poisoning() {
                System.out.println("妈的中毒了...");
            }
        }
        ~~~
        * 测试用例:
        ~~~
        @Test
        public void testJdkProxy() {
            JdkProxy jdkProxy = new JdkProxy();
            Water beer = (Water)jdkProxy.createProxy(new Beer());
            beer.drink();
            System.out.println("-----------------------------------");
            Water juice = (Water)jdkProxy.createProxy(new Juice());
            juice.drink();
        }
        ~~~
        * 测试结果
        ~~~
        前置增强->喝前摇一摇...
        喝啤酒....
        后置增强->喝完盖上....
        -----------------------------------
        前置增强->喝前摇一摇...
        喝果汁...
        后置增强->喝完盖上....
        ~~~
* Cglib动态代理:
    * cglib的几种Callback:
        * MethodInterceptor:方法拦截器,需要实现intercept方法,该方法可以完全控制拦截策略,因此不存在死循环调用风险(因为是用methodProxy去调用目标对象的方法,而不用method对象去调用).
        ~~~
        public Object intercept(Object obj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
            /**执行被代理对象的方法,而不是用method.invoke方法调用.
             * 如果用method.invoke(obj,args)方法调用目标对象的方法也会死循环,除非把obj处传入目标对象.
             */
            Object result= methodProxy.invokeSuper(obj,args);      
        }
        ~~~
        * NoOp:什么操作也不做,代理类直接调用被代理的方法不进行拦截,将对方法的调用直接委托到父类的实现中去.
        * FixValue回调器:需要实现一个loadObject方法,该方法返回的结果将作为被代理方法的一致返回结果,而不会去调用被代理对象的原方法.
        * LazyLoader:延迟加载器,延迟加载器只第一次懒加载,loadObject方法只有在第一次执行,然后保存了代理对象,后面需要直接用,相当于单例.
        * Dispatcher:调用分发器,每次都会懒加载,loadObject每次都会被调用,相当于多例.
        * ProxyRefDispatcher:调用分发器(带代理引用承载功能),和Disptcher类似,但是可以承载具体被代理对象的引用。
        * InvocationHandler:需要实现一个invoke方法,该方法可以在被代理方法调用时进行逻辑织入.
            * 注意:
                * 在实现的invoke方法中调用目标对象(被代理对象)的方法时候,一定要传入目标对象,不要传入代理对象,不然会死循环(因为每当调用代理对象的任何方法都会执行该invoke方法).
                * 正确示范:
                ~~~
                public class JdkInvocationHandlerCallBack implements InvocationHandler {
                    private Object target;
                    public JdkInvocationHandlerCallBack(Object target) {
                        this.target = target;
                    }
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("InvocationHandlerCallBack.invoke:代理");
                        /**注意,这里一样要是执行target对象的方法,而不是proxy对象的方法,不然一直循环调用了*/
                        return method.invoke(target,args);
                    }
                }
                ~~~
                * 错误示范:会死循环
                ~~~
                public class JdkInvocationHandlerCallBack implements InvocationHandler {
                    private Object target;
                    public JdkInvocationHandlerCallBack(Object target) {
                        this.target = target;
                    }
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("InvocationHandlerCallBack.invoke:代理");
                        /**注意,这里一样要是执行target对象的方法,而不是proxy对象的方法,不然一直循环调用了*/
                        return method.invoke(proxy,args);
                    }
                }
                ~~~
    * 实现步骤:
        * 直接用spring-core中的cglib包和asm包,也可以单独引入asm包和cglib包.
        * 实现MethodInterceptor接口的intercept方法.
        * 通过Enhancer对象提供创建代理方法.
        * 提供增强方法.
    * 举例:
        * 定义接口:Water
        ~~~
        public interface Water {
            void drink();
        }
        ~~~
        * 定义目标类Beer和Juice类,并实现了接口Water的drink方法.
        ~~~
        public class Beer implements Water {
            @Override
            public void drink() {
                System.out.println("喝啤酒....");
            }
        }
        ~~~
        ~~~
        public class Juice implements Water {
            @Override
            public void drink() {
                System.out.println("喝果汁...");
            }
        }
        ~~~
        * 定义Cglib动态代理类
        ~~~
        public class CglibProxy implements MethodInterceptor {
            public  Object  createProxy(Class clazz){
                /**在enhancer中有一个setCallBack(this)方法,这样就实现了代理类和委托类的关联*/
                Enhancer enhancer=new Enhancer();
                //设置公共的接口或者公共的类
                enhancer.setSuperclass(clazz);
                //建立关联关系
                enhancer.setCallback(this);
                return enhancer.create();
            }
            /**
             * @param obj   被代理的目标对象
             * @param method    被代理的目标对象的方法
             * @param args  方法参数
             * @param methodProxy   方法代理
             * @throws Throwable
             */
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
                try{
                    /**在方法执行前手动增强被代理对象的方法,spring的aop通过@Before(表达式)来指定在什么连接点(时机)进行什么通知(指定切入点做什么事)*/
                    shake();
                    /**执行被代理对象的方法*/
                    Object result= methodProxy.invokeSuper(obj,args);
                    /**返回执行结果*/
                    return result;
                    /**这个不知道怎么实现:在被代理对象的方法被执行完成后增强方法,相当于spring的@AfterRunning(表达式)*/
                }catch (Throwable e) {
                    e.printStackTrace();
                    /**如果这里也有增强方法,则相当于spring的@AfterThrowing()*/
                    return null;
                }finally {
                    /**在方法执行后手动增强被代理对象的方法,无论是否异常都执行,相当于spring的@After(表达式)*/
                    cover();
                }
            }
            /**前置增强方法:喝前摇一摇*/
            private void shake() {
                System.out.println("前置增强->喝前摇一摇...");
            }
            /**后置增强方法:喝完盖上盖子*/
            private void cover() {
                System.out.println("后置增强->喝完盖上....");
            }
            /**异常增强方法:中毒了*/
            private void poisoning() {
                System.out.println("妈的中毒了...");
            }
        }
        ~~~
        * 测试用例:
        ~~~
        @Test
        public void testCglibProxy() {
            CglibProxy cglibProxy = new CglibProxy();
            Water beer = (Water)cglibProxy.createProxy(Beer.class);
            beer.drink();
            System.out.println("===================================");
            Water juice = (Water)cglibProxy.createProxy(Juice.class);
            juice.drink();
        }
        ~~~
        * 测试结果:
        ~~~
        前置增强->喝前摇一摇...
        喝啤酒....
        后置增强->喝完盖上....
        ===================================
        前置增强->喝前摇一摇...
        喝果汁...
        后置增强->喝完盖上....
        ~~~







