### Spring的Resource体系
* 介绍:在Spring中对资源进行了抽象,从而屏蔽了资源类型和来源的区别,使得内部对于操作这些资源的API更加统一,这个统一的接口就是Resource.
#### Resource接口的通用方法
* boolean exists():返回当前Resource代表的底层资源是否存在,true表示存在.
* boolean isReadable():判断资源是否可读,只有在返回true的时候getInputStream方法才可用(需要注意的是当其结果为true的时候,其内容未必真的可读,但如果返回false,则其内容必定不可读).
* boolean isOpen():判断当前资源是否代表一个已打开的输入流,如果结果为true,则表示当前资源的输入流不可多次读取,而且在读取以后需要对它进行关闭,以防止内存泄露.该方法主要针对于InputStreamResource,实现类中只有它的返回结果为true,其他都为false.
* URL getURL():返回当前资源对应的URL.如果当前资源不能解析为一个URL则会抛出异常.如ByteArrayResource就不能解析为一个URL.
* URI getURI():获取资源对象的URI,如果该资源不能表示为URI形式则抛出异常.
* File getFile():返回当前资源对应的File,如果当前资源不能以绝对路径解析为一个File则会抛出异常.如ByteArrayResource就不能解析为一个File.
* long contentLength():获取资源内容的长度,如果资源无法解析则抛出异常.
* long lastModified():获取资源最后修改时间戳,如果资源无法解析则抛出异常.
* Resource createRelative(String relativePath):相对当前资源创建新的资源对象,如果相对的资源无法解析则抛出异常.
* String getFilename():获取当前资源的文件名,如果当前资源没有文件名则返回null.
* String getDescription():获取当对资源的描述信息.
* InputStream getInputStream():获取当前资源代表的输入流,调用者在使用完毕后必须关闭该资源,除了InputStreamResource以外,其它Resource实现类每次调用getInputStream()方法都将返回一个全新的java.io.InputStream字节流.
#### Resources接口主要实现类
* ClassPathResource:获取类路径下的资源文件.
* UrlResource:是针对于URL进行封装的Resource,可用来从URL获取资源内容.
* ServletContextResource:ServletContext封装的资源,用于访问.
* ServletContext环境下的资源.
* ByteArrayResource:是针对于字节数组封装的Resource,用来从字节数组获取资源内容.
* FileSystemResource:可以用来获取文件系统里面的资源,也可以获取到其对应的输出流OutputStream进行写操作.
* InputStreamResource:是针对于输入流的Resource,其getInputStream()方法只能被调用一次.
* VfsResource:是针对Jboss虚拟文件系统资源,不考虑.
* ServletContextResource:代表web应用资源,用于简化servlet容器的ServletContext接口的getResource操作和getResourceAsStream操作.效果类似于request.getServletContext().getRealPath("").
#### 在bean中获取Resource的方式
* 直接通过内置Resource的实现来获取对应的Resource
    * ClassPathResource:
    ~~~
    Resource resource = new ClassPathResource("data.db");
    String fileName = resource.getDescription();
    System.out.println(fileName);
    if (resource.isReadable()) {
        //每次都会打开一个新的流
        InputStream is = resource.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line=br.readLine()) != null) {
            System.out.println(line);
        }
        if (is != null) {
            is.close();
        }
        if (br != null) {
            br.close();
        }
    }
    ~~~
    * FileSystemResource:
    ~~~
    FileSystemResource resource = new FileSystemResource("d:/abc.properties");
    if (resource.isReadable()) {
        InputStream is = resource.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line=br.readLine()) != null) {
            System.out.println(line);
        }
        if (is != null) {
            is.close();
        }
        if (br != null) {
            br.close();
        }
    }
    if (resource.isWritable()) {
        //每次都会获取到一个新的输出流
        OutputStream os = resource.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        bw.write("FileSystemResource实现类测试。");
        bw.flush();
        if (os != null) {
            os.close();
        }
        if (bw != null) {
            bw.close();
        }
    }
    ~~~
    * UrlResource:
    ~~~
    UrlResource resource = new UrlResource("http://www.baidu.com");
    if (resource.isReadable()) {
        InputStream is = resource.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line=br.readLine()) != null) {
            System.out.println(line);
        }
        if (is != null) {
            is.close();
        }
        if (br != null) {
            br.close();
        }
    }
    ~~~
    * ByteArrayResources:
    ~~~
    ByteArrayResource resource = new ByteArrayResource("Hello".getBytes());
    if (resource.isReadable()) {
        InputStream is = resource.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line=br.readLine()) != null) {
            System.out.println(line);
        }
        if (is != null) {
            is.close();
        }
        if (br != null) {
            br.close();
        }
    }
    ~~~
    * InputStreamResource:
    ~~~
    InputStream is = new FileInputStream("D:/abc.properties");
    InputStreamResource resource = new InputStreamResource(is);
    //对于InputStreamResource而言，其getInputStream()方法只能调用一次，继续调用将抛出异常。
    InputStream target = resource.getInputStream();   //返回的就是构件时的那个InputStream
    if (resource.isReadable()) {
        BufferedReader br = new BufferedReader(new InputStreamReader(target));
        String line;
        while ((line=br.readLine()) != null) {
            System.out.println(line);
        }
        if (is != null) {
            is.close();
        }
        if (br != null) {
            br.close();
        }
    }
    ~~~
* 直接创建DefaultResourceLoader的实例,再调用其getResource(String location)方法获取对应的Resource.
    * spring的ResourceLoader接口getResource方法的策略顺序(ClassPathResource->UrlResource->ClassPathResource):
        * 首先判断指定的location是否含有“classpath:”前缀,如果有则把location去掉“classpath:”前缀返回对应的ClassPathResource.
        * 否则就把它当做一个URL来处理,封装成一个UrlResource进行返回.
        * 如果当成URL处理也失败的话就把location对应的资源当成是一个ClassPathResource进行返回.
    * 例子
    ~~~
    //ResourceLoader还可以通过实现ResourceLoaderAware接口或者使用@Autowired注解注入的方式获取
    ResourceLoader loader = new DefaultResourceLoader();
    Resource resource = loader.getResource("http://www.baidu.com");
    System.out.println(resource instanceof UrlResource); //true
    //注意这里前缀不能使用“classpath*:”，这样不能真正访问到对应的资源，exists()返回false
    resource = loader.getResource("classpath:test.txt");
    System.out.println(resource instanceof ClassPathResource); //true
    resource = loader.getResource("test.txt");
    System.out.println(resource instanceof ClassPathResource); //true
    ~~~
* 在bean里面获取到对应的ApplicationContext,再通过ApplicationContext的getResource(String path)方法获取对应的Resource.
    * 介绍
        * ApplicationContext接口也继承了ResourceLoader接口，所以它的所有实现类都实现了ResourceLoader接口，都可以用来获取Resource。
        * 常见的ApplicationContext的实现类都继承抽象类AbstractApplicationContext,而该抽象类继承了DefaultResourceLoader,所以获取resource也是用的DefaultResourceLoader的策略.
        * FileSystemXmlApplicationContext也继承了DefaultResourceLoader,但是它重写了DefaultResourceLoader的getResourceByPath.所以其策略():
            * 首先判断指定的location是否含有“classpath:”前缀,如果有则把location去掉“classpath:”前缀返回对应的ClassPathResource.
            * 否则继续尝试把location封装成一个URL,返回对应的UrlResource.
            * 如果还是失败,则把location指定位置的资源当做一个FileSystemResource进行返回(这条和DefaultResourceLoader不一样).
    * 例子
    ~~~
    // TODO 获取spring上下文
    //通过spring context获取Resource
    Resource resource = context.getResource("application.properties");
    if (resource.isReadable()) {
        //URLConnection对应的getInputStream()。
        if (resource.isReadable()) {
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line=br.readLine()) != null) {
                System.out.println(line);
            }
            if (is != null) {
                is.close();
            }
            if (br != null) {
                br.close();
            }
        }
    }
    ~~~
* 通过依赖注入的方式把Resource注入到bean中
    * 比如有redis.xml
    * 在某个地方需要加载
    ~~~
    @ContextConfiguration(locations = {"classpath:applicationContext.xml"})
    ~~~






















