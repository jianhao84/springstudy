### Conditional和它的派生注解
#### @Conditional注解
* 介绍:
    * @Conditional注解是可以根据一些自定义的条件动态的选择是否加载该bean到springIOC容器中去,SpringBoot就是利用这个特性进行自动配置的.
    * 使用的时候,都要有一个实现Condition接口的条件控制类来配合.
    * 当给定的类型、类名、注解、昵称在beanFactory中存在时返回true.各类型间是or的关系.
* Conditionnal作为注册bean实例的条件的几个场景
    * 场景1:根据是否存在某特定的系统环境变量来注册bean.
        * 定义条件控制类
        ~~~
        public class MySqlDbTypeCondition implements Condition {
            @Override
            public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
                final String dbType = System.getProperty("dbType");
                return "mysql".equalsIgnoreCase(dbType);
            }
        }
        ~~~
        * 配置类:当spring加载JdbcUserDaoImpl的时候会判断@Conditional指定的条件,如果返回true则将配置类加载.
        ~~~
        @Repository
        @Conditional(MySqlDbTypeCondition.class)
        public class JdbcUserDaoImpl implements UserDao {
            @Override
            public  JdbcUserDaoImpl() {
                System.out.printf("加载成功");
            }
        }
        ~~~
        * 测试类
        ~~~
        @RunWith(SpringJUnit4ClassRunner.class)
        @SpringBootTest(classes = SpringApplicationBoot.class)
        public class UserDaoTest {
            @Autowired
            private UserDao userDao;
            @BeforeClass
            public static void init() {
               System.setProperty("dbType", "mysql");
            }
            
            @Test
            public void testMysqlDbType() {
                System.out.println("=====================");  
            }
        }
        ~~~
    * 场景2:根据在classpath路径中是否存在某个特定的类来注册bean
        * 定义条件控制类
        ~~~
        public class MySqlDriverPresentsCondition implements Condition {
            @Override
            public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
                try {
                    // 是否存在com.mysql.jdbc.Driver
                    Class.forName("com.mysql.jdbc.Driver");
                    return true;
                } catch (ClassNotFoundException e) {
                    return false;
                }  
            }
        }
        ~~~
        * 配置类:当spring加载JdbcUserDaoImpl的时候会判断@Conditional指定的条件,如果返回true则将配置类加载.
        ~~~
        @Repository
        @Conditional(MySqlDbTypeCondition.class)
        public class JdbcUserDaoImpl implements UserDao {
            @Override
            public  JdbcUserDaoImpl() {
                System.out.printf("加载成功");
            }
        }
        ~~~
        * 测试类
        ~~~
        @RunWith(SpringJUnit4ClassRunner.class)
        @SpringBootTest(classes = SpringApplicationBoot.class)
        public class UserDaoTest {
            @Autowired
            private UserDao userDao;

            @Test
            public void testMysqlDbType() {
                System.out.println("=====================");  
            }
        }
        ~~~
    * 其他场景就不一一实现了,都简单
        * 可以做开关注解是否存在加载配置类.
        * 可以根据开关注解的属性值决定是否加载配置类.
        * 可以根据properties文件的属性来决定是否加载配置类.
#### Conditionnal的派生注解
* 简介:
    * Conditionnal的派生注解都在org.springframework.boot.autoconfigure.condition中.
    * Conditionnal的派生注解对应的处理类都springboot实现了,都继承SpringBootCondition外,还实现了ConfigurationCondition接口(ConfigurationCondition继承Condition).
* 源码注释介绍
    * @ConditionalOnBean:
        * 注解介绍:
            * 对应的处理类(条件控制类)是OnBeanCondition.
            * 容器中存在指定Bean就将配置类载入容器.
            * 注解中多个属性之间,或同一个属性的多个值之间基本都是或的关系,即满足一个属性或满足一个属性中的一个值就可以了.
        * 源码注释:
        ~~~
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @Conditional(OnBeanCondition.class)
        public @interface ConditionalOnBean {
            // bean的类型,当ApplicationContext包含给定类的bean时返回true
            Class<?>[] value() default {};
            
            // bean的类型名,当ApplicationContext包含给定的id时返回true   
            String[] type() default {};
            
            // bean所声明的注解,当ApplicationContext中存在声明该注解的bean时返回true
            Class<? extends Annotation>[] annotation() default {};
            
            // bean的id,当ApplicationContext中存在给定id的bean时返回true
            String[] name() default {};
            
            /**默认是所有上下文搜索
             * CURRENT:查询当前的context
             * PARENTS:查询所有的祖先和父辈容器,但是不包含当前容器(从1.5开始废弃,推荐使用ANCESTORS)
             * ANCESTORS:搜索所有的祖先,不搜索当前的context
             * ALL:搜索整个上下文
             */
            SearchStrategy search() default SearchStrategy.ALL;
        }
        ~~~
    * @ConditionalOnSingleCandidate:
        * 注解介绍:
            * 对应的处理类(条件控制类)是OnBeanCondition.
            * 容器中只有一个指定的Bean或者有多个但这个Bean是首选Bean的时候就将配置类载入容器.
            * value和type属性不能同时出现,只能使用一个.
        * 源码注释:
        ~~~
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @Conditional(OnBeanCondition.class)
        public @interface ConditionalOnSingleCandidate {
            /**
             * bean的类型,当ApplicationContext包含给定类的bean时或如果有多个该类型的bean并且指定为primary的
             * 存在则返回true.
             */
            Class<?> value() default Object.class;
            
            /**
             * bean的类型名,当ApplicationContext包含给定的id或如果有多个该类型的bean并且指定为primary的
             * 存在则返回true.
             */
            String type() default "";
            
            /**默认是所有上下文搜索
             * CURRENT:查询当前的context
             * PARENTS:查询所有的祖先和父辈容器,但是不包含当前容器(从1.5开始废弃,推荐使用ANCESTORS)
             * ANCESTORS:搜索所有的祖先,不搜索当前的context
             * ALL:搜索整个上下文
             */
            SearchStrategy search() default SearchStrategy.ALL;
        }
        ~~~
    * @ConditionalOnMissingBean:
        * 注解介绍:
            * 对应的处理类(条件控制类)是OnBeanCondition.
            * 仅仅在上下文中不存在某个对象时,才会实例化一个Bean(如果存在它修饰的类的bean,则不需要再创建这个bean).
            * 各属性之间是or(或)的关系.
            * 使用举例:@ConditionOnMissingBean(name="example"),这个表示如果name为"example"的bean存在,这该注解修饰的代码块不执行。
        * 源码注释
        ~~~
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @Conditional(OnBeanCondition.class)
        public @interface ConditionalOnMissingBean {
            // bean的类型,当ApplicationContext不包含给定类的bean时返回true
            Class<?>[] value() default {};
            
            // bean的类型名,当ApplicationContext不包含给定的id时返回true
            String[] type() default {};
            
            // 给定的类型当进行匹配时进行忽略
            Class<?>[] ignored() default {};
    
            // 给定的类型名当进行匹配时进行忽略
            String[] ignoredType() default {};
            
            // bean所声明的注解,当ApplicationContext中不存在声明该注解的bean时返回true
            Class<? extends Annotation>[] annotation() default {};
            
            // bean的id,,当ApplicationContext中不存在给定id的bean时返回true
            String[] name() default {};
            
            // 默认是所有上下文搜索
            SearchStrategy search() default SearchStrategy.ALL;
        }
        ~~~
    * @ConditionalOnClass
        * 注解介绍:
            * 对应的处理类(条件控制类)是OnClassCondition.
            * 某个class位于类路径上，才会实例化一个Bean(该注解的参数对应的类必须存在，否则不解析该注解修饰的配置类).
            * value和name的属性可以不一样,是and的关系.
        * 源码注释
        ~~~
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @Conditional(OnClassCondition.class)
        public @interface ConditionalOnClass {
        
            /**
             * 给定的类必须存在
             * @return the classes that must be present
             */
            Class<?>[] value() default {};
            
            /**
             * 给定的类名,该类名必须存在
             * @return the class names that must be present.
             */
            String[] name() default {};
        }
        ~~~
    * @ConditionalOnMissingClass
        * 注解介绍
            * 对应的处理类(条件控制类)是OnClassCondition.
            * 某个class类路径上不存在的时候，才会实例化一个Bean(该注解的参数对应的类必须不存在，否则不解析该注解修饰的配置类).
            * 各属性之间是and的关系.
        * 源码注释
        ~~~
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @Conditional(OnClassCondition.class)
        public @interface ConditionalOnMissingClass {
            // 给定的类名在当前类路径下不存在时返回true
            String[] value() default {};
        }
        ~~~
    * @ConditionalOnCloudPlatform
        * 注解介绍
            * 对应的处理类(条件控制类)是OnCloudPlatformCondition.
            * 当所配置的CloudPlatform为激活时返回true.
            * 国内用不上,因为枚举支持的2个枚举值都是国外的云平台.
    * @ConditionalOnExpression
        * 注解介绍
            * 对应的处理类(条件控制类)是OnExpressionCondition.
            * spel表达式执行为true的时候实例化被修饰的配置类.
            * 举例:
                * @ConditionalOnExpression("${test.enabled:true}"):被修饰的配置类只有在配置文件中test.enabled为true才被实例化.
                * @ConditionalOnExpression: 默认是"true",永远加载BasicConfiguration的配置.
        * 源码注释
        ~~~
        @Retention(RetentionPolicy.RUNTIME)
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Documented
        @Conditional(OnExpressionCondition.class)
        public @interface ConditionalOnExpression {
        
            // 如果该表达式返回true则代表匹配,否则返回不匹配
            String value() default "true";
        }
        ~~~
    * @ConditionalOnJava
        * 注解介绍
            * 对应的处理类(条件控制类)是OnJavaCondition.
            * 基于JVM版本作为判断条件.
        * 源码注释
        ~~~
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @Conditional(OnJavaCondition.class)
        public @interface ConditionalOnJava {
            /**
             * 表明是大于等于配置的JavaVersion还是小于配置的JavaVersion.
             * 枚举值有:
             *  EQUAL_OR_NEWER:大于或者等于给定的JavaVersion
             *  OLDER_THAN:小于给定的JavaVersion
             */
            Range range() default Range.EQUAL_OR_NEWER;
            
            /**
             * 配置要检查的java版本.使用range属性来表明大小关系
             * 枚举值有(也可以自己构造该枚举):
             *  SIX(JDK1.6)
             *  SEVEN(JDK1.7)
             *  EIGHT(JDK1.8)
             *  NINE(JDK1.9)
             * @return the java version
             */
            JavaVersion value();
        }
        ~~~
    * @ConditionalOnJndi
        * 注解介绍
            * 对应的处理类(条件控制类)是ConditionalOnJndi.
            * 指定的JNDI存在时实例化配置类.
            * 举例:@ConditionalOnJndi("java:appserver/TransactionManager","java:pm/TransactionManager"):
        * 源码注释
        ~~~
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @Conditional(OnJndiCondition.class)
        public @interface ConditionalOnJndi {
        
            // 给定的jndi的Location 必须存在一个.否则,返回不匹配
            String[] value() default {};
        }
        ~~~
    * @ConditionalOnWebApplication
        * 注解介绍
            * 对应的处理类(条件控制类)是OnWebApplicationCondition.
            * 当前项目是Web项目就实例化配置类.
            * 只是一个标记注解,没有属性.
    * @ConditionalOnNotWebApplication
        * 注解介绍
            * 对应的处理类(条件控制类)是OnWebApplicationCondition.
            * 当前项目是非Web项目就实例化配置类.
            * 只是一个标记注解,没有属性.
    * @ConditionalOnProperty
        * 注解介绍
            * 对应的处理类(条件控制类)是OnPropertyCondition.
            * 指定的属性在配置文件中是否有指定的值的时候实例化配置类.
            * 举例:@ConditionalOnProperty(prefix="spring.aop",name="auto",havingValue ="true",matchIfMissing=true):配置了spring.aop.auto并且值为true时匹配,或者spring.aop.auto没配置时匹配.
        * 源码注释
        ~~~
        @Retention(RetentionPolicy.RUNTIME)
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Documented
        @Conditional(OnPropertyCondition.class)
        public @interface ConditionalOnProperty {
        
            // name属性的别名
            String[] value() default {};
        
            // 属性前缀,如果该前缀不是.结尾的,则会自动加上
            String prefix() default "";
        
            // 属性名,如果前缀被声明了,则会拼接为prefix+name 去查找.通过-进行分割单词,name需要为小写
            String[] name() default {};
        
            // 表明所期望的结果,如果没有指定该属性,则该属性所对应的值不为false时才匹配
            String havingValue() default "";
        
            // 表明配置的属性如果没有指定的话,是否匹配,默认不匹配
            boolean matchIfMissing() default false;
        
            // 是否支持relaxed(松散匹配). 默认支持
            boolean relaxedNames() default true;
        }
        ~~~
    * @ConditionalOnResource
        * 注解介绍
            * 对应的处理类(条件控制类)是OnResourceCondition.
            * 在classpath下存在指定的resource时实例化配置类.
            * 举例:@ConditionalOnResource(resources="mybatis.xml"):如果有mybatis.xml这个文件就实例化配置类.
        * 源码注释
        ~~~
        @Target({ ElementType.TYPE, ElementType.METHOD })
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @Conditional(OnResourceCondition.class)
        public @interface ConditionalOnResource {
            // 指定的资源必须存在,否则返回不匹配
            String[] resources() default {};
        }
        ~~~