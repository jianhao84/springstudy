### Spring Aware容器感知技术
#### Spring Aware容器感知技术
* 只要实现Spring提供Aware接口能让Bean感知Spring容器的存在，即让Bean可以使用Spring容器所提供的资源。
#### Spring Aware的分类
* ApplicationContextAware:实现接口能获取ApplicationContext调用容器的服务.
* ApplicationEventPublisherAware:应用事件发布器,可以用来发布事件.
* BeanClassLoaderAware:能获取加载当前Bean的类加载器.
* BeanFactoryAware:能获取Bean Factory调用容器的服务.
* BeanNameAware:能获取当前Bean的名称.
* EnvironmentAware:能获取当前容器的环境属性信息.
* MessageSourceAware:能获取国际化文本信息.
* ResourceLoaderAware:获取资源加载器读取资源文件.
* ServletConfigAware:能获取到ServletConfig.
* ServletContextAware:能获取到ServletContext.
#### 举例
* 常用的例子就是实现ApplicationContextAware获取到context,然后通过context获取bean(见ApplicationContext获取方式.md).这里以beanNameAware为例:
    * 实现类:通过实现BeanNameAware接口的方法得到beanName.
    ~~~
    @Component
    public class TestBeanNameAware implements BeanNameAware {
        private String beanName;
    
        @Override
        public void setBeanName(String name) {
            this.beanName = name;
        }
    
        public void show() {
            System.out.println("beanName:" + beanName);
        }
    }
    ~~~
    * 启动类
    ~~~
    @SpringBootApplication
    public class AwareStart {
        public static void main(String[] args) {
            ApplicationContext context = SpringApplication.run(AwareStart.class,args);
            context.getBean(TestBeanNameAware.class).show();
        }
    }
    ~~~
    * 结果:
    ~~~
    beanName:testBeanNameAware
    ~~~