### ApplicationContext获取方式

#### 背景
* 在实际Web项目启动时，Web服务器就会初始化加载Spring容器,如果在程序运行期间，再次通过加载XMl的方式获取,相当于重复加载，无疑是非常耗时的,其次我们在开发中获取ApplicationContext大多是为了抽成Util类或者getBean的Static方法.
* 这里主要讲解基于实际开发中获取Spring上下文的方式，区别于通过加载xml的方式(ClassPathXmlApplicationContext()和FileSystemXmlApplicationContext())。

#### 3种获取方式
一、直接注入方式
* @Autowired、@Resource、@Inject都可以,只是@Inject需要额外依赖包.
~~~
public class User {
    @Autowired
    //@Resource
    //@Inject
    private ApplicationContext applicationContext;

    public void show() {
        System.out.println("applicationContext = " + applicationContext.getClass());
    }
}
~~~
二、实现ApplicationContextAware接口方式
* Spring容器会检测容器中的所有Bean，如果发现某个Bean实现了ApplicationContextAware接口，Spring容器会在创建该Bean之后，自动调用该Bean的setApplicationContextAware()方法，调用该方法时，会将容器本身作为参数传给该方法——该方法中的实现部分将Spring传入的参数（容器本身）赋给该类对象的applicationContext实例变量，因此接下来可以通过该applicationContext实例变量来访问容器本身。
~~~
@Component
public class Book implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void show() {
        System.out.println("applicationContext = " + applicationContext.getClass());
    }
}
~~~
三、使用构造器,在构造器里传入
* 这是spring4.3的新特性,需要注意2点:
    * 构造函数只能有一个,这也是该方式的这个局限性导致其他类中使用该类不方便.
    * 构造器参数必须在spring容器中
~~~
@Component
public class Bank {

    private ApplicationContext applicationContext;

    public Bank(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void show() {
        System.out.println("applicationContext = " + applicationContext.getClass());
    }
}
~~~
四、整合测试:
1. 创建测试类
    * 创建一个实体类,如下:
    ~~~
    @Component
    @Data
    public class TestBean {
        private String msg="啦啦啦啦";
    }
    ~~~
    * 创建整合测试类
    ~~~
    @Component
    @Data
    public class ApplicationContextUtil implements ApplicationContextAware {
    
        @Autowired
        private ApplicationContext applicationContext_autowired;
        private ApplicationContext applicationContext_implements;
        private ApplicationContext applicationContext_struct;
    
        /**
         * 展示测试结果方法
         */
        public void show() {
            show_autowired();
            show_struct();
            show_implements();
        }
    
        /**
         * 直接注入方式
         */
        public void show_autowired() {
            System.out.println("----------直接注入方式方式获取applicationContext------------------");
            System.out.println("byName方式获取bean:========================:" + applicationContext_autowired.getBean("testBean"));
            System.out.println("byType方式获取bean:========================:" + applicationContext_autowired.getBean(TestBean.class));
        }
    
        /**
         * 构造器传入方式
         */
        public void  show_struct() {
            System.out.println("----------构造器传入方式获取applicationContext------------------");
            System.out.println("byName方式获取bean:========================:" + applicationContext_struct.getBean("testBean"));
            System.out.println("byType方式获取bean:========================:" + applicationContext_struct.getBean(TestBean.class));
        }
    
        /**
         * 实现ApplicationContextAware接口方式
         */
        public void  show_implements() {
            System.out.println("----------实现ApplicationContextAware接口方式获取applicationContext------------------");
            System.out.println("byName方式获取bean:========================:" + applicationContext_implements.getBean("testBean"));
            System.out.println("byType方式获取bean:========================:" + applicationContext_implements.getBean(TestBean.class));
        }
    
        /**
         *使用构造器,在构造器里传入applicationContext
         * @param applicationContext
         */
        public ApplicationContextUtil(ApplicationContext applicationContext) {
            this.applicationContext_struct = applicationContext;
        }
    
        /**
         *实现ApplicationContextAware接口setApplicationContext方法
         * @param applicationContext
         * @throws BeansException
         */
        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            this.applicationContext_implements = applicationContext;
        }
    }
    ~~~
    * spring启动类:为了方便测试,直接在启动类里调用
    ~~~
    @SpringBootApplication
    public class StartupServer {
        public static void main(String[] args) {
            ApplicationContext context = SpringApplication.run(StartupServer.class, args);
            // 调用工具类的show方法进行测试
            context.getBean(ApplicationContextUtil.class).show();
        }
    }
    ~~~
2. 测试结果
~~~
----------直接注入方式方式获取applicationContext------------------
byName方式获取bean:========================:TestBean(msg=啦啦啦啦)
byType方式获取bean:========================:TestBean(msg=啦啦啦啦)
----------构造器传入方式获取applicationContext------------------
byName方式获取bean:========================:TestBean(msg=啦啦啦啦)
byType方式获取bean:========================:TestBean(msg=啦啦啦啦)
----------实现ApplicationContextAware接口方式获取applicationContext------------------
byName方式获取bean:========================:TestBean(msg=啦啦啦啦)
byType方式获取bean:========================:TestBean(msg=啦啦啦啦)
~~~
3. 结论
3种方式都能获取到applicationContext