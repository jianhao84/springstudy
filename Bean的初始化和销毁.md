### Bean的初始化和销毁方式
#### 方式一:使用@Bean注解的参数(和xml配置init-method和destroy-method一样,xml配置现在基本少用了)
* 步骤:
    * 定义一个实例类，并在实例类中定义初始化方法和销毁方法
    ~~~
    @Getter
    public class Bike {
        private String name;
        // 设置属性
        public void setName(String name) {
            this.name = name;
            System.out.println("setName");
        }
        public Bike() {
            System.out.println("构造方法");
        }
        public void init() {
            System.out.println("init");
        }
        public void destroy() {
            System.out.println("destroy");
        }
    }
    ~~~
    * 定义一个spring配置类,用@Configuration注解标记,在bean注解中指定初始化和注销方法
    ~~~
    @Configuration
    public class TestConfig {
        @Bean(initMethod = "init",destroyMethod = "destroy")
        public Bike getBike() {
            Bike bike = new Bike();
            bike.setName("周剑");
            return bike;
        }
    }
    ~~~
#### 方式二:使用@PostConstruct和@PreDestroy注解
* 需要注意:
    * 只能有一个方法可以用此注释进行注释，方法不能有参数，返回值必需是void,方法需要是非静态的。
* 步骤:
    * 定义一个实体类,并在实例类中定义初始化方法和销毁方法,并分别用@PostConstruct和@PreDestroy注解标记.
    ~~~
    @Getter
    public class Bike2 {
        private String name;
        public void setName(String name) {
            this.name = name;
            System.out.println("setName");
        }
        public Bike2() {
            System.out.println("构造方法");
        }
        @PostConstruct
        public void init() {
            System.out.println("init");
        }
        @PreDestroy
        public void destroy() {
            System.out.println("destroy");
        }
    }
    ~~~
    * 定义一个配置类，配置了@Configuration注解
    ~~~
    @Configuration
    public class TestConfig2 {
        @Bean
        public Bike2 getBike() {
            Bike2 bike = new Bike2();
            bike.setName("周剑");
            return bike;
        }
    }
    ~~~
    
#### 方式三:继承InitializingBean和DisposableBean方法
* 步骤:
    * 定义一个实体类实现InitializingBean和DisposableBean接口.
    ~~~
    @Getter
    public class Bike3 implements InitializingBean,DisposableBean {
        private String name;
        public void setName(String name) {
            this.name = name;
            System.out.println("setName");
        }
        public Bike3() {
            System.out.println("构造方法");
        }
        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("init");
        }
        @Override
        public void destroy() {
            System.out.println("destroy");
        }
    }
    ~~~
    * 定义配置类,用@Configuration标记.
    ~~~
    @Configuration
    public class TestConfig3 {
        @Bean
        public Bike3 getBike() {
            Bike3 bike = new Bike3();
            bike.setName("周剑");
            return bike;
        }
    }
    ~~~
#### 测试:
1. 混合顺序测试:上面有3种方式单独的代码,这里提供一份包含3种的代码,来测试执行顺序
    
    * 创建一个实例类,包含以上3种方式
    ~~~
    @Getter
    public class Bike4  implements InitializingBean,DisposableBean {
        private String name;
        // 设置属性
        public void setName(String name) {
            this.name = name;
            System.out.println("setName");
        }
        public Bike4() {
            System.out.println("构造方法");
        }
    //-----------方式一:通过@Bean注解的initMethod和destroyMethod参数指定--------
        public void method1_init() {
            System.out.println("方式一:bean指定的initMethod方法执行...");
        }
    
        public void method1_destroy() {
            System.out.println("方式一:bean指定的destroyMethod方法执行...");
        }
    //-------方式二:使用@PostConstruct和@PreDestroy注解------------------------
        @PostConstruct
        public void method2_init() {
            System.out.println("方式二:PostConstruct注解方式的init");
        }
    
        @PreDestroy
        public void method2_destroy() {
            System.out.println("方式二:PreDestroy注解方式的destroy");
        }
    //----------方式三:继承InitializingBean和DisposableBean方法---------------------
        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("方式三:实现InitializingBean接口的方式init");
        }
        @Override
        public void destroy() {
            System.out.println("方式三:实现DisposableBean接口的方式destroy");
        }
    }
    ~~~
    * 为混合顺序测试创建一个配置类:
    ~~~
    @Configuration
    public class TestConfig4 {
        @Bean(initMethod = "method1_init",destroyMethod = "method1_destroy")
        public Bike4 getBike() {
            Bike4 bike = new Bike4();
            bike.setName("周剑");
            return bike;
        }
    }
    ~~~
2. 多例测试:上面test1-test3是单个方式的测试,test4是顺序测试.
    * 实例类
    ~~~
    @Getter
    public class Bike5 implements InitializingBean,DisposableBean {
    
        private String name;
        // 设置属性
        public void setName(String name) {
            this.name = name;
            System.out.println("setName");
        }
    
        public Bike5() {
            System.out.println("构造方法");
        }
    
    //-----------方式一:通过@Bean注解的initMethod和destroyMethod参数指定--------
        public void method1_init() {
            System.out.println("方式一:bean指定的initMethod方法执行...");
        }
    
        public void method1_destroy() {
            System.out.println("方式一:bean指定的destroyMethod方法执行...");
        }
    //-------方式二:使用@PostConstruct和@PreDestroy注解------------------------
        @PostConstruct
        public void method2_init() {
            System.out.println("方式二:PostConstruct注解方式的init");
        }
    
        @PreDestroy
        public void method2_destroy() {
            System.out.println("方式二:PreDestroy注解方式的destroy");
        }
    //----------方式三:继承InitializingBean和DisposableBean方法---------------------
        @Override
        public void afterPropertiesSet() throws Exception {
            System.out.println("方式三:实现InitializingBean接口的方式init");
        }
        @Override
        public void destroy() {
            System.out.println("方式三:实现DisposableBean接口的方式destroy");
        }
    }
    ~~~
    * 配置类:bean是多例的.
    ~~~
    @Configuration
    public class TestConfig5 {
    
        public TestConfig5() {
            System.out.println("TestConfig5容器启动初始化...");
        }
    
        @Bean(initMethod = "method1_init",destroyMethod = "method1_destroy")
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public Bike5 getBike() {
            Bike5 bike = new Bike5();
            bike.setName("周剑");
            return bike;
        }
    }
    ~~~
3. 测试类:
~~~
public class TestCase {
    @Test
    public void test1() {
        System.out.println("------方式一:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig.class);

        System.out.println(context.getBean(Bike.class));
        context.close();

    }
    @Test
    public void test2() {
        System.out.println("------方式二:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig2.class);
        System.out.println(context.getBean(Bike2.class));
        context.close();
    }
    @Test
    public void  test3() {
        System.out.println("------方式三:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig3.class);
        System.out.println(context.getBean(Bike3.class));
        context.close();
    }
    @Test
    public void  test4() {
        System.out.println("------混合顺序测试:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig4.class);
        System.out.println(context.getBean(Bike4.class));
        context.close();
    }
    @Test
    public void test5() {
        System.out.println("------多例测试:------------------------------");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TestConfig5.class);
        System.out.println(context.getBean(Bike5.class));
        context.close();
    }
}
~~~
3. 测试结果:
~~~
------方式一:------------------------------
构造方法
setName
init
com.hqch.test.testinitdestroy.Bike@6404f418
destroy
------方式二:------------------------------
构造方法
setName
init
com.hqch.test.testinitdestroy.Bike2@290222c1
destroy
------方式三:------------------------------
构造方法
setName
init
com.hqch.test.testinitdestroy.Bike3@61001b64
destroy
------混合顺序测试:------------------------------
TestConfig4容器启动初始化...
构造方法
setName
方式二:PostConstruct注解方式的init
方式三:实现InitializingBean接口的方式init
方式一:bean指定的initMethod方法执行...
com.hqch.test.testinitdestroy.Bike4@2dc54ad4
方式二:PreDestroy注解方式的destroy
方式三:实现DisposableBean接口的方式destroy
方式一:bean指定的destroyMethod方法执行...
------多例测试:------------------------------
TestConfig5容器启动初始化...
构造方法
setName
方式二:PostConstruct注解方式的init
方式三:实现InitializingBean接口的方式init
方式一:bean指定的initMethod方法执行...
com.hqch.test.testinitdestroy.test5.Bike5@7d4f9aae
~~~
3. 结果分析:
* 三种方式都是首先执行的都是改造方法,然后执行设置属性方法.
* 设置属性执行完后才会执行各种方式的初始化方法.
* 三种方式的初始化完成后方法的执行顺序是:@PostConstruct注解方式、实现InitializingBean接口方式、@Bean的initMethod参数指定方式.
* 三种方式的注销前方法的执行顺序是:@PreDestroy注解方式、实现DisposableBean接口方式、@Bean的destroyMethod参数指定方式.
* 如果bean的作用于是多例(prototype),Spring不能对一个多例bean的整个生命周期负责:容器在初始化、配置、装饰或者是装配完一个prototype实例后,将它交给客户端,随后就对该多例bean就不管了,所以只有init方法被调用,destroy不会被调用.