### Spring中SpEL详解
#### 介绍
* SpEL在求表达式值的步骤:
    * 创建解析器:SpEL使用ExpressionParser接口表示解析器,提供SpelExpressionParser默认实现.
    * 解析表达式:使用ExpressionParser的parseExpression来解析相应的表达式为Expression对象.
    * 构造上下文:准备比如变量定义等等表达式需要的上下文数据.
    * 求值:通过Expression接口的getValue方法根据上下文获得表达式值.
* 几个概念:
    * 表达式:表达式是表达式语言的核心,所以表达式语言都是围绕表达式进行的,从我们角度来看是“干什么”.
    * 解析器:用于将字符串表达式解析为表达式对象,从我们角度来看是“谁来干”.
    * 上下文:表达式对象执行的环境,该环境可能定义变量、定义自定义函数、提供类型转换等等,从我们角度看是“在哪干”.
    * 根对象及活动上下文对象:根对象是默认的活动上下文对象,活动上下文对象表示了当前表达式操作的对象,从我们角度看是“对谁干”.
* SpEL的主要接口
    * ExpressionParser接口:
        * 表示解析器,默认实现是org.springframework.expression.spel.standard包中的SpelExpressionParser类.
        * 使用parseExpression方法将字符串表达式转换为Expression对象,对于ParserContext接口用于定义字符串表达式是不是模板,及模板开始与结束字符.
        * 接口的2个方法:
        ~~~
        public interface ExpressionParser {
           // 默认
           Expression parseExpression(String expressionString);
           // 传入字符串是否模版形式
           Expression parseExpression(String expressionString, ParserContext context);
        }
        ~~~
        * 举例(拿第二个实现举例)
        ~~~
        @Test
        public void testParserContext() {
            // 创建解析器
            ExpressionParser parser = new SpelExpressionParser();
            ParserContext parserContext = new ParserContext() {
                @Override
                 public boolean isTemplate() {
                    // 定义字符串表达式是模版
                    return true;
                }
                @Override
                public String getExpressionPrefix() {
                    // 定义表达式前缀
                    return "#{";
                }
                @Override
                public String getExpressionSuffix() {
                    // 定义表达式后缀
                    return "}";
                }
            };
            String template = "#{'Hello '}#{'World!'}";
            Expression expression = parser.parseExpression(template, parserContext);
            System.out.println(expression.getValue());
        }
        ~~~
    * EvaluationContext接口:
        * 表示上下文环境,默认实现是org.springframework.expression.spel.support包中的StandardEvaluationContext类.
        * 使用setRootObject方法来设置根对象,使用setVariable方法来注册自定义变量,使用registerFunction来注册自定义函数等等.
    * Expression接口:
        * 表示表达式对象,默认实现是org.springframework.expression.spel.standard包中的SpelExpression.
        * 提供getValue方法用于获取表达式值,提供setValue方法用于设置对象值.
#### SpEL语法
##### 基本语法
* 字面量表达式:
    * 字符串
    ~~~
    String str1 = parser.parseExpression("'Hello World!'").getValue(String.class);
    String str2 = parser.parseExpression("\"Hello World!\"").getValue(String.class);
    ~~~
    * 数字类型
    ~~~
    int int1 = parser.parseExpression("1").getValue(Integer.class);
    long long1 = parser.parseExpression("-1L").getValue(long.class);
    float float1 = parser.parseExpression("1.1").getValue(Float.class);
    double double1 = parser.parseExpression("1.1E+2").getValue(double.class);
    ~~~
    * 布尔类型
    ~~~
    boolean true1 = parser.parseExpression("true").getValue(boolean.class);
    ~~~
    * null类型
    ~~~
    Object null1 = parser.parseExpression("null").getValue(Object.class);
    ~~~
* 算数运算表达式:
    * 加减乘除:DIV也可以表示除,不区分大小写.
    ~~~
    int result1 = parser.parseExpression("1+2-3*4/2").getValue(Integer.class);
    ~~~
    * 求余:MOD也可以表示求余,不区分大小写.
    ~~~
    int result2 = parser.parseExpression("4%3").getValue(Integer.class);
    ~~~
    * 幂运算
    ~~~
    int result3 = parser.parseExpression("2^3").getValue(Integer.class);
    ~~~
* 关系表达式
    * 等于(==或EQ)、不等于(!=或NE)、大于(>或GT)、大于等于(>=或GE)、小于(<或LT)、小于等于(<=或LE),区间(between)运算.
    ~~~
    boolean flag1 = parser.parseExpression(“1>2″).getValue(boolean.class);
    boolean flag2 = parser.parseExpression(“1 between {1,2}”).getValue(boolean.class);
    ~~~
* 逻辑表达式
    * 且(and)、或(or)、非(!或NOT).
    ~~~
    String expression1 = "2>1 and (!true or !false)"; 
    boolean result1 = parser.parseExpression(expression1).getValue(boolean.class); 
    String expression2 = "2>1 and (NOT true or NOT false)"; 
    boolean result2 = parser.parseExpression(expression2).getValue(boolean.class);
    ~~~
* 字符串连接及截取表达式
    * 使用"+"进行字符串连接.
    * 使用"String[index]"来截取一个字符,目前只支持截取一个.
* 三目运算及Elivis运算表达式
    * 三目运算符"表达式1?表达式2:表达式3"用于构造三目运算表达式.
    * Elivis运算符"表达式1?:表达式2":是三目运算符的特殊写法,当表达式1为非null时则返回表达式1,当表达式1为null时则返回表达式2.
    * 为了避免操作对象本身可能为null,取属性或调用方法时报错,定义语法:"对象?.变量|方法".当对象为null时,直接返回"null",不会抛出NullPointerException.
    ~~~
    // 三目条件表达式:
    name != null? name : "other";
    // Elivis表达式,和上面条件表达式意思一样.
    name?:"other";
    // 安全操作属性和方法
    list?.length
    list?.add()
    ~~~
* 正则表达式
    * 使用"str matches regex"语法.
    ~~~
    "'123' matches '\\d{3}'"
    ~~~
* 括号优先级表达式:使用“(表达式)”构造，括号里的具有高优先级.
##### 类相关表达式
* 类类型表达式
    * 使用"T(Type)"来表示java.lang.Class实例.
    * "Type"必须是类全限定名,"java.lang"包除外,即lang包下的类可以不指定包名.
    * 使用类类型表达式还可以进行访问类静态方法及类静态字段.
    ~~~
    @Test
    public void testClassTypeExpression() { 
     ExpressionParser parser = new SpelExpressionParser(); 
     //java.lang包类访问 
     Class<String> result1 = parser.parseExpression("T(String)").getValue(Class.class); 
     Assert.assertEquals(String.class, result1); 
     //其他包类访问 
     String expression2 = "T(cn.javass.spring.chapter5.SpELTest)"; 
     Class<String> result2 = parser.parseExpression(expression2).getValue(Class.class); 
     Assert.assertEquals(SpELTest.class, result2); 
     //类静态字段访问 
     int result3=parser.parseExpression("T(Integer).MAX_VALUE").getValue(int.class); 
     Assert.assertEquals(Integer.MAX_VALUE, result3); 
     //类静态方法调用 
     int result4 = parser.parseExpression("T(Integer).parseInt('1')").getValue(int.class); 
     Assert.assertEquals(1, result4); 
    }
    ~~~
* 类实例化
    * 类实例化同样使用java关键字"new".
    * 类名必须是全限定名,但java.lang包内的类型除外,如String、Integer.
    ~~~
    @Test
    public void testConstructorExpression() { 
     ExpressionParser parser = new SpelExpressionParser(); 
     String result1 = parser.parseExpression("new String('haha')").getValue(String.class); 
     Assert.assertEquals("haha", result1); 
     Date result2 = parser.parseExpression("new java.util.Date()").getValue(Date.class); 
     Assert.assertNotNull(result2); 
    }
    ~~~
* instanceof表达式
    * SpEL支持instanceof运算符，跟Java内使用同义.
    ~~~
    //将返回true
    boolean result1 = parser.parseExpression("'haha' instanceof T(String)").getValue(boolean.class); 
    ~~~
* 变量定义及引用
    * 变量定义通过EvaluationContext接口的setVariable(variableName, value)方法定义.
    * 在表达式中使用"#variableName"引用.
    * 获取容器内的变量,可以使用“#变量名”来获取,有两个特殊的变量可以直接使用:
        * "#root":引用容器的root对象.
        * "#this":引用当前正在计算的上下文,始终定义和指向的是当前的执行对象(不支持对其中不合格的引用解析).
    ~~~
    @Test
    public void testVariableExpression() { 
     ExpressionParser parser = new SpelExpressionParser(); 
     EvaluationContext context = new StandardEvaluationContext(); 
     context.setVariable("variable", "haha"); 
     context.setVariable("variable", "haha"); 
     String result1 = parser.parseExpression("#variable").getValue(context, String.class); 
     Assert.assertEquals("haha", result1); 
     
     context = new StandardEvaluationContext("haha"); 
     String result2 = parser.parseExpression("#root").getValue(context, String.class); 
     Assert.assertEquals("haha", result2); 
     // 这里this代表根对象.
     String result3 = parser.parseExpression("#this").getValue(context, String.class); 
     Assert.assertEquals("haha", result3); 
     
     List<Integer> list = new ArrayList();
     list.add(4);
     list.add(5);
     // 对集合中所有元素进行+1操作,其中#this代表集合中所有迭代元素.
     Collection<Integer> result1 = parser.parseExpression("#collection.![#this+1]").getValue(context1, Collection.class);
     System.out.println(result1);
    }
    ~~~
* 自定义函数
    * 只支持类静态方法注册为自定义函数.
    * SpEL使用StandardEvaluationContext的registerFunction方法进行注册自定义函数.
    * 其实完全可以使用setVariable代替,两者其实本质是一样的;
    * 举例:
    ~~~
    @Test
    public void testFunctionExpression() throws SecurityException, NoSuchMethodException {
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        Method parseInt = Integer.class.getDeclaredMethod("parseInt", String.class);
        // registerFunction方式:推荐
        context.registerFunction("parseInt", parseInt);
        // setVariable方式:不推荐
        context.setVariable("parseInt2", parseInt);
        int parse1 = parser.parseExpression("#parseInt('3')").getValue(context,int.class);
        int parse2 = parser.parseExpression("#parseInt2('3')").getValue(context,int.class);
        System.out.println(parse1);
        System.out.println(parse2);
    }
    ~~~
* 赋值表达式
    * SpEL即允许给自定义变量赋值,也允许给跟对象赋值,直接使用“#variableName=value”即可赋值.
    * 通过表达式给#this和#root赋值并不能真正改变其值.
    * 通过表达式给自定义变量赋值可以改变自定义变量的值.
    * 通过context.setRootObject可以真正改变#root的值.
    * 举例:
    ~~~
    @Test
    public void testAssignExpression() {
        ExpressionParser parser = new SpelExpressionParser();
        //1.给root对象赋值
        StandardEvaluationContext context = new StandardEvaluationContext("aaaa");
        String result1 = parser.parseExpression("#root='bbb'").getValue(context, String.class);
        System.out.println("#root='bbb':" + result1);
        String result6 = parser.parseExpression("#root").getValue(context, String.class);
        System.out.println("#root:" + result6);
        String result2 = parser.parseExpression("#this='ccc'").getValue(context, String.class);
        System.out.println("#this='ccc':" + result2);
        String result7 = parser.parseExpression("#this").getValue(context, String.class);
        System.out.println("#this:" + result7);
        //2.给自定义变量赋值
        context.setVariable("name", "dddd");
        context.setVariable("age","12");
        String result3 = parser.parseExpression("#name=#age").getValue(context, String.class);
        System.out.println("#name=#age:" + result3);
        String result5 = parser.parseExpression("#name").getValue(context, String.class);
        System.out.println("#name:" + result5);
        // 用context.setRootObject修改根对象的值.
        context.setRootObject("kkk");
        String result4 = parser.parseExpression("#name=#root").getValue(context, String.class);
        System.out.println("#name=#root:" + result4);
    }
    ~~~
* 对象属性存取及安全导航表达式:
    * 对象属性获取使用如“a.property.property”这种点缀式获取.
    * SpEL对于属性名首字母是不区分大小写的.
    * SpEL还引入了Groovy语言中的安全导航运算符"(对象|属性)?.属性",用来避免但"?."前边的表达式为null时抛出空指针异常,而是返回null;
    * 修改对象属性值则可以通过赋值表达式或Expression接口的setValue方法修改。
    * 举例:
    ~~~
    @Test
    public void test12() {
        ExpressionParser parser = new SpelExpressionParser();
        Person person = new Person("毛毛");
        StandardEvaluationContext context = new StandardEvaluationContext(person);
        //1.访问root对象属性:可以直接属性名,也可以通过#root.属性名访问.
        String result1 = parser.parseExpression("name").getValue(context, String.class);
        System.out.println(result1);
        String result2 = parser.parseExpression("#root.name").getValue(context, String.class);
        System.out.println(result2);
        //2.安全访问
        context.setRootObject(null);
        Integer result3 = parser.parseExpression("#root?.age").getValue(context, Integer.class);
        System.out.println(result3);
        try {
            //3.不安全访问
            Integer result4 = parser.parseExpression("#root.age").getValue(context, Integer.class);
            System.out.println(result4);
        }catch (Exception e) {
            System.out.println("报错了:" + e.getMessage());
        }
        //4.访问对象属性
        Person tt = new Person("周周");
        context.setVariable("tt",tt);
        String result5 = parser.parseExpression("#tt.name").getValue(context, String.class);
        System.out.println(result5);
        //5.设置对象属性
        parser.parseExpression("#tt.age").setValue(context,12);
        Integer result6 = parser.parseExpression("#tt.age").getValue(context, Integer.class);
        System.out.println(result6);
    }
    ~~~
* 对象方法调用:
    * 对象方法调用更简单,跟Java语法一样,如:如"'haha'.substring(2,4)"将返回"ha".
    * 而对于根对象可以直接调用方法.
    * 举例:
    ~~~
    @Test
    public void test32() {
        ExpressionParser parser = new SpelExpressionParser();
        Person person = new Person("吉吉");
        StandardEvaluationContext context = new StandardEvaluationContext(person);
        // 根对象可以直接只写方法名.
        String result1 = parser.parseExpression("getName()").getValue(context, String.class);
        System.out.println(result1);
        Person tt = new Person("毛毛");
        context.setVariable("tt",tt);
        // 普通对象方法访问
        String result2 = parser.parseExpression("#tt.getName()").getValue(context, String.class);
        System.out.println(result2);
    }
    ~~~
* Bean引用:
    * SpEL支持使用“@”符号来引用Bean.
    * 在引用Bean时需要使用BeanResolver接口实现来查找Bean,Spring提供BeanFactoryResolver实现.
    * 举例:
    ~~~
    @Test
    public void testBeanExpression() {
        // spring容器
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);
        ExpressionParser parser = new SpelExpressionParser();
        // spel虚拟容器
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setBeanResolver(new BeanFactoryResolver(ctx));
        Person result1 = parser.parseExpression("@person").getValue(context, Person.class);
        System.out.println("result1:" + result1);
    }
    ~~~
##### 集合相关表达式
* 内联List:
    * 使用{表达式,……}定义内联List,如"{1,2,3}"将返回一个整型的ArrayList,而"{}"将返回空的List.
    * 对于字面量表达式列表,SpEL会使用java.util.Collections.unmodifiableList方法将列表设置为不可修改.
        * 如果List的表达式中的值都是字面常量,则List不可修改.
        * 只要有一个值不是字面常量,则可以修改.
    * 举例:
    ~~~
    @Test
    public void test324() {
        ExpressionParser parser = new SpelExpressionParser();
        //将返回不可修改的空List
        List<Integer> result1 = parser.parseExpression("{}").getValue(List.class);
        System.out.println("result1:" + result1);
        //对于字面量列表也将返回不可修改的List
        List<Integer> result2 = parser.parseExpression("{3,6}").getValue(List.class);
        System.out.println("result2:" + result2);
        try {
            result2.add(22);
            System.out.println("try_result2:" + result2);
        }catch (Exception e) {
            System.out.println("List不可修改");
        }
        //对于列表中只要有一个不是字面量表达式，将只返回原始List，
        //不会进行不可修改处理
        String expression3 = "{{1+2,2+4},{3,4+4}}";
        List<List<Integer>> result3 = parser.parseExpression(expression3).getValue(List.class);
        System.out.println("result3:" + result3);
        // 定义中只要有一个不是字面常量,则可以修改(如1+2不是字面常量,而是一个表达式,所以result6可以修改).
        List<Integer> result6 = parser.parseExpression("{1+2,6}").getValue(List.class);
        System.out.println("result6:" + result6);
        result6.add(43);
        System.out.println("modify_result6:" + result6);
    }
    ~~~
* 内联数组:
    * 和Java数组定义类似,只是在定义时进行多维数组初始化.
    * 举例:
    ~~~
    @Test
    public void test432() {
        ExpressionParser parser = new SpelExpressionParser();
        //声明一个大小为2的一维数组并初始化
        Integer[] result4 = parser.parseExpression("new Integer[2]{1,2}").getValue(Integer[].class);
        System.out.println("result4:" + Arrays.toString(result4));
        result4[1] = 32323;
        System.out.println("modify_result4:" + Arrays.toString(result4));
        //定义一维数组但不初始化
        Integer[] result5 = parser.parseExpression("new Integer[1]").getValue(Integer[].class);
        System.out.println("result5:" + Arrays.toString(result5));
        //定义多维数组但不初始化
        int[][][] result3 = parser.parseExpression("new int[1][2][3]").getValue(int[][][].class);
        System.out.println("result3:" + result3);
        //错误的定义多维数组，多维数组不能初始化
        String expression4 = "new int[1][2][3]{{1}{2}{3}}";
        try {
            int[][][] result6 = parser.parseExpression(expression4).getValue(int[][][].class);
        } catch (Exception e) {
            System.out.println("数组定义错误");
        }
    } 
    ~~~
* 集合、字典元素访问:
    * SpEL目前支持所有集合类型和字典类型的元素访问.
        * 使用"集合[索引]"访问集合元素.
        * 使用"map[key]"访问字典元素.
    * 举例:
    ~~~
    @Test
    public void test23() {
        ExpressionParser parser = new SpelExpressionParser();
        //SpEL内联List访问index为0的元素
        int result1 = parser.parseExpression("{1,2,3}[0]").getValue(int.class);
        System.out.println("result1:" + result1);
        //SpEL目前支持所有集合类型的访问
        Collection<Integer> collection = new HashSet<Integer>();
        collection.add(1);
        collection.add(2);
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("collection", collection);
        // 访问变量collection的index为1的元素
        int result2 = parser.parseExpression("#collection[1]").getValue(context, int.class);
        System.out.println("result2:" + result2);
        //SpEL对Map字典元素访问的支持
        Map<String, Integer> map = new HashMap();
        map.put("a", 1);
        EvaluationContext context3 = new StandardEvaluationContext();
        context3.setVariable("map", map);
        // 访问Map类型变量map的key为"a"的元素.
        int result3 = parser.parseExpression("#map['a']").getValue(context3, int.class);
        System.out.println("result3:" + result3);
    }
    ~~~
* 列表、字典、数组元素修改:
    * 可以使用赋值表达式或Expression接口的setValue方法修改.
    * 举例:
    ~~~
    @Test
    public void test435() {
        ExpressionParser parser = new SpelExpressionParser();
        //1.修改数组元素值
        int[] array = new int[] {1, 2};
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("array", array);
        // 通过表达式修改
        parser.parseExpression("#array[1] = 3").getValue(context, int.class);
        System.out.println("modify_array1:" + Arrays.toString(array));
        // 通过setValue修改
        parser.parseExpression("#array[1]").setValue(context,4);
        System.out.println("modify_array2:" + Arrays.toString(array));
        //2.修改集合值
        Collection<Integer> collection = new ArrayList<Integer>();
        collection.add(1);
        collection.add(2);
        context.setVariable("collection", collection);
        // 通过表达式修改
        parser.parseExpression("#collection[1] = 3").getValue(context, int.class);
        System.out.println("modify_collection1:" + collection.toString());
        // 通过setValue修改
        parser.parseExpression("#collection[1]").setValue(context, 4);
        System.out.println("modify_collection2:" + collection.toString());
        //3.修改map
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("a", 1);
        context.setVariable("map", map);
        parser.parseExpression("#map['a'] = 2").getValue(context, int.class);
        System.out.println("modify_map1:" + map);
        parser.parseExpression("#map['a']").setValue(context,3);
        System.out.println("modify_map2:" + map);
    }
    ~~~
* 集合投影:
    * 指根据集合中的元素中通过选择来构造另一个集合,该集合和原集合具有相同数量的元素.
    * SpEL使用"(list|map).![投影表达式]"来进行投影运算.
    * SpEL投影运算还支持Map投影,但Map投影最终只能得到List结果.
    * 对于数组和集合来说,#this可以代表集合中元素;
    * 对map来说,#this只能代表键值对(Map.Entry),所以可以使用“value”来获取值,使用“key”来获取键.
    * 举例:
    ~~~
    @Test
    public void test3324() {
        ExpressionParser parser = new SpelExpressionParser();
        //1.首先准备测试数据
        Collection<Integer> collection = new ArrayList();
        collection.add(4);
        collection.add(5);
        Map<String, Integer> map = new HashMap();
        map.put("a", 1);
        map.put("b", 2);
        //2.测试集合或数组
        EvaluationContext context1 = new StandardEvaluationContext();
        context1.setVariable("collection", collection);
        // 对集合中所有元素进行+1操作,其中#this代表集合中所有迭代元素.
        Collection<Integer> result1 = parser.parseExpression("#collection.![#this+1]").getValue(context1, Collection.class);
        System.out.println(result1);
        //3.测试字典:SpEL投影运算还支持Map投影,但Map投影最终只能得到List结果
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("map", map);
        //对于投影表达式中的“#this”将是Map.Entry,所以可以使用“value”来获取值,使用“key”来获取键
        List<Integer> result2 = parser.parseExpression("#map.![ value+1]").getValue(context, List.class);
        System.out.println(result2);
        //对于投影表达式中的“#this”将是Map.Entry,所以可以使用“value”来获取值,使用“key”来获取键.
        List<Integer> result3 = parser.parseExpression("#map.![ key+key]").getValue(context, List.class);
        System.out.println(result3);
        // #this表示键值对,可以用#this.key和#this.value代表key和value,#this可以省略.
        List<Integer> result4 = parser.parseExpression("#map.![#this.key+1]").getValue(context, List.class);
        System.out.println(result4);
    }
    ~~~
* 集合选择:
    * 根据原集合通过条件表达式选择出满足条件的元素并构造为新的集合.
    * SpEL使用"(list|map).?[选择表达式]",其中选择表达式结果必须是boolean类型,如果true则选择的元素将添加到新集合中,false将不添加到新集合中.
    * 选择和投影可以一起用.
    * 选择和投影的区别是:投影结果必须是List,投影结果还和原来一样.
    * 举例:
    ~~~
    @Test
    public void test3213() {
        ExpressionParser parser = new SpelExpressionParser();
        //1.首先准备测试数据
        List<Integer> collection = new ArrayList();
        collection.add(4);
        collection.add(5);
        Map<String, Integer> map = new HashMap();
        map.put("a", 1);
        map.put("b", 2);
        //2.集合或数组测试
        EvaluationContext context1 = new StandardEvaluationContext();
        context1.setVariable("collection", collection);
        // 只选择大于4的值到新的list中,#this放在[]中代表每个元素.
        List<Integer> result1 = parser.parseExpression("#collection.?[#this>4]").getValue(context1, List.class);
        System.out.println(result1);
        //3.字典测试
        EvaluationContext context2 = new StandardEvaluationContext();
        context2.setVariable("map", map);
        // #this放在map的[]中代表每个map元素,即键值对.这里是筛选key不为'a'的元素到新的map中.
        Map<String, Integer> result2 = parser.parseExpression("#map.?[#this.key != 'a']").getValue(context2, Map.class);
        System.out.println(result2);
        // 选择和投影一起用:选择key不为'a'的元素,并将所有满足条件的元素value+1并投影为list.
        List<Integer> result3 = parser.parseExpression("#map.?[key != 'a'].![value+1]").getValue(context2, List.class);
        System.out.println(result3);
    }
    ~~~
* 表达式模板:
    * 模板表达式就是由字面量与一个或多个表达式块组成.
    * 每个表达式块由"前缀+表达式+后缀"形式组成，如"${1+2}"即表达式块.
    * 举例:
    ~~~
    @Test
    public void testParserContext() {
        // 创建解析器
        ExpressionParser parser = new SpelExpressionParser();
        // 1.自定义模版
        ParserContext parserContext = new ParserContext() {
            @Override
            public boolean isTemplate() {
                // 定义字符串表达式是模版
                return true;
            }
            @Override
            public String getExpressionPrefix() {
                // 定义表达式前缀
                return "{";
            }
            @Override
            public String getExpressionSuffix() {
                // 定义表达式后缀
                return "}";
            }
        };
        String template = "{'Hello '}{'World!'}";
        Expression expression = parser.parseExpression(template, parserContext);
        System.out.println(expression.getValue());

        // 2.使用默认的模版TemplateParserContext.
        String greetingExp = "Hello, #{ #user }";
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("user", "苍老师");
        String result = parser.parseExpression(greetingExp,new TemplateParserContext()).getValue(context, String.class);
        System.out.println(result);
    }
    ~~~