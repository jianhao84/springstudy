### springboot异步调用@Async
#### 异步处理方式
* 调用之后,不返回任何数据。
    * 在启动类上加注解@EnableAsync启用异步注解
    * 代码
    ~~~
    @Component
    public class TestAsync {
        @Async
        public void test(String name) {
            System.out.println(Thread.currentThread().getId() + "-----------");
        }
    }
    ~~~
* 调用之后,返回数据,通过Future来获取返回数据.
    * 启动类
    ~~~
    @SpringBootApplication
    @EnableAsync
    public class StartAsync {
        public static void main(String[] args) throws Exception {
            ApplicationContext context = SpringApplication.run(StartAsync.class,args);
            System.out.println("开始执行异步任务");
            Future<String> future = context.getBean(TestAsync.class).test2();
            while (true) {
                if(future.isCancelled()){
                    System.out.println("异步任务已被取消");
                    break;
                }
                if (future.isDone() ) {
                    System.out.println("异步任务已执行完成");
                    System.out.println("返回结果是: " + future.get());
                    break;
                }
                System.out.println("异步任务执行中...");
                Thread.sleep(1000);
            }
            // 懒得去手动关闭
            SpringApplication.exit(context);
        }
    }
    ~~~
    * 异步方法类
    ~~~
    @Component
    public class TestAsync {
        @Async
        public Future<String> test2() {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("thread", Thread.currentThread().getName());
            jsonObject.put("time", System.currentTimeMillis());
            return new AsyncResult<String>(jsonObject.toJSONString());
        }
    }
    ~~~
    * 执行结果:
    ~~~
    开始执行
    异步任务执行中...
    异步任务执行中...
    异步任务执行中...
    异步任务执行中...
    异步任务已执行完成
    返回结果是: {"thread":"SimpleAsyncTaskExecutor-1","time":1539835068054}
    ~~~
#### 线程池
* 介绍:
    * @Async的注释中也说到了，会自动去寻找TaskExecutor实例或者bean的名称叫taskExecutor的Executor对象.
    * 如果系统中没有TaskExecutor实例或者没有bean的名称叫taskExecutor的Executor对象，默认会使用SimpleAsyncTaskExecutor线程池.
    * SimpleAsyncTaskExecutor线程池没有进行线程的复用,而是每次操作都新开一个线程进行处理,所以还是需要我们自己实现.
* 实现方式:
    * springbean方式:
    ~~~
    @Configuration
    public class SpringConfig {
        @Bean
        public Executor threadPoolTaskExecutor() {
            ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
            /**线程池维护线程的最小数量*/
            taskExecutor.setCorePoolSize(2);
            /**线程池维护线程的最大数量*/
            taskExecutor.setMaxPoolSize(5);
            /**线程池维护线程所允许的空闲时间*/
            taskExecutor.setKeepAliveSeconds(1);
            /**线程池所使用的缓冲队列大小*/
            taskExecutor.setQueueCapacity(5);
            /**(可不设置,默认AbortPolicy)用来拒绝一个任务的执行的处理handler,可以自己实现RejectedExecutionHandler接口*/
            taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
            /**线程前缀*/
            taskExecutor.setThreadNamePrefix("spring_bean_method-async-thread-pool-");
            return taskExecutor;
        }
    }
    ~~~
    * 实现AsyncConfigurer接口的方法或继承AsyncConfigurerSupport类并重写方法(实现和重写的方法代码都一样).
    ~~~
    @Override
        public Executor getAsyncExecutor() {
            ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
            /**线程池维护线程的最小数量*/
            executor.setCorePoolSize(2);
            /**线程池维护线程的最大数量*/
            executor.setMaxPoolSize(5);
            /**线程池维护线程所允许的空闲时间*/
            executor.setKeepAliveSeconds(1);
            /**线程池所使用的缓冲队列大小*/
            executor.setQueueCapacity(5);
            /**用来拒绝一个任务的执行的处理handler,可以自己实现RejectedExecutionHandler接口*/
            executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
            /**线程前缀*/
            executor.setThreadNamePrefix("implements-method-Thread-");
            /**实现AsyncConfigurer接口或继承AsyncConfigurerSupport方式的getAsyncExecutor必须要initialize,不然会报找不到执行器*/
            executor.initialize();
            return executor;
        }
    ~~~
* 线程池的处理流程(当一个任务想要添加到线程池时,按下规则处理):
    * 如果此时线程池中的数量小于corePoolSize,即使线程池中的线程都处于空闲状态,也要创建新的线程来处理被添加的任务。
    * 如果此时线程池中的数量大于等于corePoolSize,但是缓冲队列workQueue未满,那么任务被放入缓冲队列。  
    * 如果此时线程池中的数量大于corePoolSize,缓冲队列workQueue满,并且线程池中的数量小于maxPoolSize,建新的线程来处理被添加的任务。
    * 如果此时线程池中的数量大于corePoolSize,缓冲队列workQueue满,并且线程池中的数量等于maxPoolSize,那么通过handler所指定的策略来处理此任务。
    * maxPoolSize的设定如果比系统支持的线程数还要大时,会抛出java.lang.OutOfMemoryError: unable to create new native thread 异常。
    * 如果此时线程池中的数量大于corePoolSize时,多余的线程会等待keepAliveTime长时间,如果无新任务可处理就自行销毁,线程池中的线程数也会减少。
* 配置线程数依据:
    * 如果是CPU密集型任务,那么线程池的线程个数应该尽量少一些,一般为CPU的个数+1条线程。
    * 如果是IO密集型任务,那么线程池的线程可以放的很大,如2*CPU的个数。
    * 对于混合型任务,如果可以拆分的话,通过拆分成CPU密集型和IO密集型两种来提高执行效率;如果不能拆分的的话就可以根据实际情况来调整线程池中线程的个数。
    
* 线程池的拒绝策略
    * new ThreadPoolExecutor.CallerRunsPolicy():替换默认线程池,线程队列满了以后交给调用者执行,也就是同步执行.
    * new ThreadPoolExecutor.AbortPolicy():默认策略,队列满了以后,抛弃任务,但是会抛出 rejectedExecution 如果不处理会中断线程.
    * new ThreadPoolExecutor.DiscardPolicy():队列满了,直接丢弃当前任务,不抛出异常.
    * new ThreadPoolExecutor.DiscardOldestPolicy():队列满了,丢弃最老的任务,不抛出异常.

* 异步处理的异常处理
    * 定义一个异常处理的handler,实现AsyncUncaughtExceptionHandler.
    ~~~
    public class AsyncHandler implements AsyncUncaughtExceptionHandler {
        @Override
        public void handleUncaughtException(Throwable ex, Method method, Object... params) {
            if (ex instanceof BusinessException) {
                // 业务异常处理逻辑
            }else {
                // 未知异常处理逻辑
    
            }
        }
    }
    ~~~
    * 和线程池一样(可写在一个类里),实现AsyncConfigurer接口的方法或继承AsyncConfigurerSupport类并重写方法(实现和重写的方法代码都一样)
    ~~~
    @Component
    public class AsyncConfig implements AsyncConfigurer {
        @Override
        public Executor getAsyncExecutor() {
            ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
            /**线程池维护线程的最小数量*/
            executor.setCorePoolSize(2);
            /**线程池维护线程的最大数量*/
            executor.setMaxPoolSize(5);
            /**线程池维护线程所允许的空闲时间*/
            executor.setKeepAliveSeconds(1000);
            /**线程池所使用的缓冲队列大小*/
            executor.setQueueCapacity(17);
            /**用来拒绝一个任务的执行的处理handler,可以自己实现RejectedExecutionHandler接口*/
            executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
            /**线程前缀*/
            executor.setThreadNamePrefix("implements-method-Thread-");
            /**实现AsyncConfigurer接口或继承AsyncConfigurerSupport方式的getAsyncExecutor必须要initialize,不然会报找不到执行器*/
            executor.initialize();
            return executor;
        }
    
        @Override
        public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
            return new AsyncHandler();
        }
    }
    ~~~
    
* 异步方法注意事项
    * 异步事务(尽量避免异步事务):
        * 在@Async方法上@Transactional是没有问题的。 
        ~~~
        @Transactional(rollbackFor = Exception.class)
        @Async
        public void test3() {
            System.out.println("---".concat(Thread.currentThread().toString()));
            UserInfo userInfo = new UserInfo();
            userInfo.setId(1);
            userInfo.setAmount(BigDecimal.ONE);
            userInfoMapper.updateById(userInfo);
    
            UserInfo userInfo2 = new UserInfo();
            userInfo2.setId(1);
            userInfo2.setFlowerTotal(10);
            userInfoMapper.updateById(userInfo2);
            int a = 2/0;
        }
        或者
        @Autowired
        AsyncTransactional asyncTransactional;
        
        @Transactional(rollbackFor = Exception.class)
        @Async
        public void test3() {
            asyncTransactional.modify2_1();
            asyncTransactional.modify2_2();
        }
        ~~~
    * 异步方法的内部调用
        * 异步方法不支持内部调用,也就是异步方法不能写在需要调用他的类的内部。
        * 比如ClassA有a,b,c3个方法。b有Async标注。此时a对b的异步调用是失效的。
        * @Async注解本质使用的是动态代理(和@Transactional一样),如果实在想这么调用,可以把本类在spring里的实例获取出来然后调用.
        ~~~
        @Autowired
        ApplicationContext applicationContext;
        
        public void test() {
            applicationContext.getBean(本类.class).本类中的异步方法名();
        }
        ~~~
    * 异步方法必须是实例方法:
        * 因为static方法不能被Override。
        * 因为@Async异步方法的实现原理是通过注入一个代理类到Bean中,这个代理继承这个Bean,需要覆写异步方法并执行。     
        