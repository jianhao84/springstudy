### Spring的单例bean和线程安全

#### 背景
* spring中的bean默认都是单例的,即在Spring容器中一个类只有一个实例.
* 实例中如果有成员变量并且有对成员变量进行修改的操作,则必须考虑线程同步问题,因为多个线程操作的都是同一个实例.

#### 解决方法
* 线程同步机制:采用了“以时间换空间”的方式.要求程序慎密地分析什么时候对变量进行读写、锁定某个对象、释放对象锁等繁杂的问题，程序设计和编写难度相对较大,不考虑.
* 将bean设置为非单例:在类或bean上加上注解@Scope("prototype").
* ThreadLocal机制:采用了“以空间换时间”的方式,为每一个线程都提供了一份变量，因此可以同时访问而互不影响.

#### 测试
* 测试类
    * 测试接口
    ~~~
    public interface DemoService  {
    
        public void print() throws InterruptedException;
    }
    ~~~
    * 测试接口实现:有2个计数成员变量,用来做对比.
    ~~~
    @Component
    //@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE) // spring默认是单例,这里设置为非单例,就不会count混乱.
    public class DemoServiceImpl implements DemoService {
    
        private  ThreadLocal<Integer> localCount = new ThreadLocal<>();
    
        private int count;
    
    
        @Override
        @Async
        public void print() throws InterruptedException {
            count = 0;
            localCount.set(0);
            Thread.sleep(2000);
            count++;
            System.out.println(Thread.currentThread().getName() + "  count    " + count);
            localCount.set(localCount.get()+1);
            System.out.println(Thread.currentThread().getName() + "  localCount    " + localCount.get());
        }
    }
    ~~~
    *. 启动类
    ~~~
    @SpringBootApplication
    @EnableAsync
    public class TestStart {
    
        public static void main(String[] args) throws InterruptedException {
            ApplicationContext context = SpringApplication.run(TestStart.class,args);
            context.getBean(DemoService.class).print();
            context.getBean(DemoService.class).print();
            context.getBean(DemoService.class).print();
            context.getBean(DemoService.class).print();
            context.getBean(DemoService.class).print();
        }
    }
    ~~~
* 测试场景:
    * 默认单例场景:在多线程环境下会出现count变量混乱的情况.
        * 测试结果如下:各线程的count混乱,localCount正确.
        ~~~
        SimpleAsyncTaskExecutor-1  count    1
        SimpleAsyncTaskExecutor-1  localCount    1
        SimpleAsyncTaskExecutor-3  count    2
        SimpleAsyncTaskExecutor-3  localCount    1
        SimpleAsyncTaskExecutor-4  count    3
        SimpleAsyncTaskExecutor-4  localCount    1
        SimpleAsyncTaskExecutor-5  count    4
        SimpleAsyncTaskExecutor-5  localCount    1
        SimpleAsyncTaskExecutor-2  count    5
        SimpleAsyncTaskExecutor-2  localCount    1
        ~~~
    * 非单例场景:将DemoServiceImpl类的@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)注释去掉.
        * 测试结果:各个线程的count和localCount值都正确.
        ~~~
        SimpleAsyncTaskExecutor-1  count    1
        SimpleAsyncTaskExecutor-1  localCount    1
        SimpleAsyncTaskExecutor-2  count    1
        SimpleAsyncTaskExecutor-2  localCount    1
        SimpleAsyncTaskExecutor-3  count    1
        SimpleAsyncTaskExecutor-3  localCount    1
        SimpleAsyncTaskExecutor-4  count    1
        SimpleAsyncTaskExecutor-4  localCount    1
        SimpleAsyncTaskExecutor-5  count    1
        SimpleAsyncTaskExecutor-5  localCount    1
        ~~~
* 测试结论:由于spring的bean默认是单例,可能会产生成员变量混乱,解决办法2个
    * 使用@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)将bean设置为非单例.
    * 用ThreadLocal.