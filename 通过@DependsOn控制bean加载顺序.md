### 通过@DependsOn控制bean加载顺序
#### 背景
* 在某些场景中,需要A类实例化后B类才能实例化
    * 场景1:B类是缓存初始化类,A类需要使用缓存,所以必须B类先加载.
    * 场景2:消息的订阅发布(事件的发布和监听),为了不丢掉消息,需要订阅类(监听类)先加载.
#### 举例说明
* 测试类
    * ClassA.java
    ~~~
    @Component
    public class ClassA {
    
        public ClassA() {
            System.out.println("加载classA");
        }
    }
    ~~~
    * ClassB.java
    ~~~
    @Component
    public class ClassB {
    
        public ClassB() {
            System.out.println("加载classB...");
        }
    }
    ~~~
* 测试场景:
    * 正常场景:直接启动spring,结果如下
    ~~~
    加载classA...
    加载classB...
    ~~~
    * lazy场景:在ClassB的类上加注解@Lazy,然后启动spring.结果如下
    ~~~
    加载classA...
    ~~~
    * depensOn场景:在ClassA的类上加注解@DependsOn(value = {"classB"}),然后启动spring.结果如下
    ~~~
    加载classB...
    加载classA...
    ~~~
    * depansOn+lazy场景:在ClassB的类上加注解@Lazy,在ClassA的类上加注解@DependsOn(value = {"classB"}),然后启动spring.结果如下
    ~~~
    加载classB...
    加载classA...
    ~~~
* 测试结论:
    * @Lazy注解让bean在需要使用的时候才会去加载.
    * @DepensOn注解可以让bean在指定名字的多个bean加载后再加载.
    * @DepensOn注解如果指定名字的bean中有是带@Lazy注解的,带@Lazy的bean也会先加载.
