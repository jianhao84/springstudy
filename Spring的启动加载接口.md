### Spring的启动加载接口

#### 业务场景
* 应用服务启动完成后,加载一些数据和执行一些应用的初始化动作.比如:
    * 删除临时文件
    * 清除缓存信息
    * 读取配置文件信息
    * 初始化数据库相关
#### 实现方式
下面两种实现方式功能都一样，只是传入参数不一样。可以通过@Order注解控制先后顺序。
* 实现CommandLineRunner接口
    * 执行时机:在spring容器启动完成,所有的bean加载完成后。
    * 举例:
    ~~~
    @Component
    @Order(1)
    public class CommandLineRunnerImpl implements CommandLineRunner {
        @Override
        public void run(String... strings) throws Exception {
            System.out.println("通过实现commandLineRunner接口实现启动加载...");
        }
    }
    ~~~
* 实现ApplicationRunner接口
    * 执行时机:在spring容器启动完成,所有的bean加载完成后。
    * 举例:
    ~~~
    @Component
    @Order(2)
    public class ApplicationRunnerImpl implements ApplicationRunner {
        @Override
        public void run(ApplicationArguments applicationArguments) throws Exception {
            System.out.println("通过实现ApplicationRunner接口实现启动加载...");
        }
    }
    ~~~