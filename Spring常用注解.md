### spring常用注解
1. 组件类注解
* 注解介绍 
    * @Component:标准一个普通的spring Bean类,没有明确定义的时候使用。 
    * @Repository:标注一个DAO组件类,数据层使用。 
    * @Service:标注一个业务逻辑组件类,业务层使用。 
    * @Controller:标注一个控制器组件类,控制层使用。
* 注意点
    * 被注解的java类当做Bean实例，Bean实例的名称默认是Bean类的首字母小写，其他部分不变。@Service也可以自定义Bean名称，但是必须是唯一的.
    * @Component可以代替@Repository、@Service、@Controller，因为这三个注解是被@Component标注的,但尽量使用对应组件注解的类替换@Component注解，在spring未来的版本中，@Controller，@Service，@Repository会携带更多语义。
2. @Configuration和@Bean
* 注解介绍:
    * @Configuration:告诉spring这个类是一个配置类,该类等价于XML中配置beans,相当于Ioc容器，它的某个方法头上如果注册了@Bean,就会作为这个Spring容器中的Bean.
    * @Bean:声明该方法返回的实例是受Spring管理的Bean,有下面这些注解属性:
        * value和name:为bean起一个名字,如果默认没有写该属性,那么就使用方法的名称为该bean的名称,如果都指定,必须一致.
        * autowire:装配方式,有3种:Autowire.NO (默认设置)、Autowire.BY_NAME(根据名字)、Autowire.BY_TYPE(根据类型).
            * @Autowired加载方法上,表示方法的所有参数会被注入.
            * @Autowired加载方法的参数上,表示注入加了的那个参数.
        * initMethod:指定bean初始化后(即构造函数执行完后)会执行的方法,直接指定方法名称即可.
        * destroyMethod:指定bean销毁前会自动执行的方法,如果你不想执行该方法,则添加@Bean(destroyMethod="")来防止出发销毁方法.
* 注意点:
~~~
@Configuration
public class MainConfig {
    @Bean
    public WxMpService getWxMpService() {
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(111);
        return wxMpService;
    }
}
~~~
3. @Value注解
* 注入属性2种用法
    * 注入配置文件中的配置.
    * 通过SpEL表达式注入spring容器中其他bean的属性或方法返回值.
* 例子
    * 创建一个实体类
    ~~~
    @Data
    @Component
    public class PojoBean {
        private String nickName;
        public PojoBean() {
            this.nickName="周剑";
        }
        public String sayHello() {
            return nickName.concat(",hello!");
        }
        public String fuck(String name) {
            return nickName.concat(" fuck").concat(" ").concat(name);
        }
    }
    ~~~
    * @Value注入展示类
    ~~~
    @Data
    @Configuration
    @PropertySource("test.properties")
    public class ValueConfig {
        // 1.@Value标签读取配置文件的配置信息:$表示读取配置.
        @Value("${lalaname}")
        private String lalaname;
        // 2.读取spring容器中其他bean的属性:#表示读取spEL表达式.
        @Value("#{pojoBean.nickName}")
        private String nickName;
        // 3.调用其他bean的无参方法
        @Value("#{pojoBean.sayHello()}")
        private String sayContent;
        // 3.调用其他bean的带参方法
        @Value("#{pojoBean.fuck('妹子')}")
        private String fuckContent;
    }
    ~~~
    * 启动类
    ~~~
    @SpringBootApplication
    public class TestStart {
        public static void main(String[] args) {
            ApplicationContext context = SpringApplication.run(TestStart.class,args);
            System.out.println(context.getBean(ValueConfig.class));
        }
    }
    ~~~
    * 执行结果
    ~~~
    ValueConfig(lalaname=23, nickName=周剑, sayContent=周剑,hello!, fuckContent=周剑 fuck 妹子)
    ~~~
4. 依赖注入注解:
* 注解介绍
    * @Autowired:默认是按照类型进行装配注入,是spring的注解.
    * @Inject:默认是按照类型进行装配注入,是javax.inject的注解.
    * @Resource:默认是按照名称来装配注入,是spring的注解.
* 异同点:
    * 3种注解如果最终都没有找到对应的bean,默认会抛异常;@Autowired有required属性,如果设置为false就不会抛异常.
    * @Autowired和@Inject基本是一样,因为两者都是使用AutowiredAnnotationBeanPostProcessor来处理依赖注入.@Resource是CommonAnnotationBeanPostProcessor来处理依赖注入.当然,两者都是BeanPostProcessor.
    * 构造方法里不支持@Autowired、@Inject、@Resource注入的实例.
    * @Autowired和@Inject
        * 默认按照类型进行装配注入.
        * 可以通过@Qualifier按指定的名称进行注入.
        * 如果按类型注入失败(找不到或者找到多个实现),则退化为按名称注入.
    * @Resource
        * 默认按照名称来装配注入.
        * 如果按名称注入失败，会退化为按类型注入.
        * 可以用@Qualifier显式按指定的名称注入.
        * 如果@Qualifier按指定名称注入失败,会退化为按名称注入.这时候如果按名称注入失败，就不会再退化为按类型注入了。
5. 消除自动装配注入歧义性注解@Primary和@Qualifier
* 歧义性产生的原因:
    * 接口的实现方式，因为接口有多个实现类,这样在自动装配时不知道找哪一个具体的实现类.
* 注解介绍:
    * @Primary:在众多相同的bean中,优先选择用@Primary注解的bean,相当于默认选择(该注解加在各个bean上).
    * @Qualifier:在众多相同的bean中,@Qualifier指定需要注入的bean,相当于显示选择(该注解跟随在@Autowired后)
    * 显示选择优先于默认选择:比如接口有实现类A和B,A用@Primary.而接口的注入地方使用Qualifier("a"),那么注入的是A的bean.
6. @Lazy注解:
* 注解介绍:
    * 该注解有个value属性,默认为true,会懒加载,只有在被引用的时候才会加载,为false会在容器初始化的时候预加载.
    * 需要懒加载的bean不要使用注入注解,要使用beanFactory加载.
7. @Async异步方法注解
* 注解介绍
    * @Async标注的方法，称之为异步方法,这个注解用于标注某个方法或某个类里面的所有方法都是需要异步处理的。
    * 被注解的方法被调用的时候,会在新线程中执行,而调用它的方法会在原来的线程中执行。
* 使用步骤
    * springMvc项目需要配置xml:
    ~~~
    <!--扫描注解，其中包括@Async -->
    <context:component-scan base-package="com.test"/>
    <!-- 支持异步方法执行, 指定一个缺省的executor给@Async使用-->
    <task:annotation-driven executor="defaultAsyncExecutor"  /> 
    <!—配置一个线程执行器-->
    <task:executor id=" defaultAsyncExecutor "pool-size="100-10000" queue-capacity="10" keep-alive =”5”/>
    ~~~
    * springboot项目需要在启动类上加注解@EnableAsync,并配置自己的executor的bean(不配置executor会使用默认的).
    ~~~
    @Configuration
    public class AsyncThreadPoolConfiguration {
        @Bean
        public ThreadPoolTaskExecutor taskExecutor() {
            ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
            taskExecutor.setCorePoolSize(100);
            taskExecutor.setMaxPoolSize(500);
            taskExecutor.setThreadNamePrefix("async-thread-pool-");
            return taskExecutor;
        }
    }
    ~~~
    * 在需要异步的方法上加@Async注解.
8. 元注解
* 注解介绍
    * @Retention:定义注解的保留策略
        * @Retention(RetentionPolicy.SOURCE):注解仅存在于源码中,在class字节码文件中不包含.
        * @Retention(RetentionPolicy.CLASS):默认的保留策略,注解会在class字节码文件中存在,但运行时无法获得.
        * @Retention(RetentionPolicy.RUNTIME):注解会在class字节码文件中存在,在运行时可以通过反射获取到
    * @Target:定义注解的作用目标
        * @Target(ElementType.TYPE):接口、类、枚举、注解.
        * @Target(ElementType.FIELD):字段、枚举的常量.
        * @Target(ElementType.METHOD):方法.
        * @Target(ElementType.PARAMETER):方法参数.
        * @Target(ElementType.CONSTRUCTOR):构造函数.
        * @Target(ElementType.LOCAL_VARIABLE):局部变量.
        * @Target(ElementType.ANNOTATION_TYPE):注解.
        * @Target(ElementType.PACKAGE):包.
    * @Document:说明该注解将被包含在javadoc中.
    * @Inherited:说明子类可以继承父类中的该注解.
9. @Named:是javax.inject中的注解,和spring中的@Component功能相同.
10. @Singleton:是javax.inject中的注解,只要在类上加上这个注解,就可以实现一个单例类,不需要自己手动编写单例实现类.等价于spring的@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON).
11. 请求参数注解
* @RequestParam
    * 作用是提取和解析请求中的参数,@RequestParam支持类型转换,类型转换目前支持所有的基本Java类型.
    * 举例:
        * @RequestParam([value="number"], [required=false])  String number
        * 将请求中参数为number映射到方法的number上,required=false表示该参数不是必需的,请求上可带可不带。
* @PathVariable
    * 处理requet的uri部分,绑定url传过来的值到方法的参数上.
    * 举例
    ~~~
    @Controller 
    @RequestMapping("/owners/{a}") 
    public class RelativePathUriTemplateController { 
      @RequestMapping("/pets/{b}") 
      public void findPet(@PathVariable("a") String a,@PathVariable String b) {     
        // implementation omitted 
      } 
    }
    ~~~
* @RequestHeader
    * 处理request header部分的注解
    * 举例:将头部信息绑定到方法参数上.
    ~~~
    @RequestMapping("/test") 
    public void displayHeaderInfo(@RequestHeader("Accept-Encoding") String encoding, 
                                  @RequestHeader("Keep-Alive")long keepAlive)  { 
      //...TODO 
    }
    ~~~
* @CookieValue
    * 处理request cookie的注解
    * 举例:将cookie里JSESSIONID绑定到方法参数上.
    ~~~
    @RequestMapping("/test")  
    public void displayHeaderInfo(@CookieValue("JSESSIONID") String cookie)  {  
      //...TODO
    }
    ~~~
12. @Inherited：说明子类可以继承父类中的该注解.
13. @Scope:spring中bean的作用域:
    * singleton(默认模式):单例,指一个bean容器中只存在一份,取值ConfigurableBeanFactory#SCOPE_SINGLETON.
    * prototype:每次请求(每次使用)创建新的实例,destroy方式不生效,取值ConfigurableBeanFactory#SCOPE_PROTOTYPE.
    * request:每次http请求创建一个实例且仅在当前request内有效,取值WebApplicationContext#SCOPE_REQUEST.
    * session:同上,每次http请求创建,当前session中有效,取值WebApplicationContext#SCOPE_SESSION.
    * global session:基于portlet的web中有效(portlet定义了global sessio),如果在web中,同session.




