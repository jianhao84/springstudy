### ThreadLocal介绍
#### 介绍
* 解决的问题:
    * 将一个公共的可变对象，转换为线程私有的可变对象，防止出现不正确的共享.
* 使用场景:
    * 比如spring中的bean在容器中默认都是单例存在的,但需要在多个线程处使用,这就需要该bean中的变量在各个线程处不要相互影响,所以需要使用ThreadLocal.
    * 一个可变对象,在每一个线程(或者说多线程环境)中都会被使用,并且该可变对象无需在各个线程间进行同步,那么该可变对象就可以通过ThreadLocal进行管理,而此时该可变对象变成多实例对象,与线程一一对应.
#### 源码方法分析:
* ThreadLocal有4个方法
    * public void set(T value){}:设置当前线程的线程局部变量的值.
        * 源码注释:
        ~~~
        /**
         * 1.set方法用来修改或者初始化ThreadLocal管理的变量对象.
         * 2.ThreadLocal对象调用get方法获取变量的时候,要么重写initialValue()方法,要么主动调用set方法,否则将返回null。
         */
         public void set(T value) {
            Thread t = Thread.currentThread();
            ThreadLocalMap map = getMap(t);
            if (map != null)
                map.set(this, value);
            else
                createMap(t, value);
         }
        ~~~
        * createMap方法:
        ~~~
        void createMap(Thread t, T firstValue) {
            //直接new一个新的ThreadLocalMap实例,封装进入当前线程对象t.
            t.threadLocals = new ThreadLocalMap(this, firstValue);
        }
        ~~~
    * protected T initialValue(){}:返回该线程局部变量的初始值.
        * initialValue源码注释:
        ~~~
        /**
         * 1.initialValue()方法声明为protected,目的就是让子类覆盖重新的,如果不覆盖重写,则返回null.
         * 2.如果没有重写initialValue()方法,也没有手动set(T value),ThreadLocal对象直接调用get()方法,最终从setInitialValue()返回的对象为null.    
         */
        protected T initialValue() {
            return null;
        }
        ~~~
        * setInitialValue
        ~~~
        private T setInitialValue() {
            T value = initialValue();
            Thread t = Thread.currentThread();
            ThreadLocalMap map = getMap(t);
            if (map != null)
                map.set(this, value);
            else
                createMap(t, value);
            return value;
        }
        ~~~
      
    * public T get(){}:返回当前线程所对应的线程局部变量.
        * 源码注释:
        ~~~
        public T get() {
            //获取当前线程对象
            Thread t = Thread.currentThread();
            /**
             *从当前线程对象中获取ThreadLocalMap类型实例对象:
             * 1.ThreadLocalMap,是ThreadLocal的静态内部类.
             * 2.ThreadLocalMap把其外部类ThreadLocal的实例对象作为key,把要管理的可变对象作为value.
             * 3.ThreadLocalMap的实例对象由当前线程对象Thread的实例持有,而不是由ThreadLocal持有.
             */
            ThreadLocalMap map = getMap(t);
            if (map != null) {
                //this对象，也就是ThreadLocal对象作为map的key，获取ThreadLocal管理的可变对象
                ThreadLocalMap.Entry e = map.getEntry(this);
                if (e != null) {
                    @SuppressWarnings("unchecked")
                    T result = (T)e.value;
                    return result;
                }
            }
            //map为空，延迟执行初始化方法
            return setInitialValue();
        }
        ~~~
        * getMap:每个线程里都有个变量threadLocals.
        ~~~
        ThreadLocalMap getMap(Thread t) {
            return t.threadLocals;
        }
        ~~~
    * public void remove(){}:
        * 将当前线程局部变量的值删除,目的是为了减少内存的占用,该方法是JDK 5.0新增的方法。
        * 需要指出的是,当线程结束后,对应该线程的局部变量将自动被垃圾回收,所以显式调用该方法清除线程的局部变量并不是必须的操作,但它可以加快内存回收的速度。
* 相互之间的关系
    * 业务代码中定义ThreadLocal变量.
    * ThreadLocal类中有个内部类ThreadLocalMap.
    * Thread线程类有个ThreadLocalMap类型的变量threadLocals,用来存放各个定义在业务代码中的ThreadLocal变量的线程变量信息.
    * ThreadLocal变量只是一个入口,真正存放变量值的地方在每个线程Thread的ThreadLocalMap中.即使业务代码中定义了多个ThreadLocal,都是放在每个线程Thread的ThreadLocalMap中.
    * 在每个线程Thread的ThreadLocalMap中,用业务代码中ThreadLocal变量对象作为key.