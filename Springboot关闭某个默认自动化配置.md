### Springboot关闭某个默认自动化配置
* 举例:不想自动配置Redis
~~~
@Controller
@SpringBootApplication(exclude = { RedisAutoConfiguration.class })
public class StartupServer {
    public static void main(String[] args) {
            SpringApplication.run(StartupServer.class, args);
}
~~~